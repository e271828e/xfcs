import numpy as np

import xFCS.environment.wgs84 as wgs84
from xFCS.aircraft.aircraft import HandOfGodRig
from xFCS.aircraft.rigidbody import InitialCondition
from xFCS.common.attitude import Attitude3D as Att
from xFCS.environment.atmosphere import Atmosphere
from xFCS.environment.terrain import Flat
from xFCS.simulation.blocks.aircraft import AircraftHoGCtlBlock
from xFCS.simulation.blocks.general import Constant
from xFCS.simulation.engine import Model, Signal

trn = Flat()
atm = Atmosphere()
aircraft = HandOfGodRig(trn, atm)
init = InitialCondition(
    Ob = wgs84.Point(h = 0),
    n_b = Att.from_euler(0, 0, 0),
    ω_wb_b = np.array((0., 0, 0)),
    v_eOb_b = np.array((0, 0, 0)))
aircraft.airframe.initialize_state(initial_condition = init)

m = Model(t_end = 10)

aircraft_block = AircraftHoGCtlBlock(aircraft = aircraft)
aircraft_inputs_block = Constant(
    value = aircraft_block.ControlInputs(
    t_Oc_zc = 0.1))

m.add_block('inputs', aircraft_inputs_block)
m.add_block('aircraft', aircraft_block)
m.add_signal('aircraft_out', Signal(logging = True))
m.connect(m.blocks['inputs'].get_output(), m.blocks['aircraft'].get_input())
m.connect(m.blocks['aircraft'].get_output(), m.signals['aircraft_out'])

m.run(profiled = True)
log = m.signals['aircraft_out']._log
save_path = "tmp\\hand_of_god_test"
aircraft.plot(log, save_path = save_path, keep_open=False)
