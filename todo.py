#TODO:LandingGear: por ahora basta considerar ruedas libres o con steering.
# definir property brk ldg: steered/castered.if steered, add block u.psi braked
# / not braked. if braked, add block u.brk.

#mas adelante, definir steering y braking como subsystems de landing gear.
#potencialmente, cada uno tendra sus propios bloques en u y en x. en algun
#momento steering tendra su propio actuador.
#
#TODO: fucking write docstrings

#TODO: try making everything a subsystem

#TODO: try to move multiple imports to __init__.py

#TODO: create tailwheel aircraft!
#TODO: create ISA model as a System object, with wind as a subsystem. add it as
#a reference to the AircraftBase constructor. and embed it within Simulation.
#problem: this basically forces me to use the same dt for both Aircraft and
#Atmosphere, that is, for all physics. and the Simulation class will no longer
#be agnostic regarding the simulation object it handles. it will need to have an
#aircraft and an atmosphere model. well, not that bad...

#TODO: multi-rate scheduling
# TODO: ajustar parametros para Cessna172 con JSBSim

#TODO: arreglar la explosion numerica al volcar
# TODO: Añadir contact points
# TODO: Actualizar Doc ldg

# arbitrary number arbitrary airframe attachment point and strut orientation
# arbitrary terrain slope customizable shock absorber force function
# customizable steerable & castering wheel static & dynamic friction coefficient
# simulates both oleo struts and solid shock absorbers stable behavior below
# maximum friction coefficients

# TODO: Considerar cómo hacer un modelo con masa en el pistón assembly

#TODO: add logging

#TODO: add Cartesian x and y to airframe kinematics. these are actually
#different states, they cannot be derived from any other kinematic variables. or
#maybe not, and leave them for an Euler angle, flat Earth implementation for
#control design
#TODO: Implement FlatEarth RigidBodyDynamics

#TODO: #create helper SimObjects such as Sources: white noise, step, ramp, and
#filters

#TODO: *maybe* create a EmptyNodeType subclass from NodeType that is
#handled correctly (ie, ignored) by the NodeType function when passed as
#a block

#TODO: add unit tests!

# TODO: #read up on screw theory, twists, wrenches

# TODO:
#basic testing. test multiple simple scenarios to exercise the dynamics and
#kinematics, then run with vertical rotor orientations, different attitudes,
#nonzero rOG, etc
