import pyglet

window = pyglet.window.Window(width = 320, height = 200)
label = pyglet.text.Label("Reading joystick input",
                          font_name='Times New Roman',
                          font_size=36,
                          x=window.width//2, y=window.height//2,
                          anchor_x='center', anchor_y='center')

#the Window class can dispatch key press events to the event handler assigned to
#its on_key_press attribute
@window.event('on_key_press')
def window_key_press_handler(symbol, modifier):
    print(f"Key {symbol} was pressed")

# the decorator above adds an event handler that is executed before the default one
# doing the following completely replaces the default handler instead:
# window.on_key_press = window_key_press_handler

@window.event
def on_draw():
    window.clear()
    # image.blit(0, 0)
    label.draw()

#WARNING: the default handler for on_close only calls window.close() if the
#running event loop is pyglet.app.event_loop. this means that if we have defined
#our own event_loop, we need to define a custom handler for window.on_close()
#that calls window.close()

def show_joystick_axes(dt):
    print(f"Delta time: {dt}")
    print(f"LeftStick: {joystick.x, joystick.y}")
    print(f"RightStick: {joystick.rx, joystick.ry}")
    print(f"Triggers: {joystick.z}")
    print(f"Buttons: {joystick.buttons}\n")


def handle_joyaxis_motion(joystick, axis, value):
    print(joystick, axis, value)


try:
    joystick = pyglet.input.get_joysticks()[0]
    joystick.open()
except IndexError as e:
    print("No joystick detected")
    raise IndexError from e

#two different approaches
pyglet.clock.schedule_interval(show_joystick_axes, 0.2) #poll
#attach handler to joystick's on_joyaxis_motion event
joystick.on_joyaxis_motion = handle_joyaxis_motion
pyglet.app.run()
