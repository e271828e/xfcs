from xFCS.simulation.engine import Signal, Model
from xFCS.simulation.blocks.general import *

m = Model()
m.add_block('neg', Gain(value = -0.5))
m.add_block('adder', Adder())
m.add_block('accum', Accumulator(length = 1, initial_value = 4))
m.add_block('const', Constant(value = 1))
m.add_signal('error', Signal(logging = True))
m.add_signal('feedback', Signal(logging = True))

m.connect(m.blocks['const'].get_output(), m.blocks['adder'].get_input('input1'))
m.connect(m.signals['feedback'], m.blocks['adder'].get_input('input2'))
m.connect(m.blocks['adder'].get_output(), m.signals['error'])
m.connect(m.signals['error'], m.blocks['accum'].get_input())
m.connect(m.blocks['accum'].get_output(), m.blocks['neg'].get_input())
m.connect(m.blocks['neg'].get_output(), m.signals['feedback'])
# m.connect(m.blocks('neg').get_output('Output'), m.signals('error'))
# m.connect(m.signals('error'), m.blocks('adder').get_input('Input1'))

m.dt = 1
m.t_end = 10
m.run(profiled = True)

print(m.signals['error']._log)
print(m.signals['feedback']._log)
