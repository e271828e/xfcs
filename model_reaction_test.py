import numpy as np

import xFCS.environment.wgs84 as wgs84
from xFCS.aircraft.rigidbody import InitialCondition
from xFCS.aircraft.aircraft import ReactionTestRig
from xFCS.common.attitude import Attitude3D as Att
from xFCS.simulation.blocks.aircraft import AircraftGndCtlBlock
from xFCS.simulation.blocks.io import XBoxOne, XBoxOne2GndCtl
from xFCS.simulation.engine import Model, Signal

aircraft = ReactionTestRig()

init = InitialCondition(
    Ob = wgs84.Point(h = -0.086856293),
    n_b = Att.from_euler(0, 0, 0),
    ω_wb_b = np.array((0., 0, 0)),
    v_eOb_b = np.array((0, 0, 0)))
aircraft.airframe.initialize_state(initial_condition = init)

m = Model(t_end = 15)

m.add_block('xboxone', XBoxOne())
m.add_block('adapter', XBoxOne2GndCtl())
m.add_block('aircraft', AircraftGndCtlBlock(aircraft = aircraft))
m.add_signal('aircraft_out', Signal(logging = True))

m.connect(m.blocks['xboxone'].get_output(), m.blocks['adapter'].get_input())
m.connect(m.blocks['adapter'].get_output(), m.blocks['aircraft'].get_input())
m.connect(m.blocks['aircraft'].get_output(), m.signals['aircraft_out'])

m.run(profiled = True, real_time = True)

log = m.signals['aircraft_out']._log
save_path = "tmp\\interactive_reaction_test"
aircraft.plot(log, save_path = save_path , keep_open=False)
