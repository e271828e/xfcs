requisitos:

necesito una clase root que contenga todas las subclases.

un NodeType tendra como class properties: children = dcit{'label1', type1,
'label2', type2}

y Node tendra como data data = dict{'label1', type1_instance, 'label2', type2}

la diferencia respecto a structuredvector es data ya no es un array, sino otro
dict pero con las instancias de los objetos cuyos tipos indica children y se
crean en __init__. en cambio los dunder methods de set y get no crean objetos,
solo acceden a los que ya existen y los devuelven. al instanciar
type1_instance, el constructor de type1 llamara recursivamente a los
constructores de las clases indicadas en su propio children

el aliciente de esto es que me permite instanciar toda una jerarquia de
objetos, pero con clases generadas dinamicamente a partir de los componentes
que se crean en el vehiculo, y acceder a cualquier nodo de la jerarquia y
cambiarlo (cosa que no es posible con nametuples) y hacerlo con dot notation
(cosa que si es posible con namedtuples)

OJO: si hago que la clase almacene los instance attributes en el dict normal de
la clase, en vez de en uno llamado data, ya no tengo que hacer override de
setitem, getitem,etc

pero un momento... para eso no hace falta crear una clase jerarquica dedicada.
en vez de devolver un UType, cada componente podria devolver simplemente una
instancia de U! con todos los objetos ya creados. cada componente va
definiendo su clase e instanciandola, y pasandosela al componente padre, que a
su vez la incluye como atributo en su propia clase U, etc.

pero crear clases individualizadas para esto es overkill, claramente
lo que necesito es simplemente una subclase de dict que:
1) no permita anadir keys nuevas despues de crearse
  como esto:
  https://stackoverflow.com/questions/32258706/how-to-prevent-key-creation-through-dkey-val
  por ejemplo, podria pasar un tuple con los key names que quiero permitir y
  dejarlas congeladas

2) permita acceder a sus keys con dot notation. para esto puedo definir un
  getattr (que recordemos solo se llama si la key solicitada no existe ya) que
  a su vez llame a getitem. y tambin un setattr que mire si la key existe ya
  entre las aceptadas como validas, y si no haga dispatch a super(). para
  setitem, nuevamente miro si la key esta en las que he definido al crear, y
  si no no permito nada.

una vez definido este tipo de dict, lo que hago es simplemente construir
recursivamente con llamadas a U el U del vehiculo completo. El resultado sera
un arbol de CustomDicts en el que los leafs seran objetos concretos
(tipicamente floats, bools, ints o arrays). Aircraft y sus diferentes
componentes lo unico que tienen que hacer es extraer el nodo relevante para
cada uno de sus componentes y pasarselo.
