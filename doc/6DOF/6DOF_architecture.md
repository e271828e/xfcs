# TO DO, next derivation
The derivation must make use of an air vehicle reference frame. The airframe must be simply one of the multiple bodies
whose position and orientation are described with respect to it. The air vehicle reference frame will be denoted simply
by $b$ and its origin as $O$, no subscripting. The airframe will be body $B_0$, with reference frame $b_0$ and origin
$O_0$. The remaining air vehicle bodies (stores and rotating parts) will be denoted $B_k$, with reference frames $b_k$
and origins $O_k$, $\forall k \ge 1$.

# Vehicle
The vehicle does not own the environment model (which includes atmosphere, wind and turbulence, terrain... whatever).
these must be input arguments to the air vehicle's update method, and the f function. These will in turn relay these
objects to those internal components that require them. For example, terrain is required by aerodynamics and landing
gear model. And atmosphere and wind are required by aerodynamics.

The environment model must be updated separately, with its own method! It holds its own states and can be queried to
provide its output variables at any location.

**Open issues**

the complexity of coupling the rotational dynamics of the rotors with those of the airframe is simply not worth it. note
that by neglecting this coupling does NOT lose neither gyroscopic effects (which depend on h) nor torque asymmetry
(which is accounted for by the external wrenches acting on the overall system). the only effect we lose is the angular
acceleration induced on the airframe during rotor transients. this is NEGLIGIBLE. it simply isn't worth it. so drop the
fucking thing already. the dynamic equations will still be coupled, but only between velocity and angular velocity of
the airframe. SIMPLIFY AND DECOUPLE IN THE DOCUMENTATION

should components and other elements of the air vehicle be class attributes or instance attributes? the essential
requirement is that they be customizable for each Aircraft subclass. if we define them as Aircraft class attributes
outside __init__, they will be automatically inherited by any subclass. not what we need. since _XType and _UType depend
on the components, they will too need to be deferred to __init__


# Subsystems
Each of these components must have a method that, returns the force and torque exerted on this component by agents
external to the air vehicle, and resolved in the air vehicle frame.

**Open issues**
--
powerplant, landing gear, control surfaces are components.\
mass? maybe, because it generates a wrench. maybe not, because its properties are also core to the airframe dynamics,
and also because it is not a physical element

can we say the same of aerodynamics? clearly, control surfaces belong to the aerodynamics in the sense that their state
is required to compute the aerodynamic forces and moments. but that may also be true of powerplant. in fact, it doesn't
seem entirely realistic to expect that all components will have exactly the same interface and the main air vehicle
object need not care which exact component subtype it is dealing with, simply because different components will need
different inputs. landing gear does not care about airdata, but powerplant and control surfaces certainly do. powerplant
does not care about terrain, but landing gear and aerodynamics do.

a control surface is NOT an actuator. it HAS an actuator. but its deflection is determined by the actuator state and
other things. however, those other things are stateless. and if they were not, the control surface would create a
hierarcy x_srf/act and x_src/other in which each subnode would be provided by each subcomponent

what is certain is: ANYTHING that has either u or x must go in the component list!

IMPORTANT: x only must include continuous states (not discrete ones), those which intervene in the update method and
subject to numerical integration. the others should be store somewhere else.

this raises another question: how do we handle discrete inputs? such as "lower flaps" or "lower landing gear" or
"release store". and how do we store the discrete states of systems? at first glance, the answer could be: the discrete
inputs can be part of u because we do not need to do algebra directly with u. but if we don't need to perform algebraic
operations on u, this makes it clear that u DOES NOT NEED TO BE A STRUCTURED VECTOR!!! It can be, like y, just a tuple
of different objects, hierarchically organized so that Aircraft knows how to distribute them between different
components. so, it still needs to be a dynamically generated type, but not a Node subtype. exactly likeY, which will be
a hierarchical gathering of the Y's of all components

--
y si en cada component creara un attribute x local que se vinculara al bloque adecuado de Aircraft.x, de manera que
cuando el update method de Aircraft actualizara Aircraft.x la actualizacion se propagara directamente por toda la
jerarquia de componentes? con esto me ahorraria la necesidad de estar pasandole el bloque de x pertinente a cada
componente

como funcionaria esto? durante la inicializacion, cada bloque inicializaria su x a None. a peticion externa, enviaria
aguas arriba su XType. al llegar a Aircraft, este ensamblaria su propio XType, crearia una instancia x_master, e iria
llamando a un metodo init_x de cada componente con el bloque que le corresponde de x_master para que este a su vez
hiciera self.x = _XType(x_master_block). a partir de ahi, su x esta ligado al x_master! despues ese componente llamaria
recursivamente al init_x de sus propios subcomponentes.

esto simplificaria un poco los interfaces, pero parece peligroso. ademas, aunque ahorre en los interfaces y no tenga que
propagar explicitamente x aguas bajo, si tengo que definir methods de propagacion para la inicializacion. de momento
mejor distribuir explicitamente los bloques aguas abajo.

--

## Aerodynamics
Abstract class.

Attributes: $r_{O O_a}^{b}$, $q^{b}_{a}$, aerodynamic coefficients, aerodynamic database, wet surface area The position
and attitude may not be necessary if our aerodynamic database already works in the airframe axes, but it is a good thing
to decouple them entirely just in case.

Methods: Air Data: Computes magnitudes relative to the surrounding airflow in airframe components: Aerodynamic velocity
components, angular velocity relative to the airflow, airflow angles and Mach number. This method needs xvel, xpos (to
transform wind velocities to body axes) and also inputs from the atmosphere and wind models. ($v_{aO_a}^{b}$,
$\omega_{ab}^{b}$, $p_s$, $p_t$, $\rho$, $q$, $M$, $\alpha$, $\beta$) = airdata_local(kinematics, atmosphere, wind...).
Note that the atmosphere and wind models must be queried for local value such as $v_{wO_a}^{n}$).

This method first needs to translate kinematics from $O$ to $O_a$ and project onto the $a$ frame.

Abstract methods: ($F_{aero}^{b}$, $T_{air[O]}^{b}$) = tf_air(airdata, terrain)
This must compute the resulting aerodynamic force and aerodynamic moment at the aerodynamic center $O_a$, transfer them
to the airframe origin $O$, and resolve them in the airframe axes. It needs the terrain model to compute h_AGL of its
reference point.

## Mass

This must provide the mass properties of the "rigid" airframe, that is, the empty airframe, fuel, stores and non-moving
contributions of rotating parts (for example, lever arm moments of inertia). This should be an abstract class, because
depending on the specific air vehicle, these properties might need to be computed on the fly or be stored in advance for
different configurations, or maybe assembled from individual components which may change (stores...).

This subsystem has no dynamics of itself. However, it is influenced by other systems that might have, such as the fuel
system

Abstract methods: mass, inertia_tensor = get_mass(self, fuel_status, stores_status) Fuel status might be anything from a
simple fuel quantity, to a complex description of the status of each of many different tanks, from which mass and
inertia tensor contributions could be computed

## Landing Gear

Abstract class

Attributes: Position of its reference point with respect to O0, attitude with respect to b0.

SimpleLandingGear: Attributes: Damping Coefficient, Spring coefficient Methods: actions(x_pos, x_vel, terrain). Given
the position and velocity of the airframe origin, this computes the position and velocity of the landing gear tip. It
calls the terrain model with this position, and it obtains hAGL from it. If its h_AGL < 0, then it accounts for its
value and the velocity component along the landing gear axis to compute the normal force


## Powerplant (pwp)
This provides a simple model for a engine-propeller combination. It has an the following attributes:
- A shaft reference frame, with its origin $O_s$ at the propeller center, its $x$ axis aligned with the engine's output
  shaft, and its $y$ and $z$ axes fixed to the airframe. It is described by the position of $O_s$, given by
  $r_{OO_s}^b$, and the attitude of $s$ relative to the air vehicle frame $b$, given by $R^b_s$.
- Attributes describing the engine's torque curve and the propeller's mass and aerodynamic properties: $J_{xx}$...
- A method to query the external aerodynamic forces and moments acting on the powerplant assembly (essentially the
  propeller), applied at $O_s$ and projected in $s$ axes. This will require the airspeed at $O$. It is the
  responsibility of the component to transform it to its own location and axes.
- A method to query the engine's shaft torque as a function of throttle setting and shaft angular rate.

Any component object (of which powerplant is a child, others are: landing gear, aerodynamics...) must provide a method
to compute the external forces and moments acting on it, applied and resolved in its own reference frame, and another
method that applies and resolves those actions on the air vehicle frame. Different subtypes of components will require
different inputs for the first one.

However, because the rotational dynamics of the powerplant are coupled with those of the overall air vehicle, in this
particular case we also need to know the internal torque applied by the engine to the propeller along $x_s$ to use it in
the angular momentum equation for the propeller dynamics. Two options:
- Define method *tf_ext* providing external forces and torques translated to and resolved in the air vehicle frame and a
  separate method providing the net torque along $x_s$ (the sum of external and engine torques).
  Drawback: Needs to compute external actions on the propeller twice.
- Define a method *tf_ext* providing external forces and torques on the local component frame, another method *tf_trans*
  (which will be common to all components) transferring those to the air vehicle frame, another *tf_body*, which simply
  concatenates *tf_ext* and *tf_trans* and another *eng_torque*, exclusive to the powerplant component, providing
  current engine torque along $x_s$.
- Define a method *tf_ext* in the same way it would work for any other component, that is, one that returns torques and
  forces in the air vehicle frame. but for this particular case, return also the net torque acting along $x_s$, that is,
  the difference between external and engine torque.

## Control Surfaces (srf)

 El actuador jerárquicamente debe estar bajo control surface, igual que eng y prp en pwp, porque xdot del actuador no se
puede determinar sin hinge moment ni sin conocer la relación cinemática, que es propiedad de la surf. Luego control
surface has an actuator y también tiene la transmisión y cualquier otro elemento. Si hubiera un tab, por ejemplo, la
superficie tendría dos actuadores. Así que hay que definir una clase que agrupe actuator y estos otros elementos:
Control Surface . Lo mismo con steerable landing gear. Así que en la jerarquía, tendré srf/lail/act Y ojo, esto no
significa que no pueda definir distintos tipos de actuado, rotary, linear, Ema, hydro, y cada uno tendrá un sv distinto,
pero tendré que pasarle una instancia al constructor de surf. Finalmente, cómo asegurar que la aero asignada es
compatible con la Ctl surf config? Simplemente miró las labelsde las Ctrl srf que le entran y veo que hay match Concepto
de component groups For group in (pwp, srf, ldg) For cmp in self.components.group

# Weather Model

## Atmosphere
Given a location (expressed, for example, as a WGS84 point) returns static pressure, density, temperature and speed of sound.

## Wind
Given a location (location, maybe terrain data), provides the local wind velocity in NED components. This may include
the stationary wind velocity, as well as a turbulence model.