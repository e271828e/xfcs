### Definitions

$O_g$: Origin of the landing gear frame, located at the end point of the strut and tire assembly when the strut is at
its natural length (no compression or elongation, other than that due to the weight of the assembly itself). The
location of $O_g$ with respect to the air vehicle frame is constant and given by $r_{O_bO_g}^b$. Its altitude can be
computed as:

$h_{O_g} = h_{O_b} -e_3^T r_{O_bO_g}^w = h_{O_b} -e_3^T R^w_b r_{O_bO_g}^b$

$c$: Contact frame. It is defined as follows:
- Its origin $O_c$ is the vertical projection of $O_g$ onto the ground.
- Its $z_c$ axis is parallel to the local vertical, defined by the $z_w$ axis of the LTF.
- Its $y_c$ axis is parallel to the wheel axis.

The position vector for $O_c$ is given by:

$\Delta h_{O_g} = h_{O_g} - h_{gnd}$\
${r_{O_gO_c}^c = r_{O_gO_c}^w = {\begin{pmatrix} 0 & 0 & \Delta h_{O_g} \end{pmatrix}}^{T}}$

When $r_{O_gO_c}^{c}[3]=0$, the tire is in contact with the ground, but it is supporting no weight, so that the strut is
exactly at its natural length and we have $O_c = O_g$. When $r_{O_gO_c}^{c}[3] < 0$, the tire is on the ground and the
strut is compressed. $O_c$ is above $O_g$ (which penetrates the ground), and it represents the tire-ground contact
point. When $r_{O_gO_c}^{c}[3] > 0$, $O_c$ is below $O_g$, there is no tire-ground contact and therefore no forces are
exerted by the ground.

To translate the forces and moments applied at the contact point (when there is actual contact), we need the position
vector from the airframe origin to the contact frame origin, projected in airframe axes:

$r_{O_bO_c}^b = r_{O_bO_g}^b + r_{O_gO_c}^b = r_{O_bO_g}^b + \left(R^w_b\right)^T r_{O_gO_c}^w$

$\xi$: Strut elongation. For simplification, the strut is assumed to be always vertically oriented (that is, parallel to
$z_w$ and $z_c$), so that only normal contact forces (not friction) contribute to strut compression. Under this
assumption we have $\xi = min(0, \Delta h_{O_g})$:
- $\forall \Delta h_{O_g} \ge 0, \xi = 0$
- $\forall \Delta h_{O_g} \lt 0, \xi = \Delta h_{O_g}$

Now, if $\xi > 0$, there is no contact and we're done. Otherwise, we continue.


### Computing the contact frame axes
The exact method for obtaining the attitude of $c$ is:
1. Define a proper strut frame $s$ as an arbitrary rotation of $b$.
2. Define the landing gear frame $g$ as the result of rotating $s$ an angle $\psi_{sg}$ around $z_s$, so that $y_g$ is
   aligned with the wheel axis.
3. Find the projection of unit vector $i_g$ onto the local horizontal plane. Obviously $i_g^g = e_1$. Therefore:
 ${i_g^w = R^w_g i_g^g = R^w_g e_1}$. Then, the contact steering angle is given by $\psi_{wc} = atan2(i_g^w(2), i_g^w(1))$.
4. Rotate $w$ an angle $\psi_{wc}$ around $z$ to yield $R^w_c$.

But, since the strut is already assumed to be verticalliy aligned, the attitude of the contact frame can be more easily
obtained as follows:
1. Extract the azimuth angle $\psi_{wb}$ of the LTF-to-body rotation.
2. Add the contact wheel steering angle $\psi_{bc}$ to yield the contact frame azimuth:
$\psi_{wc} = \psi_{wb} + \psi_{bc}$.
3. Rotate $w$ an angle $\psi_{wc}$ around $z$ to yield $R^w_c$.

### Computing $v_{eO_c}^c$

We also neglect the mass of the strut-wheel assembly. Since it has no inertia, $\dot{\xi}$ is given simply by
$v_{eO_c}^c$, which is in turn computed from the vehicle kinematics and the non-penetration condition of the contact
constraint.

The next step is to find $v_{eP}^c$, that is, the velocity of the contact point $P$ projected in the contact frame $c$.
The non-penetration constraint is then enforced by setting its vertical component to zero. From this, we shall find
$\dot{\xi}$

From $\xi$ and $\dot{\xi}$ we can determine the normal force. The horizontal velocity components shall allow us to
compute the friction force.

$r_{eO_c}^e = r_{eO_g}^e + r_{O_gO_c}^e$

$\dot{r}_{eO_c}^e = v_{eO_c}^e = \dot{r}_{eO_g}^e + \dot{r}_{O_gO_c}^e =$
$v_{eO_g}^e + R^e_c \left(\dot{r}_{O_gO_c}^c + \Omega_{ec}^c r_{O_gO_c}^c \right)$

Now, $\omega_{ec}^c = \omega_{en}^c + \omega_{nc}^c$. The transport rate $\omega_{en}^c$ is negligible in this context,
and $\omega_{nc}^c = {\begin{pmatrix} 0 & 0 & \dot{\psi}_{nc} \end{pmatrix}}^T \parallel r_{O_gO_c}^c$. Therefore, we can
write:

$v_{eO_c}^e \approx v_{eO_g}^e + R^e_c \dot{r}_{O_gO_c}^c$

$v_{eO_c}^c = v_{eO_g}^c + \dot{r}_{O_gO_c}^c$

$v_{eO_g}^c$ is found as follows:

$R^c_b = {R^w_c}^T R^w_b$

$v_{eO_g}^c = R^c_b v_{eO_g}^b= R^c_b \left(v_{eO}^b + \Omega_{eb}^b r_{OO_g}^b \right)$

While there is ground contact, we have:

$r_{O_gO_c}^c = {\begin{pmatrix} 0&0&\xi \end{pmatrix}}^T$

Thus:

$v_{eO_c}^c = v_{eO_g}^c + \dot{r}_{O_gO_c}^c= v_{eO_g}^c + {\begin{pmatrix} 0&0&\dot{\xi} \end{pmatrix}}^T$

From the above we see that the in-plane components of $v_{eO_c}^c$ are simply $v_{eO_c}^{x_c} = v_{eO_g}^{x_c}$ and
$v_{eO_c}^{y_c} = v_{eO_g}^{y_c}$.
Setting the vertical component to zero we find $\dot{\xi}$:

$e_3^T v_{eO_c}^c = e_3^T v_{eO_g}^c + \dot{\xi} = 0$

$\dot{\xi} = -e_3^T v_{eO_g}^c$

### Normal force
*Simplification: No aligning torques are experienced by the tire.*

*To do: more realistic strut force model with* $k_s(\xi)$ and $k_d(\dot{\xi})$.

The force exerted by the strut-tire assembly on the ground along $z_c$ is:

$F_{str,gnd}^{z_c} = - \left( k_s \xi + k_d \dot{\xi} \right)$

Reciprocally:

$F_{gnd,str}^{z_c} = - F_{str,gnd}^{z_c} = k_s \xi + k_d \dot{\xi}$

Due to the unilateral constraint, we must always have $F_{gnd,str}^{z_c} \le 0$. However, the above expression could
potentially become positive (for example, for small, negative $\xi$ and large, positive $\dot{\xi}$, which corresponds to
a quickly elongating but still compressed strut). To avoid this, we should clamp the result to be less or equal to zero.

We define the normal force as: $F_N = - F_{gnd,str}^{z_c} \ge 0$.

### Lateral Friction Coefficient

To compute the force along $y_c$ we start by computing the tire slip angle from the in-plane components of $v_{eO_c}^c$:

$\psi_{cv} = atan2(v_{eO_c}^{y_c}, v_{eO_c}^{x_c}) \in [-\pi, \pi)$ (assertion here)

The cornering friction coefficient $\mu_y$ is a function of the following parameters:
- $\mu_{skid}$: Skidding friction coefficient. Typical values: $\mu_{skid} = 0.5$ for dry surfaces, $\mu_{skid} = 0.15$
  for wet surfaces, $\mu_{skid} = 0.05$ for icy surfaces.

- $|\psi_{cv}|$: Absolute value of the tire slip angle. Note that $|\psi_{cv}| \leq \pi$.

- $\psi_{cv(skid)}$: Tire slip angle at which full skidding friction is reached. Typical value: $\psi_{cv(skid)} = 10°$.


The cornering friction coefficient is given by:

- $\mu_y = \mu_{skid} \dfrac{|\psi_{cv}|}{\psi_{cv(skid)}} ,$
$\forall |\psi_{cv}| \in [0, \psi_{cv(skid)})$

- $\mu_y = \mu_{skid},$
$\forall |\psi_{cv}| \in [\psi_{cv(skid)}, \pi - \psi_{cv(skid)}]$

- $\mu_y =\mu_{skid} \left( 1 - \dfrac{ \psi_{cv(skid)}-\left(\pi-|\psi_{cv}| \right)}{\psi_{cv(skid)}} \right) ,$
  $\forall |\psi_{cv}| \in (\pi - \psi_{cv(skid)}, \pi]$


### Longitudinal Friction Coefficient

The longitudinal friction coefficient $\mu_{x}$ is a function of three parameters:

- $\mu_{roll}$: Free (no braking) rolling friction coefficient for zero slip angle. Typical value: $\mu_{roll} = 0.02$.

- $\mu_{max}$: Maximum braking friction coefficient. Assuming anti-skid is available, its maximum theoretically
  achievable value is the skidding friction coefficient $\mu_{skid}$.

- $u_{brk}$: Braking intensity, with $u_{brk} \in [0, 1]$

Then, $\mu_{x}$ can be computed as:

$\mu_{x} = \mu_{roll} + \left( \mu_{skid} - \mu_{roll} \right) u_{brk}$

### Coefficient Scaling

From the above, we can see that both $\mu_x$ and $\mu_y$ have $\mu_{skid}$ as their maximum value. However, the
magnitude of the friction coefficient, both components considered, cannot exceed $\mu_{skid}$. This makes intuitive
sense. For example, if the tire slip angle is large enough that the tire is already fully skidding, applying the brakes
should make no difference to the overall friction coefficient. And, if the tire is only slipping, applying the brakes
should not allow the overall friction coefficient to surpass that of pure skidding.

$\mu_{mag} = \sqrt{\mu_x^2 + \mu_y^2}$

To keep $\mu_{mag} \leq \mu_{skid}$, the friction coefficients are scaled as follows:

$\mu_{x} := \mu_{x} \, \min \left( 1, \mu_{skid} / \mu_{mag} \right)$

$\mu_{y} := \mu_{y} \, \min \left( 1, \mu_{skid} / \mu_{mag} \right)$


### Friction Forces

The lateral and longitudinal friction forces are given respectively by:

$F_{gnd,str}^{y_c} = -\mu_y F_N \eta$

$F_{gnd,str}^{x_c} = -\mu_x F_N \eta$

Where we have introduced the smoothing factor:\
$\eta = \eta(v_t) = 1 - e^{-v_t / v_{t0}}$

Its purpose is to fade-out the friction forces as the in-plane velocity tends to zero, in order to avoid jittering when
the aircraft is not moving.


Since the restoring torque at the contact point has been neglected, the overall torque exerted on the ground is zero. We
can finally write the torque-force resultant acting on the strut as:

$$F_{gnd,str}^c = {\begin{pmatrix} F_{gnd,str}^{x_c} & F_{gnd,str}^{x_c} & F_{gnd,str}^{x_c} \end{pmatrix}}^T$$
$$T_{gnd,str[O_c]}^c = {\begin{pmatrix} 0 & 0 & 0 \end{pmatrix}}^T$$

### Improvements

Basicamente, el fade-out coefficient se comporta a muy baja velocidad como  una especie de controlador proporcional en
velocidad que intenta eliminar los errores metiendo fuerzas opuestas que son localmente proporcionales a la velocidad y
tienen un maximo en las fuerzas de friccion normales. Esto significa que el error estacionario en velocidad ante
perturbaciones externas no va a ser cero. Por tanto, habra creep.

Para corregir este comportamiento, una posible solucion seria meter un regulador PI en velocidad, que intente
permanentemente mantener la velocidad a cero, y cuyo output sea una variable entre -1 y 1, que multiplica al valor de mu
correspondiente a cada momento y determina el signo de la fuerza de friccion resultante.

Este regulador tendria que operar en dos ejes independientes. En cada eje, el output total (integral mas proporcional)
deberia saturar cuando en valor absoluto llegue a -1 o a 1. De esta manera, si yo de repente suelto el freno, aunque el
integrador este todavia cerca de la saturacion porque esta rechazando perturbaciones, el descenso en mu es inmediato y
produce la disminucion deseada en F_y. El output saturado del integrador es solo un factor de escalado que multiplica a
mu_y.

que pasa cuando inicialmente el avion toca tierra y se determina WoW por primera vez? pues que tipicamente  la velocidad
sera muy alta, de manera que el termino proporcional del controlador dominara, y sera suficiente como para llevar el mu
de ese eje hasta 1 o -1. al estar saturado el output, el termino integral no tendra tiempo de empezar a integrar. a
medida que el avion vaya frenando, llegara un momento en que la simple contribucion del termino proporcional no sera
bastante para mantener el output saturado. a partir de ahi, parte de su contribucion al output del controlador empezara
a pasar al termino integral. el controlador integral seguira saturado mientras haya velocidad. el problema es que, para
cuando la velocidad sea cero, todavia tendremos el output saturado, de modo que al pararse el avion, inmediatamente
empezara a retroceder durante unos instantes hasta que el error en velocidad opuesto haya sido suficiente para descargar
el integrador. naturalmente, a esto tambien ayudara el proporcional, que en principio deberia tener una ganancia mucho
mas grande. no obstante, para terminar de suavizar el comportamiento, lo suyo seria introducir un termino D en el
controlador.

El significado de la ganancia proporcional es basicamente: al decelerar, a partir de que velocidad quiero que el output
del controlador deje de estar saturado totalmente por la parte proporcional y empiece a estarlo parcialmente por el
integral?

Probar esto en Simulink con un modelo ultrasimplificado. Simular una masa puntual sometida a rozamiento y a
perturbaciones externas. Con posibilidad de aplicar frenado. Ver como se comporta en deceleracion, en estatico sometida
a perturbaciones, etc.

We have:\
$\dot{v} = 1/m (T + F_R + F_D)$\
$\dot{x} = v$\
$F_N = mg$\
$F_R = sgn(v) \, \mu \, F_N \, c(t)$\
$F_D = F_D(t)$\
$\mu = \mu_{roll} + \left( \mu_{skid} - \mu_{roll} \right) u_{brk}$\
$c(t) = k_p |v| + k_i \int{|v|dt} + k_d \dfrac{d|v|}{dt}$
