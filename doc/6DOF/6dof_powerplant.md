kin/pos\
kin/vel\
kin/rot/prp_l/omega\
act/srf/ail_l/x\
act/srf/ail_r/x\
act/ldg/nose/psi

states in kin/rot only deal with the kinematic state of the rotors. any powerplant might also have other states but they
will be in sys/

Pwp shall return also fuel flow and hand it to Fuel so the mdot for each tank can be propagated

Each aero surface will have a reference to an actuator, whose identifier must match its own. It must implement a
function that transforms actuator position to surface deflection, and this deflection must match the sign criterium in
the aerodynamics database. The aero surface will query the actuator for its current output (but not its state). So, the
Aero block that holds the aero surfaces must have access to the vehicle's actuator list

Each actuator will have a dynamic model which can be either direct input, first order or second order. If it's direct
input, it will have an associated block in u, but not in x. if it has actual dynamics, it will have blocks both in x and
u. It must implement a method x_dot = f(x, u) and another y = f(x, u). Every actuator needs to have a unique identifier.

En un generador de gas ideal, toda la energia se emplea en cambiar la energía y la cantidad de movimiento axial del
fluido. La cantidad de movimiento circunferencial a la entrada y a la salida del generador de gas no cambia (el
rotacional es cero en ambos casos), de lo que se deduce que el par neto experimentado por los sólidos en contacto con el
fluido durante este proceso debería ser cero. En la práctica, la corriente sale del generador de gas con una cierta
velocidad circunferencial residual, pero este es un efecto de segundo orden. Además, por supuesto, en las turbomáquinas
hay pérdidas de calor, disipación energética por fricción, etc, pero en principio esto no debería contribuir al par neto
sobre los sólidos que interactúan con el fluido.

Los álabes del estator del compresor recibirán un par del fluido al devolverlo a la dirección axial a la salida del
rotor. Recíprocamente, los álabes del estator de la turbina recibirán también un par del fluido al imprimirle una
velocidad circunferencial antes de entrar al rotor. Ambos pares serán de signo contrario, y en un generador de gas
ideal, de la misma magnitud.

Ahora bien, este equilibrio no existe cuando hay un transitorio, incluso en un generador de gas ideal. Aunque durante el
transitorio el generador de gas tampoco imprima velocidad circunferencial al fluido, sí que está incrementando (o
disminuyendo) el momento cinético del rotor compresor-eje-turbina. Y para que esto ocurra, la potencia extraída por la
turbina tiene que ser mayor que la consumida por el compresor. Y, puesto que está acelerando el rotor, esa potencia
tiene que ser rotacional. La potencia rotacional es igual al par por la velocidad angular. La velocidad angular es la
misma en compresor y turbina. Por tanto, el par del fluido sobre el rotor en la turbina tiene que ser mayor que en el
compresor. Y si la cantidad de movimiento circunferencial del fluido sigue sin variar a su paso por el generador de gas,
la única posibilidad es que el par del fluido sobre los álabes del estator de la turbina también sea mayor que sobre los
del compresor. Por tanto, hay un par neto no nulo, que obviamente será proporcional al producto de momento de inercia
por aceleración angular del rotor. Esto es exactamente lo que modela la ecuación

Cuando además del generador de gas tenemos un turbofan, los razonamientos anteriores siguen siendo vàlidos, porque el
fan y su etapa de turbina correspondiente lo único que hacen es transferir cantidad de movimiento axial del flujo
primario al flujo secundario. Cuando tenemos un turboprop, cualitativamente el razonamiento es el mismo: la turbina de
potencia extrae casi toda la cantidad de movimiento axial (y potencia) del flujo principal para transferirla a la
hélice, que acelera el fluido en dirección axial y genera empuje. No obstante, en la práctica, la eficiencia de la
hélice es mucho menor que la de una etapa de compresor, un fan o una turbina, en cuanto a que está mucho más lejos de
cumplir el ideal de mantener la velocidad circunferencial de la corriente a cero. La resistencia aerodinámica de las
palas induce una estela turbillonaria (vortex slipstream) en la hélice. Para imprimir esta cantidad de movimiento
circunferencial al fluido a la salida de la hélice, esta ha tenido que ejercer un par sobre él, y por tanto habrá
recibido un par recíproco. En condiciones estacionarias, con el eje de potencia a velocidad constante, el par del fluido
sobre la hélice es igual al par del fluido sobre los álabes del rotor de la turbina de potencia. Para poder convertir
cantidad de movimiento axial en par, los álabes del estator de la turbina primero tienen que generar velocidad
circunferencial. Esto significa que reciben par del fluido. Y puesto que a la salida de la turbina de potencia el fluido
no tiene cantidad de movimiento circunferencial, claramente este par tiene que ser igual al que el rotor obtiene del
fluido, que a su vez es igual al par resistente que sufre la hélice. Por tanto, el par resistente de la hélice se
traslada al airframe.

https://patents.google.com/patent/US3861139A/en

Conclusion: En operacion estacionaria, la potencia generada por un turbofan se basa en acelerar axialmente una corriente
de aire, no de generar potencia en un eje. En estas condiciones, el par sobre el airframe ejercido por un turbofan no
responde al metodo primario de generacion de potencia, sino a un efecto de segundo orden que a efectos de flight physics
es despreciable. Lo que no es despreciable es el momento cinético acumulado por el rotor. En transitorios, sí que hay un
par neto sobre el rotor. En cambio, en un turboprop, el par resistente que experimenta la hélice no es despreciable y se
transmite al airframe, aunque por un mecanismo diferente al que existe en un motor alternativo. Aquí por tanto es al
mismo tiempo posible

Por las razones anteriores, en motores turbofan, el acoplamiento entre la dinamica rotacional de powerplant y airframe
suele ser despreciable. Los modelos dinamicos no se expresan habitualmente en terminos de pares, y en su lugar se
presentan como una respuesta dinamica autocontenida. En cambio, para reciprocating y turboprops, si que se suelen dar
modelos de par de salida y, al haber helices, es posible modelar el par resistente y el empuje en terminos de la
velocidad angular. Ahora bien, incluso siendo despreciable el acoplamiento rotacional, en motores turbofan si conviene
tener en cuenta el momento angular acumulado por el motor.

Tanto para turbofan como para turboprop necesitaremos un modelo de respuesta dinámica del rotor, que será de primer
orden en omega. Su ecuación será: Jxx * omega_dot + J_sys_xs * omega_eb_b_dot = T_net

Cuando se trate de la hélice de un turboprop (o piston engine), entonces T_net se calculará como la diferencia entre el
par sobre la hélice y el par motor. Además, tendremos que considerar el external wrench que actúa sobre la hélice en las
ecuaciones dinámicas de la plataforma en su conjunto, que como he arguementado antes no es despreciable.

Cuando se trate de un eje de turbofan, entonces hay dos diferencias importantes:
1. Se supone que el external wrench sobre el propio eje es despreciable, y el cálculo del total external wrench (que en
   principio, según lo expuesto, constará sólo de empuje) correrá a cuenta del objeto Turbofan en sí.
2. El par neto T_net ya no se calculará como la diferencia entre el par sobre el eje y el par externo, ya que ambos son
   pequeños y ninguno de los dos es conocido. Calcularemos su valor como k * (omega_target - omega) para simular una
   respuesta de primer orden del turbofan a un comando de N1. Este valor lo proporcionará el objeto Turbofan. El tiempo
   de respuesta dependerá de la relación entre k y J_xx, así que lo suyo será obtener una J_xx razonable y fijar k para
   obtener la respuesta deseada.

En este caso, también tendremos modelado el efecto de los transitorios sobre la dinámica de la plataforma en su
conjunto, aunque realmente será despreciable.

En ambos casos de Rotor, obviamente hará falta un method J_xx para introducir en la solución de las ecuaciones
dinámicas, y otro h_xx_b para calcular la contribución al momento cinético total de la plataforma.

Definimos dos subclases de Rotor:

Propeller, para calcular el net shaft torque el rotor:
0) Extrae su omega de x/rot/label
2) Llama a su motor shaft asociado y recibe el T_shaft_xs(omega). Este T_shaft se lo habra calculado el motor como
   funcion de omega y u/pwp/label/thr
1) Llama a su propio method wrench para calcular el external wrench
2) De wrench extrae el T_ext_xs
3) T_net_xs = T_shaft_xs + T_ext_xs
4) Proporciona external wrench para meterlo en el wrench total sobre el vehiculo

Si es JetSpool para calcular el net shaft torque el rotor:
0) Extrae su omega de x/rot/label
2) Llama a su spool asociado y recibe directamente T_net_xs(omega). Este par se lo habra calculado el modelo de motor
   segun lo necesario para obtener la respuesta de primer orden k * (omega_dmd - omega). Y a su vez omega_dmd vendra de
   u/pwp/label/omega
1) El external wrench es despreciable
4) Proporciona external wrench para meterlo...

Ademas, necesitaremos una clase Engine, que podra ser Piston o Turboprop (por ahora)

A su vez, podemos definir una subclase de Propeller que sea  VariablePitchPropeller, cuya única diferencia es que el
cálculo del external wrench (y por tanto T_ext_xs) dependerá también del paso, no sólo de omega

La helice en funcion del paso y las RPMs, calcula su T_ext_xs. Tendra una referencia al actuador de paso.
El motor, en funcion de las vueltas y el throttle, calcula su T_eng_xs. Aqui no cambia nada. Este motor puede ser
InternalCombustion o Turboprop. El actuador de paso, en funcion del paso comandado y el actual, proporciona su x_dot

La unica cuestion que queda es: de donde viene el paso comandado? Pues del governor, que recibe la omega y la omega
comandada y comanda un paso de helice. En principio, el governor no tiene por qué ser un bloque externo al Aircraft,
puede tener sus propios states.

When a PropEng is instantiated, it must return a pair (label, SV) for a block to be added to kin/rot and
another pair (label, SV) for a block to be added to u/pwp.