Rules: each block output defines a signal. A signal must be a mutable object so that when they are assigned to
variables, references are created

Consider a model consisting of:
- A source generating an output signal $y_s(t)$.

- A system $a$ with dynamics given by:\
    $\dot{x}_a = f_a(t, x_a, u_a)$\
    $y_a = g_a(t, x_a, u_a)$\
    Its input $u_a$ is connected to $y_s$, so that $u_a(t) = s(t)$

- A system $b$ with dynamics given by:\
    $\dot{x}_b = f_b(t, x_b, u_b)$\
    $y_b = g_b(t, x_b, u_b)$\
    Its input $u_b$ is connected to $y_a$, so that $u_b(t) = y_a(t)$

How do we initialize? Steps:
- Set $x_a(t_0)$ and $x_b(t_0)$ (these are stored within their respective blocks)

Then propagate output signals.
- Evaluate the source to get $u_a(t_0) = y_s(t_0)$. $u_a$ is now updated.
- Evaluate $g_a$ to get $u_b(t_0) = y_a(t_0) = g_a(t_0, x_a(t_0), u_a(t_0))$. $u_b$ is now updated.
- Evaluate $g_b$ to get $y_b(t_0) = g_a(t_0, x_b(t_0), u_b(t_0))$. $y_b$ is now updated.

*Propagation of block outputs should be done iteratively, starting from the sources first, then checking each block
sequentially. If all its inputs have been updated, evaluate its outputs. If there are no algebraic loops or unconnected
inputs, this should be done in a few loops at most*

How do we propagate the model from $t_0$ to $t = t_1$?
## Euler method
$k_1 = f(t_0, x(t_0), u(t_0))$\
$x(t_1) = x(t_0) + \Delta t k_1$

First, we compute $k_1$. For this, we need the whole model to be updated to $t_0$. It already is, so we do the following:

- Assemble the overall model state $x(t_0)$ from each block's current state and save it.

- Call both blocks' $f$ method to get:

    $k_{1a} = \dot{x}_a(t_0) = f_a(t_0, x_a(t_0), u_a(t_0))$\
    $k_{1b} = \dot{x}_b(t_0) = f_b(t_0, x_b(t_0), u_b(t_0))$

- Assemble $k_1$ from $k_{1a}$ and $k_{2a}$

Once we have $k_1$:

- Propagate the model's internal states to $t_1$ as:\
    $x(t_1) = x(t_0) + \Delta t k_1$

- Assign the corresponding blocks of $x(t_1)$ to $x_a(t_1)$ and $x_b(t_1)$

Finally, with all the internal states at $x(t_1)$, we propagate the source, input and output signals exactly as we did
for initialization. When done, the whole model is at $t_1$, both states and outputs.

## Midpoint method
$k_1 = f(t_0, x(t_0), u(t_0))$\

$t_{[k2]} = t_0 + \Delta t /2$\
$x_{[k2]} = x(t_0) + \Delta t / 2 k_1$\
$u_{[k2]} = u(t_{[k2]})$

$k_2 = f(t_{[k2]}, x_{[k2]}, u_{[k2]})$\
$x(t_1) = x(t_0) + \Delta t k_2$

We start from the overall system at $t_0$.

We compute $k_1$ as above.

Now, from $k_1$ we can immediately compute $x_{[k2]}$. We do so at the model hierarchy level and then assign the
corresponding system state blocks. However, at this point all system inputs and outputs are still at $t_0$. To evaluate
$k_2$ we need not only the states at $x_{[k2]}$, but also all inputs at $u(t_{[k2]})$. So now we perform a model propagate
to $t_{[k2]}$ (evaluate sources at $t_{[k2]}$, and then propagate iteratively). With the model fully updated, we now compute
$k_2$ exactly as we did for $k_2$ and we save it. With $k_2$, we can compute $x(t_1)$. We then perform a final output
signal propagation.

Block inputs are nothing more than the model's signals, which in turn are actually block output signals. The $k$'s for
the different blocks are then assembled in an overall model $k$. Sources are nothing but a particular case of block!
They do not need to be handled separately. That said, sources are blocks whose update is guaranteed to be possible
independently of whether some other block has already updated. Therefore, it makes sense to place them at the update
priority list in order to avoid unnecessary additional loops.

---

## Initialize
x0 = model.x0(t0)\
model.propagate(t0) (evaluate and propagate all block outputs to bring the model to t0)

## One Midpoint step
k1 = model.f() \

x0 = model.x
t_k2 = t0 + 0.5 * dt \
x_k2 = x0 + 0.5 * dt * k1 \
model.x = x_k2 (*now states are at x_k2*) \
model.propagate(t_k2) (*now all inputs are also at x_k2 and we can compute k2*) \
k2 = model.f()

x1 = x0 + dt * k2 (*and we're done*)

## One RK4 step
k1 = model.f() \

x0 = model.x

*to compute k2 we need the complete model, states and input-outputs, at t2*\
t_k2 = t0 + 0.5 * dt \
x_k2 = x0 + 0.5 * dt * k1 \
model.x = x_k2 (*now states are at x_k2*) \
model.propagate(t_k2) (*now all inputs are also at u_k2 and we can compute k2*) \
k2 = model.f() \

t_k3 = t0 + 0.5 * dt \
x_k3 = x0 + 0.5 * dt * k2 \
model.x = x_k3 \
model.propagate(t_k3) \
k3 = model.f() \

t_k4 = t0 + dt \
x_k4 = x0 + dt * k3 \
model.x = x_k4 \
model.propagate(t_k4) \
k4 = model.f() \

x1 = x0 + dt/6 * (k1 + 2k2 + 2k3 + k4) (*and we're done*)

model.x = x1 \
model.propagate(t1)

So it seems that model.x() is always followed by model.propagate(). So we could define a single function update(t, x)
that first sets the model states to x, and then evaluates and propagates iteratively all block output for time t and the
just updated system states.

We cannot

**WRONG**: If instead of individual functions f and g for xdot and y we have a joint function h(), we can do the
following when update(t, x) is called:
- Set model state to x. All signals are still at t_prev, u_prev
- Evaluate xdot, y = h(t, x, u) for each block. And here it is: it is **impossible**! Because to evaluate xdot we
  first need t and u to be already updated, which in turn needs y(t, u)! So it needs to be done sequentially: first set
  states to x, then evaluate y(t, x) and propagate them so all block inputs are brought to its t, x values.


First try the concept with normal numpy arrays! Propagate creating views

How do we deal with discrete blocks? During the continuous update minor steps, we do nothing with them. Their output
signals should remain frozen at $t_0$ so we exclude them from the signal update calls. Once all continuous blocks have
been updated to t1, then we update the discrete block states. And finally we evaluate their outputs and perform a final
propagation, because these updated outputs can in turn affect the outputs of the continuous blocks via direct
feedthrough!

**Important**

Instead of manually propagating the model state throughout the individual systems (and viceversa) on each
step, we can create references between each system's local state and a views of the overall model state. To do this,
first assembly the XTypes for the whole model by calling recursively system.XType. Then, do the reverse: traverse the
tree from the root, assigning local block data to the appropriate views of the newly allocated model state vector.

**Questions**

Do we need to keep signals in a separate table? Or since each signal is created by the output of a system it
is enough to keep them stored as block outputs?

How does initialization work when there are feedthrough systems? See Joplin notes.