$a_{eO_b}^e = \dot{v}_{eO_b}^e = R^e_b \left( \dot{v}_{eO_b}^b + \Omega_{eb}^b v_{eO_b}^b\right)$\
$a_{eO_b}^b = \dot{v}_{eO_b}^b + \Omega_{eb}^b v_{eO_b}^b$