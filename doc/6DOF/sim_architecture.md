Notes:

## Simulation process

Runs the aircraft simulation model. Must run synchronously with the control process, nav and flight management
processes, which can use an exact multiple of its step size

sim step size: dt

ctl step size: dt * k_ctl, with k_ctl >= 1

display step size: dt * k_disp

### Simulation threads

Control definirá su T_ctl como múltiplo del período de simulación T. T debe estar disponible para todos los procesos.
Simulation no debe ocuparse del rate relativo a el al que se ejecutan los demas procesos. Sencillamente, a cada proceso
que necesite outputs, se los pasara en cada actualizacion que haga. Luego sera el proceso usuario el que ajustara su
propio rate relativo a base de diezmar los mensajes que recibe de Simulation. En la practica, fijara un ratio k,
definira (a efectos de calculos internos) su rate como k*T, y conseguira el rate relativo deseado ignorando todos los
mensajes procedentes de Sim excepto aquellos para los que mod(iter, k) == 0.

El enfoque anterior tiene un problema: no permite asegurar el determinismo en diferentes modos de ejecucion!! Si yo no
espero a estar seguro de haber recibido los control inputs que me corresponden, podria ser que el proceso Control
hubiera tardado mas en terminar justo en este ciclo, y cuando creo que estoy integrando con los inputs actualizados,
realmente no es asi. Por tanto, Sim debe esperar

Initialize simulation state and aircraft inputs

Main:
- If we're on a control epoch (mod(i, k_ctl)==0), block on input_update Event
- If we're on an control epoch (mod(i, k_ctl)==0), set the output_update Event. This will cause the Control process to
  wake up and start computing the next set of control inputs. However, they will not make it in time for the next
  integration, because we're immediately starting it. This means control inputs will always lag one sim step size
- Acquire u_lock and y_lock, then run a simulation update over a sim step size, write to y, (if configured) log it,
  release u_lock and y_lock
- If we are running in real time:
  - If we're on a display epoch (mod(i, k_disp)==0), set the draw_update Event.
  - Wait until the next step is due. And, if we are overdue, raise an Exception. This is probably due to having to wait
    for Control Inputs or the simulation taking too long.

Instead of using Locks and Events, the same behavior could be achieved more simply by defining threading queues to
exchange data with the Input, Output and Draw threads, instead of Events and Locks. On the appropriate epochs, we read
and write from each queue.

Ojo: Si quisiera propagar de t1 a t2 con u(y1) aplicada, seria imposible ejecutar en paralelo. Al obtener y1,
tendria que pasarle y1 a Control, que se pondria a calcular u(y1), y mientras tanto Sim no haria nada porque estaria
esperando. Solo cuando Control calculara u(y1) Sim propagaria de t1 a t2. Y reciprocamente, mientras Sim propaga,
Control no tiene nada que hacer, porque lo siguiente que le van a pedir es u(y2), pero todavia no tiene y2.

Con la secuencia anterior, antes de pasar de t1 a t2, Sim le pasa y1 a Control, que empieza a calcular u(y1). Pero a
continuacion propaga de t1 a t2 sin esperar a u(y1). Por tanto, lo estara haciendo con u(y0). Es decir, lleva un ciclo
de delay. A cambio, puede ejecutar en paralelo la simulacion y el resto de funciones.

Input thread:
- Block waiting to receive input commands from the Control process on multiprocessing queue Ctl2Sim.
- When a new control input arrives, pop it from the queue, acquire u_lock, update u, release u_lock and set the
  input_update Event.

Output thread:
- Block until the output_update Event is set by Main.
- When unblocked, acquire y_lock and pilot_lock and extract from y the data required to construct the output message.
  Reset output_update, release the y_lock, and add the message to Sim2Ctl queue.

Draw thread:
- Block until the draw_update Event is set by Main.
- When unblocked, acquire y_lock and extract from y the data required to construct the draw message. Reset
  draw_update, release the y_lock, and send message to XPlane via an xpc interface

## Control

For now it only needs one thread.
- Block waiting to receive simulation outputs through Sim2Ctl.
- Request pilot inputs via xpc interface, block while they arrive. Pilot inputs could also be requested asynchronously,
  but this approach minimizes input lag.
- When pilot inputs are received, run and compute control inputs from pilot inputs and simulation outputs.
- Block while sending control inputs through Ctl2Sim.

  We can afford to block here, because we know that until this output messsage is not fully
  sent, the Sim process, which is our producer, will not proceed, so even if we started a new iteration, we would block
  waiting for inputs. This is not true for Simulation process, because the loop that propagates from t1 to t2 does not
  use u(y1), but u(y0), which is already known. Therefore, integration from t1 to t2 can proceed while u(y1) is
  computed. This is not true for Control. It receives y1 and starts computing u(y1) while Sim propagates from t1 to t2.
  Until u(y1) is computed and provided to Sim, Sim will not start a new iteration,  and therefore it will not pass y2 to
  Control (and then propagate from t2 to t3). Therefore, completely sending u(y1) is necessary for Control to expect its
  next input y2.



## Navigation process

Dummy process: Simply gets the Sim state
