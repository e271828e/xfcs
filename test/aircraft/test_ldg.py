import pytest
import numpy as np
import xFCS.aircraft.rigidbody as rbd
import xFCS.environment.wgs84 as wgs84
from xFCS.environment.terrain import Flat
from xFCS.common.attitude import Attitude3D as Att
from xFCS.aircraft.landinggear import LandingGear

π = np.pi

def test_ldg_minimum():
    n_t = Att.from_euler((0, π/24, 0))
    terrain = Flat(h = 0, k_t_n = n_t.transform((0, 0, 1)))
    init = rbd.InitialCondition(
        Ob = wgs84.Point(h = .85),
        n_b = Att.from_euler(0, 0, 0),
        ω_wb_b = np.array((0., 0, 0)),
        v_eOb_b = np.array((0.007, 0, 0)))
    airframe = rbd.RigidBodyDynamics()
    airframe.initialize_state(initial_condition = init)
    y_kin = airframe.kinematics
    ldg = LandingGear()
    u = ldg.u
    u.brk = 0
    u.psi = π/24 * 0
    _, y = ldg.f_cont(t = 0, y_kin = y_kin, trn = terrain)
    print(y)
