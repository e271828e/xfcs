import pytest
import numpy as np
import xFCS.environment.wgs84 as wgs84
from xFCS.environment.wgs84 import Point

π = np.pi

def test_Point_from_geo():

    with pytest.raises(ValueError):
        Point.from_geo((π, 0, 0))

    with pytest.raises(ValueError):
        Point.from_geo((0, 3*π/2, 0))

    with pytest.raises(ValueError):
        Point.from_geo((0, 0, -100000))

    Point.from_geo((π/3, π/4, 1000)) #tuple
    Point.from_geo([π/3, π/4, 1000]) #list
    Point.from_geo(np.array([π/3, π/4, 1000])) #numpy array


def test_Point_from_nh():

    non_unit_vector = np.ones(3)
    unit_vector = non_unit_vector / np.linalg.norm(non_unit_vector)

    #non-unit n-vector should
    with pytest.warns(RuntimeWarning):
        P = Point.from_nh((non_unit_vector, 5000))

    #check that input is normalized
    assert np.linalg.norm(P._n_e) == 1

    Point.from_nh((unit_vector, 1000)) #tuple
    Point.from_nh(unit_vector, 1000) #separate arguments


def test_Point_from_LTFh():

    #define a point and a LTF azimuth angle
    P = Point.from_geo( (π/3, π/4, 0) )
    ψ = π/6

    #from P, get the (LTF(ψ), h) tuple, then use it to construct a new point and
    #extract ψ, and verify they match the originals
    assert (P, ψ) == Point.from_LTFh( P.LTFh(ψ) )


def test_Point_conversions():

    eps = 1e-8
    φ_array = np.linspace(-π/2 + eps, π/2 - eps, 8)
    λ_array = np.linspace(-π + eps, π - eps, 8)
    h_array = np.linspace(-200 + eps, 20000, 8)

    #test all Point factory methods and conversions for a complete range of
    #inputs, including corner cases
    for (φ, λ, h) in ( (φ, λ, h) for φ in φ_array for λ in λ_array for h in h_array ):

        P_initial = Point.from_geo((φ, λ, h))
        P_nh = Point.from_nh(P_initial.nh)
        P_r = Point.from_r_ECEF(P_nh.r_ECEF)
        P_LTFh, _ = Point.from_LTFh(P_r.LTFh( π/3) ) #arbitrary azimuth value
        P_final = Point.from_geo(P_LTFh.geo)

        assert(P_initial == P_final)

def test_Point_operators():

    # test subtraction
    P1 = Point.from_geo((0, π, 0)) #equator, lon=pi
    P2 = Point.from_geo((0, 0, 5000)) #equator, lon=0
    r_e_P1P2 = P2 - P1

    assert np.allclose(r_e_P1P2, np.array((2 * wgs84.a + 5000, 0, 0)))

    P3 = Point.from_geo((-π/2, 0, 0)) #south pole
    P4 = Point.from_geo((π/2, 0, 5000)) #north pole
    r_e_P3P4 = P4 - P3

    assert np.allclose(r_e_P3P4, np.array((0, 0, 2 * wgs84.b + 5000)))

    # test addition
    P2p = P1 + r_e_P1P2
    P4p = P3 + r_e_P3P4

    assert(P2 == P2p)
    assert(P4 == P4p)


def test_gravity():

    #sanity checks
    assert Point.from_geo(0, 0, 0).gravity[2] == wgs84.γ_a
    assert Point.from_geo(π/2, 0, 0).gravity[2] == wgs84.γ_b

    #quantitative check
    assert np.allclose(Point.from_geo(π/3, 0, 12000).gravity,
                       np.array((0, 0, 9.7822689975)))


def test_radii():

    #quantitative check
    assert np.allclose(np.array(Point.from_geo(π/3, 0, 0).radii),
                       np.array((6383453.857255, 6394209.17392684)))
