import pytest
import numpy as np
from collections import OrderedDict as OD
from xFCS.common.structuredvector import NodeType

#pylint: disable=protected-access

@pytest.fixture
def abc():

    NodeA = NodeType(name = 'A',
                         blocks = OD((
                             ('a', NodeType('Leaf', size = 2)),
                             ('b', NodeType('Leaf', size = 3)),
                             ('c', NodeType('Leaf', size = 4)),
                             ('d', NodeType('Leaf', size = 1)))))

    NodeB = NodeType(name = 'B',
                         blocks = OD((
                             ('a', NodeType('Leaf', size = 3)),
                             ('b', NodeType('Leaf', size = 5)),
                             ('c', NodeType('Leaf', size = 1)))))

    NodeC = NodeType(name = 'C',
                         blocks = OD((
                             ('a', NodeA),
                             ('b', NodeB))))

    return {'A': NodeA, 'B': NodeB, 'C': NodeC}

def test_sv_factory(abc):

    NodeA = abc['A']

    with pytest.raises(TypeError):
        NodeType(blocks = {'a': NodeA}) #must be an OrderedDict

    with pytest.raises(ValueError):
        NodeType(blocks = OD()) #needs either blocks or size

    with pytest.raises(ValueError):
        NodeType() #needs either blocks or size

    with pytest.raises(ValueError):
        NodeType(blocks = OD((('a', NodeA))), size = 2) #but not both

    with pytest.raises(ValueError):
        NodeType(size = 0) #size can't be zero

    with pytest.raises(ValueError):
        NodeType(blocks = OD((('for', NodeA)))) #no keywords as block names


def test_sv_init(abc):

    NodeC = abc['C']

    #data allocated on construction
    x = NodeC()
    assert (x._data == 0).all()

    #using a reference to external data
    x_data = np.arange(x.size)
    x = NodeC(x_data)
    assert x._data is x_data
    x._data[7] = 9237
    assert x_data[7] == 9237


def test_sv_operators(abc):

    NodeA = abc['A']
    NodeC = abc['C']

    c = NodeC()
    assert len(c) == c.size

    #test slice retrieval
    assert(c[:5] == c._data[:5]).all()

    #test slice assignment
    c[:] = np.arange(c.size)
    assert (c._data == np.arange(c.size)).all()
    c[3:6] = 0
    assert (c[3:6] == 0).all()

    #test direct

    #test __bool__
    assert c
    c[:] = 0
    assert not c

    #check invalid size assignment
    with pytest.raises(ValueError):
        c[:] = np.arange(c.size + 1)

    #test copy
    c1 = c.copy()
    c2 = c.copy()
    assert not c1._data is c2._data
    assert c1._children is c2._children

    #test equality
    assert c1 == c2
    c1[0] = 4
    assert c1 != c2

    #test in-place operators
    c1_copy = c1.copy()
    c1 += c2
    assert c1 == c1_copy + c2

    c1_copy = c1.copy()
    c1 -= c2
    assert c1 == c1_copy - c2

    c1_copy = c1.copy()
    c1 *= -7.3
    assert c1 == -7.3 * c1_copy

    #test operators
    c3 = c1 + c2
    assert c3 == c1 + c2

    c4 = c3 - c2
    assert c4 == c3 - c2

    c5 = 4.2 * c4 * 2.45 #right and left multiplication
    assert np.allclose(c5._data, 4.2 * 2.45 * c4._data)

    #check that repr is not broken
    print(c)

    #check iterability
    c_list = list(c)
    assert (type(c_list[0]) == NodeA)


def test_sv_getitem_setitem(abc):

    NodeA = abc['A']
    NodeB = abc['B']
    NodeC = abc['C']
    c = NodeC(data = np.arange(NodeC.size))

    with pytest.raises(KeyError):
        c['e']

    #test chained retrieval
    c_a = c['a']
    c_ab = c['a']['b']
    c_ac = c['a']['c']
    assert (c_ab == c_a['b'])

    #test chained assignment (first indexing operation calls __getitem__, last
    #one calls __setitem__)
    c['a']['c'] = 7*np.ones(c_ac.size)
    assert (c_ac == c['a']['c'])

    #test direct data assignment
    c['a']['b'][:] = 11*np.ones(len(c_ab))
    assert (c_ab == c['a']['b'])

    #test assignment with a compatible NodeType
    c['a'] = NodeA(data = 5*np.ones(NodeA.size))
    assert (c_a[:] == 5).all()

    with pytest.raises(ValueError):
        c_a += c_ab

    #test chained retrieval
    assert (c['a']['c'].size == 4)


def test_sv_getattr_setattr(abc):

    NodeA = abc['A']
    NodeB = abc['B']
    NodeC = abc['C']
    c = NodeC(data = np.arange(NodeC.size))

    with pytest.raises(KeyError):
        c.d

    #test chained retrieval
    c_a = c.a
    c_ab = c.a.b
    c_ac = c.a.c
    assert (c_ab == c_a.b)

    #test chained assignment (first indexing operation calls __getitem__, last
    #one calls __setitem__)
    c.a.c = 7*np.ones(c_ac.size)
    assert (c_ac == c.a.c)

    #test direct data assignment
    c_ac[:] = 11*np.ones(len(c_ac))
    assert (c_ac == c.a.c)

    #test assignment with a compatible NodeType
    c.a = NodeA(data = 5*np.ones(NodeA.size))
    assert (c_a[:] == 5).all()

    with pytest.raises(ValueError):
        c_a += c_ab

    #test chained retrieval
    assert (c.a.c.size == 4)


def test_sv_subclassing(abc):

    NodeA = abc['A']

    class NodeACustom(NodeA):

        def timestwo(self):
            return 2 * self._data

    a = NodeACustom(data = np.ones(NodeACustom.size))
    assert (a.timestwo() == 2).all()
