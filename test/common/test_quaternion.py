import pytest
import warnings
import numpy as np
from xFCS.common.quaternion import Quaternion as Quat

def test_quat_construction():
    
    #default constructor 
    q_null = Quat() 
    assert np.array_equal(q_null._data, np.zeros(4)) 

    #tuple, list, numpy array and component-wise construction
    q_tuple = Quat( (-1., 2., 1., 3.) )
    q_list = Quat( [-1., 2., 1., 3.] )
    q_array = Quat( np.array([-1., 2., 1., 3.]) )
    q_comp = Quat(-1., 2., 1., 3.)
    q_quat = Quat(q_tuple)

    assert np.array_equal(q_tuple._data, q_list._data)
    assert np.array_equal(q_tuple._data, q_array._data)
    assert np.array_equal(q_tuple._data, q_comp._data)
    assert np.array_equal(q_tuple._data, q_quat._data)
    
    #upcasting
    assert np.array_equal(q_tuple._data, Quat((-1, 2, 1, 3))._data)

    #incorrect input size
    with pytest.raises(ValueError):
        Quat(np.zeros(5))

    #incorrect number of positional arguments
    with pytest.raises(ValueError):
        Quat(1, 2, 3)

    #keyword arguments
    assert np.array_equal(Quat(real = 1.34)._data, Quat(1.34, 0, 0, 0)._data)
    assert np.array_equal(Quat(imag = (2, -1, 5))._data, Quat(0, 2, -1, 5)._data)
    assert np.array_equal(Quat(real = 1.34, imag = np.ones(3))._data, Quat(1.34, 1, 1, 1)._data)

@pytest.fixture
def q1():
    return  Quat( (-0.468, 2., 1., 3.) )

def test_quat_basics(q1):

    #representation
    print(q1)

    #copying
    q2 = q1.copy()
    assert (q2 is not q1)
    assert np.array_equal(q1._data, q2._data)

    #indexing
    q2[:3] = [-2.7, 0.736, 0.1]
    assert q2[0] == -2.7

    #real and imaginary parts    
    q1.real = -3.124
    assert q1[0] == -3.124
    q1.imag[1] = 0.847
    assert q1[2] == 0.847

def test_quat_unary(q1):

    #boolean
    assert q1
    assert not Quat() #null quaternion

    #negation
    assert np.array_equal((-q1)._data, -(q1._data))

    #conjugation
    assert q1.conj().real == q1.real
    assert np.array_equal( q1.conj().imag, -q1.imag )

    #norm and norm squared
    assert q1.norm == np.linalg.norm(q1._data)
    assert q1.norm_sq == np.linalg.norm(q1._data)**2

    #inversion
    q1_inv = q1.inv()
    assert q1_inv.real == q1.real / q1.norm_sq
    assert np.array_equal(q1_inv.imag, -q1.imag / q1.norm_sq) 
    with pytest.raises(ValueError):
        Quat(np.zeros(4)).inv()

    #normalization
    q1.normalize()
    assert q1.norm == 1
    with pytest.raises(ValueError):
        Quat(np.zeros(4)).normalize()

def test_quat_binary(q1):

    q2 = q1.copy()

    #equality
    assert q1 == q2
    assert not (q1 == q1 + q2)

    #addition and in-place addition
    assert np.array_equal((q1 + q2)._data, q1._data + q2._data)
    q1_copy = q1.copy()
    q1 += q2
    assert q1 == q1_copy + q2

    #subtraction and in-place subtraction
    assert np.array_equal((q1 - q2)._data, q1._data - q2._data)
    q1_copy = q1.copy()
    q1 -= q2
    assert  q1 == q1_copy - q2

    #multiplication and in-place multiplication by scalar
    assert q1 * 3.653 == Quat( 3.653 * q1._data )
    q1_copy = q1.copy()
    q1 *= 1.2452
    assert q1 == q1_copy * 1.2452

    #right multiplication by scalar
    assert 3.653 * q1 == q1 * 3.653

    #division and in-place division by scalar
    assert q1 / 3.653 == Quat( q1._data / 3.653)
    q1_copy = q1.copy()
    q1 /= 1.2452
    assert q1 == q1_copy / 1.2452
    
    #multiplication and in-place multiplication by quaternion
    q3 = q1 * q2
    assert q3.real == q1.real * q2.real - np.dot(q1.imag, q2.imag)
    assert np.array_equal(q3.imag, q1.real * q2.imag + q2.real * q1.imag + np.cross(q1.imag, q2.imag))
    q1_copy = q1.copy()
    q1 *= q2
    assert q1 == q1_copy * q2

    #quaternion multiplication properties
    assert q1 * q1.inv() == Quat(real = 1)
    assert (q1 * q2).conj() == q2.conj() * q1.conj() 
    assert (q1 * q2) * q3 == q1 * (q2 *q3)
    assert (q1 + q2) * q3 == q1 * q3 + q2 * q3

    #division and in-place division by quaternion
    assert q1 / q2 == q1 * q2.inv()
    assert q1 / q1 == Quat(real = 1)
    q1_copy = q1.copy()
    q1 /= q2
    assert q1 == q1_copy / q2