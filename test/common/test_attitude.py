import pytest
import numpy as np
from xFCS.common.attitude import Attitude3D as Att3D
from xFCS.common.attitude import v2skew
from xFCS.common.quaternion import Quaternion as Quat

π = np.pi

def test_attitude_construction():

    #default constructor yields a null rotation, which corresponds to the identity quaternion
    r_ab = Att3D()
    assert r_ab == Att3D.null()
    assert r_ab._r == Quat(real = 1)

    #copying produces a new instance
    r_ab_copy = r_ab.copy()
    r_ab_copy._r = Quat(real = 0, imag = [-1, 0, 0])
    assert r_ab._r == Quat(real = 1) #original instance is unchanged

    #assignment produces a reference
    r_ab_alias = r_ab
    r_ab_alias._r = r_ab_copy._r
    assert r_ab._r == r_ab_copy._r #original instance is also changed

def test_attitude_operators():

    r_ab = Att3D.from_euler(0.847, 1.3, -0.34)
    r_bc = Att3D.from_euler(-0.347, -1.123, -0.64)

    #__repr__
    print(r_ab)

    #renormalization
    r_ab._r *= 2
    assert abs(r_ab.renorm()._r.norm - 1) < 1e-16

    #boolean operator should test True except for the null rotation
    assert r_ab
    assert not Att3D()

    #objects with identical quaternions should test equal
    r_ab_copy = r_ab.copy()
    assert r_ab_copy == r_ab

    #and also if all quaternion components are within _eps_eq of their counterparts
    r_ab_copy._r = r_ab._r + Quat(np.full(4, 0.5 * Att3D._eps_eq))
    assert r_ab_copy == r_ab

    #objects should test unequal if any quaternion component is outside the equality threshold
    r_ab_copy._r = r_ab._r + Quat(0, 0, 0, 1.5 * Att3D._eps_eq)
    assert r_ab_copy != r_ab

    #unit quaternions are a double cover of SO(3) and this should be accounted for by the equality operator
    r_ab_copy._r = -r_ab._r
    assert r_ab_copy == r_ab

    #assign class method inv() to local variable inv to enable the more compact syntax inv(q_ba)
    inv = Att3D.inv

    #define vector x by its coordinates in target frame c
    x_c = np.array((9.2, -2.045, 1.34546))
    x_b = r_bc.transform(x_c)
    x_a = r_ab.transform(x_b)

    #coordinate transform & inversion
    r_cb = inv(r_bc)
    assert not r_bc * r_cb #multiplying an attitude by its inverse should yield a null rotation, which tests False
    assert np.allclose(x_c, r_cb.transform(x_b)) #the inverse transformation should restore the original coordinates

    #transform & composition
    r_ac = r_ab * r_bc
    assert np.allclose(x_a, r_ac.transform(x_c))

    #in place composition
    r_ab_copy = r_ab.copy()
    r_ab *= r_bc
    assert r_ab == r_ab_copy * r_bc

    #quaternion time derivative (sanity check; this can only be tested rigorously in integration)
    r_ab = Att3D()
    omega_b_ab =np.array([ .3, -2, .4 ])
    assert r_ab.quat_dot(omega_b_ab) == 0.5 * r_ab.quat * Quat(imag = omega_b_ab)
    assert np.all(r_ab.rmat_dot(omega_b_ab) == r_ab.rmat @ v2skew(omega_b_ab))
    assert np.all(r_ab.euler_dot(omega_b_ab) ==  np.array([omega_b_ab[2], omega_b_ab[1], omega_b_ab[0]]))

def test_attitude_factory():

    assert Att3D.from_quat(Quat(imag = (0, 1, 0)))

    #non-unit quaternion input should prompt a warning and force normalization
    with pytest.warns(RuntimeWarning):
        r_nonunit = Att3D.from_quat(Quat(2, 1, -2, 4))
    assert r_nonunit._r.norm == 1

    #...unless validation is bypassed
    r_nonunit = Att3D.from_quat(Quat(2, 1, -2, 4), bypass_val = True)
    assert r_nonunit._r.norm != 1

    #rotation matrix factory method only accepts a matrix if its determinant is close enough to 1
    Att3D.from_rmat( np.array( [ [1 + 0.5 * Att3D._eps_unit, 0, 0], [0, 1, 0], [0, 0, 1] ]) )

    #and rejects it otherwise
    with pytest.raises(ValueError):
        Att3D.from_rmat( np.array( [ [1 + 1.5 * Att3D._eps_unit, 0, 0], [0, 1, 0], [0, 0, 1] ]) )

    #it accepts a nested list of the appropriate shape
    Att3D.from_rmat([ [1, 0, 0], [0, 1, 0], [0, 0, 1] ])

    #the axis-angle factory method expects a tuple containing a unit vector (as
    #an iterable) and a scalar
    Att3D.from_axis_angle( (0, 0, -1 + 0.5 * Att3D._eps_unit), π/3 )
    with pytest.raises(ValueError):
        Att3D.from_axis_angle( (0, 0, -1 + 1.5 * Att3D._eps_unit), π/3 )

    #Euler factory method accepts either a sequence or individual arguments,
    Att3D.from_euler( π/3, -π/6, π/2 )
    Att3D.from_euler( [ π/3, -π/6, π/2 ] )

    #it enforces inclination and bank bounds
    with pytest.raises(ValueError):
        Att3D.from_euler( 0, π, 0 )
    with pytest.raises(ValueError):
        Att3D.from_euler( 0, 0, 1.5*π )

    #rotation vector constructor admits any 3-element sequence
    Att3D.from_rvec( (1, 2, 3) )

    #coordinate change constructor correctness
    r_ab = Att3D.from_rvec( ( -1, 2, 3 ) )
    x_b = ( 12, -3.54, -6.234 )
    x_a = r_ab.transform(x_b)
    Att3D.from_coord_change(x_a, x_b)
    assert np.allclose( x_a, r_ab.transform(x_b) )

    #it should not accept arrays with different norms
    with pytest.raises(ValueError):
        Att3D.from_coord_change( x_a + np.full(3, 2 * Att3D._eps_unit), x_b )

def test_attitude_conversions():
    eps = 0
    ψ_array = np.linspace(-π + eps, π - eps, 8)
    θ_array = np.linspace(-π/2 + eps, π/2 - eps, 8)
    φ_array = np.linspace(-π + eps, π - eps, 8)

    for (ψ, θ, φ) in ( (ψ, θ, φ) for ψ in ψ_array for θ in θ_array for φ in φ_array ):

        r_0 = Att3D.from_euler(ψ, θ, φ)

        r_1 = Att3D.from_euler(r_0.euler)
        r_2 = Att3D.from_rmat(r_1.rmat)
        r_3 = Att3D.from_axis_angle(r_2.axis_angle)
        r_4 = Att3D.from_rvec(r_3.rvec)

        assert r_0  == r_4