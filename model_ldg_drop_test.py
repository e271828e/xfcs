import numpy as np

import xFCS.environment.wgs84 as wgs84
from xFCS.aircraft.aircraft import DropTestRig
from xFCS.aircraft.rigidbody import InitialCondition
from xFCS.common.attitude import Attitude3D as Att
from xFCS.environment.terrain import Flat
from xFCS.simulation.blocks.aircraft import AircraftBlock
from xFCS.simulation.blocks.general import Constant
from xFCS.simulation.engine import Signal, Model

# claramente, el rozamiento disipa energia: cuando termine la caida, la energia
# elastica almacenada por el shock absorber solo sera una parte de la energia
# potencial inicial. esta es la vision energetica, que es muy clara. la
# existencia de friccion es similar a la de un termino de damping en el
# amortiguador, pero que solo proyecta parcialmente en la direccion del strut,
# no paralelamente

# al tener damping en el amortiguador, la fuerza del suelo sobre el shock
# absorber es mayor que si no lo hubiera, de manera que el descenso se para
# antes de que el muelle se haya comprimido lo que tendria que haberse
# comprimido para almacenar toda la energia potencial inicial. con el rozamiento
# ocurre lo mismo: durante la caida, la fuerza del suelo sobre el amortiguador
# es mayor, porque el rozamiento proyecta en la direccion del mismo (mientras
# que si el strut estuviera vertical, no proyectaria).

π = np.pi

trn = Flat(h = 0)
aircraft = DropTestRig(splay_angle= π/15, trn = trn)

init = InitialCondition(
    Ob = wgs84.Point(h = 1.5),
    n_b = Att.from_euler(0, 0, 0),
    ω_wb_b = np.array((0., 0, 0)),
    v_eOb_b = np.array((0, 0, 0)))

u = aircraft.u
for u_leg in u.ldg:
    u_leg.psi = 0

aircraft_block = AircraftBlock(aircraft = aircraft)

m = Model(dt = 1e-2, t_end = 10)
m.add_block('dummy', Constant(0)) #need to define this, because Aircraft requires an input
m.add_block('aircraft', aircraft_block)
m.add_signal('aircraft_out', Signal(logging = True))
m.connect(m.blocks['dummy'].get_output(), m.blocks['aircraft'].get_input())
m.connect(m.blocks['aircraft'].get_output(), m.signals['aircraft_out'])

m.run(profiled = False)
log = m.signals['aircraft_out']._log
save_path = "tmp\\droptest_new"
aircraft.plot(log, save_path = None, keep_open=False)