from xFCS.simulation.blocks.io import XBoxOne, XBoxOne2GndCtl
from xFCS.simulation.engine import Model, Signal

m = Model(t_end = 5)
m.add_block('xboxone', XBoxOne())
m.add_block('adapter_new', XBoxOne2GndCtl())
m.add_signal('adapter_out', Signal())
m.connect(m.blocks['xboxone'].get_output(), m.blocks['adapter_new'].get_input())
m.connect(m.blocks['adapter_new'].get_output(), m.signals['adapter_out'])
#inspect adapter behaviour with a print statement

m.run(profiled = False, real_time = True)
