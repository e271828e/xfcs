import numpy as np

import xFCS.environment.wgs84 as wgs84
from xFCS.aircraft.aircraft import AircraftGeneric
from xFCS.aircraft.rigidbody import InitialCondition
from xFCS.common.attitude import Attitude3D as Att
from xFCS.environment.atmosphere import Atmosphere
from xFCS.environment.terrain import Flat
from xFCS.io.xpinterfaces import SimInterface
from xFCS.simulation.blocks.aircraft import AircraftGndCtlBlock
from xFCS.simulation.blocks.io import XBoxOne, XBoxOne2GndCtl, XPlaneDisplay
from xFCS.simulation.engine import Model, Signal

#start X-Plane scenario here
sim = SimInterface()
xp_loc = sim.get_state(('lat', 'lon', 'alt', 'psi', 'theta', 'phi'))

#define terrain altitude as a negative offset with respect to the initial
#altitude matching the resting length of the strut (supporting weight)$
trn = Flat(xp_loc['alt'] - 0.9)
atm = Atmosphere()
aircraft = AircraftGeneric(trn, atm)

#get the at-rest x-plane altitude and use it for initialization
init = InitialCondition(
    Ob = wgs84.Point.from_geo(xp_loc['lat'], xp_loc['lon'], xp_loc['alt'] + 0.25),
    n_b = Att.from_euler(xp_loc['psi'], 0.1, 0.05),
    ω_wb_b = np.array((0., 0, 0)),
    v_eOb_b = np.array((30, 0, 0)))
aircraft.airframe.initialize_state(initial_condition = init)

m = Model(t_end = 30)

m.add_block('xboxone', XBoxOne())
m.add_block('adapter', XBoxOne2GndCtl())
m.add_block('display', XPlaneDisplay())
m.add_block('aircraft', AircraftGndCtlBlock(aircraft = aircraft))
m.add_signal('aircraft_out', Signal(logging = True))

m.connect(m.blocks['xboxone'].get_output(), m.blocks['adapter'].get_input())
m.connect(m.blocks['adapter'].get_output(), m.blocks['aircraft'].get_input())
m.connect(m.blocks['aircraft'].get_output(), m.signals['aircraft_out'])
m.connect(m.signals['aircraft_out'], m.blocks['display'].get_input())

m.run(profiled = True, real_time = True)

log = m.signals['aircraft_out']._log
save_path = "tmp\\interactive_test"
aircraft.plot(log, save_path = save_path , keep_open=False)
