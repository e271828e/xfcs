import numpy as np
from warnings import warn
from xFCS.common.quaternion import Quaternion as Quat
from xFCS.common.math import norm, cross, v2skew

π = np.pi

class Attitude3D:

    """
    Describes the attitude between two reference frames in 3D space, and
    provides methods for converting to and from different representations, and
    performing composition, inversion and vector transformation. Implementation
    is quaternion-based and relies on the Quaternion class provided by module
    quaternion.py. Units are SI unless otherwise specified.
    """

    #error threshold for unit quaternion norm and rotation matrix determinant
    _eps_unit = 1e-12
    #equality threshold for quaternion components
    _eps_eq = 1e-12
    #small rotation threshold for switching to approximate solutions
    _eps_tiny = 1e-12

    def __init__(self, quat = Quat(real=1), bypass_val = False):
        """
        Default constructor. New instances should be created instead
        using the provided class methods.

        Args:

        quat (Quaternion): Quaternion object with unit norm.

        bypass_val (bool): Flag to bypass input validation. When set to False,
        quaternion norm is checked. If it is outside the tolerance defined by
        _eps_unit, it is renormalized and a warning is issued.

        """

        if not bypass_val:

            if not isinstance(quat, Quat):
                raise TypeError("Input to constructor must be a quaternion. \
                    For other input types, use the provided classmethods.")

            if abs(quat.norm - 1) > self._eps_unit:
                warn("Quaternion norm outside tolerance, normalizing...", RuntimeWarning)
                quat.normalize() #if r is null, Quaternion will raise ValueError

        self._r = quat


    @classmethod
    def null(cls):
        """
        Creates an instance corresponding to a null rotation.
        """
        return cls( Quat(real=1), bypass_val = False )


    @classmethod
    def from_quat(cls, quat, bypass_val = False):
        """
        Creates an instance from a unit Quaternion object.

        Tries to cast its input into Quaternion and passes it to the
        constructor.
        """
        return cls( Quat(quat), bypass_val )


    @classmethod
    def from_rmat(cls, rmat, bypass_val = False):
        """
        Creates an instance from a rotation matrix.

        Args:

        rmat (numpy.ndarray of shape (3,3)): Rotation matrix.

        bypass_val (bool): Flag to bypass input validation. When set to False,
        the input matrix's shape and determinant are checked.
        """

        try:
            rmat = np.array(rmat, dtype = float)
        except ValueError:
            raise ValueError("Input must yield a numpy array of shape (3,3)")

        if not bypass_val:
            if np.shape(rmat) != (3, 3):
                raise ValueError("Input must yield a numpy array of shape (3, 3)")
            if abs(np.linalg.det(rmat) - 1) > cls._eps_unit:
                raise ValueError("Input matrix must be proper orthogonal")

        #find the maximum amongst the absolute values of quaternion components
        tr_rmat = np.trace(rmat)
        max_abs_q = np.array((tr_rmat, rmat[0,0], rmat[1,1], rmat[2,2])).argmax()

        #construct the corresponding quaternion candidate
        if max_abs_q == 0:
            q_array = np.array((
                1 + tr_rmat,
                rmat[2,1] - rmat[1,2],
                rmat[0,2] - rmat[2,0],
                rmat[1,0] - rmat[0,1] ))

        elif max_abs_q == 1:
            q_array = np.array((
                rmat[2,1] - rmat[1,2],
                1 + 2*rmat[0,0] - tr_rmat,
                rmat[0,1] + rmat[1,0],
                rmat[2,0] + rmat[0,2] ))

        elif max_abs_q == 2:
            q_array = np.array((
                rmat[0,2] - rmat[2,0],
                rmat[0,1] + rmat[1,0],
                1 + 2*rmat[1,1] - tr_rmat,
                rmat[1,2] + rmat[2,1] ))

        else:
            q_array = np.array((
                rmat[1,0] - rmat[0,1],
                rmat[2,0] + rmat[0,2],
                rmat[1,2] + rmat[2,1],
                1 + 2*rmat[2,2] - tr_rmat ))

        #normalize the chosen candidate and call the constructor
        return cls( Quat(q_array / norm(q_array)), bypass_val = True )


    @classmethod
    def from_euler(cls, *euler, bypass_val = False):
        """
        Creates an instance from a ZYX sequence of Euler angles.

        Rather than a one-step Euler-to-quaternion conversion, it uses elemental
        rotation composition, which is slightly less efficient but much more
        accurate for extreme inclinations.

        Args:

        euler (3-element sequence or three scalar floats): Azimuth (ψ),
        inclination (θ), bank (φ).

        bypass_val (boolean): Flag to bypass input validation. When set to
        False, the ranges for the inputs are checked.
        """

        n_args = len(euler)

        if n_args == 1:
            ψ, θ, φ = euler[0]
        elif n_args == 3:
            ψ, θ, φ = euler
        else:
            raise ValueError("Euler angles must be provided either as a \
                3-element sequence or as 3 separate arguments")

        if not bypass_val:
            #not necessary, it can be defined between 0 and 2pi
            # if abs(ψ) > π:
            #     raise ValueError("Heading must be within [-π, π], input: " + str(ψ))
            if abs(θ) > π/2:
                raise ValueError("Inclination must be within [-π/2, π/2], input: " + str(θ))
            if abs(φ) > π:
                raise ValueError("Bank must be within [-π, π], input: " + str(φ))

        r_ψ = cls.from_axis_angle( [0, 0, 1], ψ, bypass_val = True)
        r_θ = cls.from_axis_angle( [0, 1, 0], θ, bypass_val = True)
        r_φ = cls.from_axis_angle( [1, 0, 0], φ, bypass_val = True)

        return r_ψ * r_θ * r_φ


    @classmethod
    def from_axis_angle(cls, *args, bypass_val = False):
        """
        Creates an instance from an axis-angle pair.

        Args:

        *args (2-element sequence or two separate arguments): Axis must be
        provided as a 3-element sequence yielding a unit vector. Angle must be a
        scalar.

        bypass_val (boolean): Flag to bypass input validation. When set to False,
        the ranges for the inputs are checked.
        """

        n_args = len(args)

        if n_args == 1:
            n, μ = args[0] #as a 2-element sequence
        elif n_args == 2:
            n, μ = args #as two separate arguments
        else:
            raise ValueError("Received more than two positional arguments.\
                Axis and angle must be provided as a 2-element \
                sequence or as two separate arguments")

        n = np.array(n, dtype=float)

        if not bypass_val:
            if np.shape(n) != (3,) or abs(norm(n) - 1) > cls._eps_unit:
                raise ValueError("Axis must be a 3-element unit vector")

        return cls(
            Quat(real = np.cos(μ/2), imag = n * np.sin(μ/2)),
            bypass_val = True)


    @classmethod
    def from_rvec(cls, rvec):
        """
        Creates an instance from a rotation vector.

        Exact computation becomes ill-defined as the rotation vector norm
        approaches 0 (which corresponds to a null rotation). To avoid numerical
        issues, when the norm is below threshold _eps_tiny, the method switches
        to a second-order Taylor approximation.

        Args:

        rvec (3-element sequence): Rotation vector.
        """

        ρ = np.array(rvec, dtype=float)
        norm_ρ = norm(ρ)

        if norm_ρ > cls._eps_tiny:
            return cls(
                Quat(real = np.cos(0.5 * norm_ρ),
                     imag = np.sin(0.5 * norm_ρ) * (ρ / norm_ρ)),
                bypass_val=True)

        else:
            norm_ρ_sq = norm_ρ ** 2
            return cls(
                Quat(real = 1 - 1/8 * norm_ρ_sq,
                     imag = 0.5 * ρ * (1 - 1/24 * norm_ρ_sq)),
                bypass_val=True)


    @classmethod
    def from_coord_change(cls, x_a, x_b, bypass_val = False):
        """
        Creates an instance from the coordinates of an arbitrary vector in two
        distinct reference frames.

        Given the coordinates of vector x in frames a (x_a) and b (x_b), this
        method yields an instance q_ab corresponding to the minimum-angle
        rotation from a to b, such that x_a == q_ab.transform(x_b). If the angle
        is below _eps_tiny, it returns a null rotation.

        Args:

        x_a (3-element sequence): Coordinates of x in frame a.
        x_b (3-element sequence): Coordinates of x in frame b.
        bypass_val (boolean): Flag to bypass input validation. If set to False,
        the norms of x_a and x_b are checked for equality.

        """

        x_a = np.array(x_a, dtype=float)
        x_b = np.array(x_b, dtype=float)

        if not bypass_val:
            if abs(norm(x_a) - norm(x_b)) > cls._eps_eq:
                raise ValueError("Norm discrepancy: Input arrays must \
                    represent coordinates of the same vector, and therefore \
                    they must have equal norms")

        cross_b_a = cross(x_b, x_a)
        norm_cross_b_a = norm(cross_b_a)

        if norm_cross_b_a < cls._eps_tiny:
            return cls.null()

        else:
            n_ab = cross_b_a / norm_cross_b_a
            μ_ab = np.arctan2(norm_cross_b_a, np.dot(x_b, x_a))
            return cls.from_axis_angle(n_ab, μ_ab)


    def copy(self):
        """
        Native copy method which avoids reliance on the copy module.
        """
        new_obj = self.__new__(self.__class__)
        new_obj._r = self._r.copy()
        return new_obj


    def renorm(self):
        """
        Quaternion renormalization for correcting accumulated numerical errors.
        """
        self._r.normalize()
        return self


    def __repr__(self):
        """
        Representation is delegated to the Quaternion class.
        """
        return ("<" + type(self).__name__ + "(" + self._r.__repr__() + ")>")


    def __bool__(self):
        """
        Boolean operator overload. Returns False for the null rotation, and True
        in any other case. Provides a convenient way to check for null rotations.
        """
        return self != self.null()


    def __eq__(self, other, eps = _eps_eq):
        """
        Equality operator overload. It returns True if both quaternions are
        equal within a sign change. This accounts for the unit quaternions'
        double cover of SO(3).
        """
        if self._r.__eq__(other._r, eps) or self._r.__eq__(-other._r, eps):
            return True
        else:
            return False


    def __imul__(self, other):
        """
        In-place multiplication overload. Provides in-place rotation
        composition through quaternion multiplication.
        """
        self._r *= other._r
        return self


    def __mul__(self, other):
        """
        Multiplication overload. Provides rotation composition through
        quaternion multiplication
        """
        new_obj = self.copy()
        new_obj *= other
        return new_obj


    def inv(self):
        """
        Implements rotation inversion through quaternion conjugation.
        """
        new_obj = self.copy()
        new_obj._r.imag = -self._r.imag
        return new_obj


    def transform(self, x_b):
        """
        Transforms a vector from target frame to initial frame coordinates.

        If self defines the rotation from frame a to frame b, this method
        returns the a-frame coordinates of an arbitrary vector x given its
        b-frame coordinates, so that x_a == self.transform(x_b)

        Args:

        x_b (3-element sequence): Coordinates of x in frame b.

        Returns:

        x_a (3-element sequence): Coordinates of x in frame a.
        """

        x_b = np.array( x_b, dtype=float )
        r_ab = self._r
        x_a = x_b + 2 * cross(
            r_ab.imag,
            r_ab.real * x_b + cross(r_ab.imag, x_b))

        return x_a


    @property
    def quat(self):
        """
        Returns the quaternion representation of self as a Quaternion instance.
        """
        return self._r


    @property
    def axis_angle(self):
        """
        Axis-angle representation.

        If the rotation angle is below _eps_tiny, the axis is not computed and
        None is returned instead.

        Returns: Tuple containing the axis as a numpy array of shape (3,) and
        the angle.
        """

        r_imag = self._r.imag
        norm_r_imag = norm(r_imag)
        μ = 2 * np.arctan2(norm_r_imag, self._r.real)

        if norm_r_imag > self._eps_tiny:
            n = r_imag / norm_r_imag
        else:
            n = None

        return n, μ


    @property
    def rvec(self):
        """
        Rotation vector representation.

        The method first attempts to construct the rotation vector from the
        axis-angle representation. If the angle is too small and the axis_angle
        property getter fails to return the axis, the implementation switches to
        a second-order Taylor approximation.

        Returns: Rotation vector as a numpy array of shape (3,).
        """

        n, μ = self.axis_angle

        if n is not None: #non-null rotation, axis_angle successfully found the axis
            ρ = μ * n
        else: #use Taylor series approximation
            ρ = (2 + norm(self._r.imag)**2 / 3) * self._r.imag

        return ρ


    @property
    def rmat(self):
        """
        Rotation matrix representation.

        Returns: Rotation matrix as a numpy array of shape (3,3).
        """

        r = tuple(self._r)
        r_sq = tuple(x**2 for x in self._r)
        d_r12, d_r13, d_r23 = 2*r[1]*r[2], 2*r[1]*r[3], 2*r[2]*r[3]
        d_r01, d_r02, d_r03 = 2*r[0]*r[1], 2*r[0]*r[2], 2*r[0]*r[3]

        rmat = np.array([
            [1 - 2*(r_sq[2] + r_sq[3]), d_r12 - d_r03, d_r13 + d_r02],
            [d_r12 + d_r03, 1 - 2*(r_sq[1] + r_sq[3]), d_r23 - d_r01],
            [d_r13 - d_r02, d_r23 + d_r01, 1 - 2*(r_sq[1] + r_sq[2])]
            ])

        return rmat


    @property
    def euler(self):
        """
        Euler angle representation.

        The ZYX sequence returned consists of azimuth (ψ), inclination (θ) and
        bank (φ). For θ == π/2 and θ == -π/2, ψ and φ cannot be independently
        determined. In both cases, φ is arbitrarily set to 0.

        Returns: Tuple containing azimuth (ψ), inclination (θ) and bank (φ).

        """

        r = tuple(self._r)
        r_sq = tuple(x**2 for x in self._r)
        d_r12, d_r13, d_r23 = 2*r[1]*r[2], 2*r[1]*r[3], 2*r[2]*r[3]
        d_r01, d_r02, d_r03 = 2*r[0]*r[1], 2*r[0]*r[2], 2*r[0]*r[3]

        R_20 = d_r13 - d_r02
        R_21 = d_r23 + d_r01
        R_22 = 1 - 2*(r_sq[1] + r_sq[2])

        θ = np.arctan2( -R_20, np.sqrt( R_21**2 + R_22**2 ) )

        if abs(abs(θ) - π/2) > self._eps_tiny:

            R_00 = 1 - 2*(r_sq[2] + r_sq[3])
            R_10 = d_r12 + d_r03

            ψ = np.arctan2( R_10, R_00)
            φ = np.arctan2( R_21, R_22)

        else:

            R_11 = 1 - 2*(r_sq[1] + r_sq[3])
            R_12 = d_r23 - d_r01

            φ = 0 #arbitrary

            if abs(θ - π/2) < self._eps_tiny:
                ψ = np.arctan2( R_12, R_11)

            else: #θ - (-π/2) < self._eps_tiny:
                ψ = np.arctan2( -R_12, R_11)

        return ψ, θ, φ


    def quat_dot(self, ω_ab_b):
        """
        Computes the time derivative of the attitude quaternion.

        If self defines the rotation from frame a to frame b, this method
        returns the time derivative of its quaternion representation, given the
        b-frame components of the angular velocity of b with respect to a,
        ω_ab_b.

        Returns: Quaternion time derivative as a Quaternion instance.
        """
        return 0.5 * self.quat * Quat(real = 0, imag = ω_ab_b)


    def rmat_dot(self,  ω_ab_b):
        """
        Computes the time derivative of the rotation matrix.

        If self defines the rotation from frame a to frame b, this method
        returns the time derivative of its rotation matrix representation, given
        the b-frame components of the angular velocity of b with respect to a,
        ω_ab_b.

        Returns: Rotation matrix time derivative as a numpy array of shape
        (3,3).
        """
        return self.rmat @ v2skew(ω_ab_b)


    def euler_dot(self, ω_ab_b):
        """
        Computes the time derivative of the Euler angles.

        If self defines the rotation from frame a to frame b, this method
        returns the time derivative of its Euler angle representation, given the
        b-frame components of the angular velocity of b with respect to a,
        ω_ab_b.

        Returns: Array containing the time derivatives of the Euler angle
        representation (ψ, θ, φ) as a numpy array of shape (3,). Note that this
        will become singular for θ = π/2.
        """
        _, θ_ab, φ_ab = self.euler

        sinφ = np.sin(φ_ab)
        cosφ = np.cos(φ_ab)
        secθ = 1/np.cos(θ_ab)
        tanθ = np.tan(θ_ab)

        A_euler = np.array([
            [0, sinφ * secθ, cosφ * secθ],
            [0, cosφ       , -sinφ      ],
            [1, sinφ * tanθ, cosφ * tanθ]])

        return A_euler @ ω_ab_b