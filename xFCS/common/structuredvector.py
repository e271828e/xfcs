import keyword
import numbers
import numpy as np
from collections import OrderedDict as OD

#define decorator to retrieve class attributes as properties (the property
#decorator only works with instance attributes)
class classproperty:

    def __init__(self, f):
        self.f = f

    def __get__(self, obj, owner):
        return self.f(owner)


class Node:

    _size = 0
    _children = OD()

    def __init__(self, data = None):

        if data is None:
            data = np.zeros(self._size)

        if isinstance(data, numbers.Number):
            super().__setattr__('_data', np.array((data,)))
        else:
            super().__setattr__('_data', data)

        #this does the same, but less efficiently
        # super().__setattr__('_data', np.atleast_1d(data))


    def _get_child(self, key):

        try:
            child_type = self._children[key]['type']
            child_offset = self._children[key]['offset']
        except KeyError as err:
            raise KeyError(f"Node has no child {key}") from err
        child_size = child_type.size
        data = self._data[child_offset: child_offset + child_size]
        return child_type(data)


    def _set_child(self, key, value):

        child = self._get_child(key)
        if isinstance(value, Node):
            child[:] = value[:]
        else: #value must be either a number or an ndarray
            #let numpy handle it
            child[:] = value


    def __getitem__(self, key):
        """indexing notation wrapper for _get_child"""
        if isinstance(key, slice) or isinstance(key, numbers.Integral):
            return self._data[key]
        else:
            return self._get_child(key)


    def __getattr__(self, key):
        """indexing notation wrapper for _get_child"""
        return self._get_child(key)


    def __setitem__(self, key, value):
        """assigns an np array to the slice indicated by key"""
        if isinstance(key, slice) or isinstance(key, numbers.Integral):
            self._data[key] = value
        else: #otherwise it should be a block name
            self._set_child(key, value)


    def __setattr__(self, key, value):
        """assigns an np array to the slice indicated by key"""
        #this gets called even if the attribute already exists
        self._set_child(key, value)


    def __iter__(self):
        """returns an iterator over the node's children (not elements!)"""
        return (self._get_child(key) for key in self._children.keys())


    def __repr__(self):
        return ("<"
            + type(self).__name__
            + f" | size: {self._size}"
            + f" | children: {repr(list(self._children.keys()))}"
            + f" | data: {repr(self._data)}"
            + ">")


    def __copy__(self):
        return type(self)(data = self._data.copy())


    def __iadd__(self, other): #in-place addition (+=)
        self._data[:] += other._data
        return self


    def __add__(self, other): #addition (+)
        new_obj = self.__copy__()
        new_obj += other
        return new_obj


    def __isub__(self, other): #in-place subtraction (-=)
        self._data[:] -= other._data
        return self


    def __sub__(self, other): #subtraction (-)
        new_obj = self.__copy__()
        new_obj -= other
        return new_obj


    def __imul__(self, other): #in-place multiplication (*=)
        #other must be either a scalar or a one dimensional array of equal
        #length
        self._data[:] *= other
        return self


    def __mul__(self, other): #multiplication (*=)
        new_obj = self.__copy__()
        new_obj *= other
        return new_obj


    def __rmul__(self, other): #right multiplication (*=)
        return self * other


    def __eq__(self, other):
        if (self._data == other._data).all():
            return True
        else:
            return False

    def __bool__(self):
        return bool(self._data.any())


    def __len__(self):
        return self._size


    def copy(self):
        return self.__copy__()


    @classproperty
    def size(self):
        return self._size

    @classproperty
    def children(self):
        return self._children


def NodeType(name: str = 'GenericNode', blocks: OD = None, size: int = None):

    children = OD()

    if blocks: #a non-empty OD of blocks has been specified

        if size is not None:
            raise ValueError("Cannot specify both blocks and node size")
        if not isinstance(blocks, OD):
            raise TypeError("Blocks must be specified in a non-empty"
                            "OrderedDict as (identifier, type) items")

        size = 0
        for (ident, blocktype) in blocks.items():
            if ident[0].isnumeric():
                raise ValueError("Block identifier cannot begin with an number")
            if keyword.iskeyword(ident):
                raise ValueError("Block identifier " + ident + " is a keyword")
            if not 'Node' in [c.__name__ for c in blocktype.__mro__]:
                raise TypeError("Node blocks must be subtypes of Node")
            children[ident] = {
                'type': blocktype, 'parent': name, 'offset': size}
            size += blocktype.size

    else: #blocks not specified, non-zero size expected
        if not size:
            raise ValueError(
                "Either blocks or a non-zero node size must be specified")

    attribs = {'_size': size, '_children': children}

    return type(name, (Node,), attribs)



if __name__ == '__main__':

    Node5 = NodeType('Node5', size = 5)
    TwoNode5 = NodeType('TwoNode5', blocks = OD((('a', Node5), ('b', Node5))))

    TwoNode5Direct = NodeType('TwoNode5', blocks = OD((
        ('a', NodeType('Leaf', size = 5)),
        ('b', NodeType('Leaf', size = 5)))))

    #then subclass the simple vector hierarchy to add custom functionality
    class TwoNode5Custom(TwoNode5Direct):

        def y(self, a = 123):
            print(self[:] + a)

    x = TwoNode5Custom(data = np.zeros(10))
    x[:] = np.arange(x.size)
    x_a = x['a']
    x.b[2:] = 1
    x.y(4)
    print(x.children)

    pass