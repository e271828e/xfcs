from abc import ABC, abstractmethod
import numpy as np
from scipy.optimize import root
from numba import jit, njit

def norm(v):
    """
    Simple norm computation for one-dimensional numpy arrays.
    """
    if np.ndim(v) != 1:
        raise ValueError("Input must be a one-dimensional numpy array")

    return np.sqrt((v ** 2).sum())


def v2skew(v):
    """
    Computes the skew-symmetric matrix corresponding to the column matrix given
    by 3-element sequence v.
    """

    if len(v) != 3:
        raise ValueError("Input must be a 3-element sequences")

    return np.array([
        [0, -v[2], v[1]],
        [v[2], 0, -v[0]],
        [-v[1], v[0], 0] ])

@njit
def cross(v1, v2):
    """
    Simple cross product for a single pair of vectors
    """

    if len(v1) != 3 or len(v2) != 3:
        raise ValueError("Inputs must be 3-element sequences")

    return np.array([
        v1[1]*v2[2] - v1[2]*v2[1],
        v1[2]*v2[0] - v1[0]*v2[2],
        v1[0]*v2[1] - v1[1]*v2[0]
    ])



class Integrator(ABC):

    @abstractmethod
    def step(self, f, t0, x0, dt, k1 = None):
        #optional argument k1 allows providing a pre-computed f(t, x) to the
        #integrator, which saves one call to f
        pass


class RK4(Integrator):

    def step(self, f, t0, x0, dt, k1 = None):

        if k1 is None:
            k1 = f(t0           , x0                )
        k2     = f(t0 + 0.5 * dt, x0 + 0.5 * dt * k1)
        k3     = f(t0 + 0.5 * dt, x0 + 0.5 * dt * k2)
        k4     = f(t0 +       dt, x0 +       dt * k3)

        return x0 + dt/6.0 * (k1 + 2 * (k2 + k3) + k4)


class Midpoint(Integrator):

    def step(self, f, t0, x0, dt, k1 = None):

        if k1 is None:
            k1 = f(t0,            x0                )
        k2     = f(t0 + 0.5 * dt, x0 + 0.5 * dt * k1)

        return x0 + dt * k2


class Heun(Integrator):

    def step(self, f, t0, x0, dt, k1 = None):

        if k1 is None:
            k1 = f(t0,      x0          )
        k2     = f(t0 + dt, x0 + dt * k1)

        return x0 + dt/2 * (k1 + k2)


class AdamsMoulton(Integrator):

    def step(self, f, t0, x0, dt, k1 = None):

        if k1 is None:
            k1 = f(t0,      x0          )
        x_init = x0 + dt * k1

        sol = root(lambda x: x - x0 - 0.5*dt * (k1 + f(t0 + dt, x)), x_init,
                  method = 'hybr', tol = 1e-9)

        return sol.x