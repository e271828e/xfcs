import os
from abc import ABC
from collections import OrderedDict as OD
from collections import namedtuple as NT
from xFCS.common.structuredvector import NodeType
from matplotlib import pyplot as plt

#pylint: disable=invalid-name
#pylint: disable=non-ascii-name
#pylint: disable=protected-access

SystemOutput = NT(typename = 'SystemOutput',
                  field_names = ('root', 'ss'),
                  defaults = (OD(), OD()))

class System(ABC):

    def __init__(self, subsystems: OD = None):

        if subsystems is None:
            subsystems = OD()

        self._subsystems = subsystems

        self._XType = self._get_XType()
        if self._XType is None:
            self._x = None
        else:
            self._x = self._XType()
            self.assign_x_nodes(self._x)
        self.initialize_state()

        self._UType = self._get_UType()
        if self._UType is None:
            self._u = None
        else:
            self._u = self._UType()
            self.assign_u_nodes(self._u)
        self.initialize_input()


    def _get_XType_blocks(self):

        ss_blocks = [(ss_id, ss._XType) for ss_id, ss
                     in self._subsystems.items()
                     if ss._XType is not None]

        return ss_blocks


    def _get_UType_blocks(self):

        ss_blocks = [(ss_id, ss._UType) for ss_id, ss
                     in self._subsystems.items()
                     if ss._UType is not None]

        return ss_blocks


    def _get_XType(self) -> NodeType:
        #returns the NodeType describing the system's state. if the system has
        #sub-systems, it should call their own _get_XType methods and assemble
        #all the NodeTypes in a single one. if the system is stateless, it shall
        #return None, and this will have to be handled.
        ss_blocks = self._get_XType_blocks()
        if not ss_blocks:
            return None
        else:
            return NodeType(name = 'X' + type(self).__name__,
                            blocks = OD(ss_blocks))

    def _get_UType(self) -> NodeType:
        #same as _get_XType but for system inputs
        ss_blocks = self._get_UType_blocks()
        if not ss_blocks:
            return None
        else:
            return NodeType(name = 'U' + type(self).__name__,
                            blocks = OD(ss_blocks))

    def assign_x_nodes(self, node):
        x = self._x #save current system state value
        self._x = node #assign state to upstream node
        self._x[:] = x[:] #restore the original value while keeping the reference to upstream node
        for ss_id, ss in self._subsystems.items():#assign subnodes to subsystems
            if ss._XType is not None:
                ss.assign_x_nodes(self._x[ss_id])

    def assign_u_nodes(self, node):
        u = self._u
        self._u = node
        self._u[:] = u[:]
        for ss_id, ss in self._subsystems.items():
            if ss._UType is not None:
                ss.assign_u_nodes(self._u[ss_id])

    def __getattr__(self, ss):
        return self._subsystems[ss]

    @property
    def XType(self) -> NodeType:
        return self._XType

    @property
    def UType(self) -> NodeType:
        return self._UType

    @property
    def x(self):
        return self._x

    @property
    def u(self):
        return self._u

    @x.setter #prevent accidental reassignment of attribute, only allow value changes
    def x(self, value):
        self._x[:] = value

    @u.setter #prevent accidental reassignment of attribute, only allow value changes
    def u(self, value):
        self._u[:] = value

    def initialize_state(self, **kwargs):
        #override to customize initial values
        pass

    def initialize_input(self, **kwargs):
        #override to customize initial values
        pass


    def f_cont(self, *, t, **kwargs): #u is taken from local storage
        #returns a tuple (x_dot, y), where y is a tuple comprising additional
        #outputs objects that can be determined along x_dot and provide
        #necessary or useful information.#

        #the reason for grouping x_dot, wrench and y is that all these will
        #typically require a set of common previous computations, and if they
        #were segregated in different methods, these computations would have to
        #be repeated on each call

        if self._XType is None: #handles null or stateless systems
            x_dot = None
        else:
            x_dot = self._XType()

        y_root = OD()
        y_ss = OD()
        for ss_id, ss in self._subsystems.items():
            x_dot_ss, y_ss[ss_id] = ss.f_cont(t = None, **kwargs)
            if ss._XType is not None: #only if ss has continuous states
                x_dot[ss_id] = x_dot_ss

        y = SystemOutput(root = y_root, ss = y_ss)
        return x_dot, y


    def f_disc(self, *, t, **kwargs):

        for ss in self._subsystems.values():
            ss.f_disc(t = t, **kwargs)

    def get_root_figures(self, log):

        return OD()


    def plot(self, log, save_path = None, keep_open = True):

        """Any System subclass that needs to plot root outputs can override this
to define, call it first to get subsystem plots filled up, and then insert its
root figures in the root field"""

        figs = SystemOutput(root = OD(), ss = OD())

        #check first entry to see if we actually have data in the root fields
        if log['y'][0].root:
            root_log = {'t': log['t'], 'y': [sample.root for sample in log['y']]}
            root_figs = self.get_root_figures(root_log)
        else:
            root_figs = OD()

        #if there are root figures, add them to figs.root
        for f_id, f in root_figs.items():
            figs.root[f_id] = f
            if save_path:
                os.makedirs(os.path.join(save_path), exist_ok=True)
                print(f"Writing figure {f_id} to {save_path}")
                f.savefig(os.path.join(save_path, f_id), dpi = 100)
            if not keep_open:
                plt.close(f)


        for ss_id, ss_obj in self._subsystems.items():
            ss_log = {'t': log['t'],
                      'y': [sample.ss[ss_id] for sample in log['y']]}
            if save_path:
                ss_path = os.path.join(save_path, ss_id)
            else:
                ss_path = None
            figs.ss[ss_id] = ss_obj.plot(ss_log, ss_path, keep_open)

        return figs