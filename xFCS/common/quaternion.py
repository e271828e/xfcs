import numpy as np
import numbers
from xFCS.common.math import cross

class Quaternion():
    "Implements a general quaternion class. See test/common/quaternion for usage examples"

    def __init__(self, *args, **kwargs):

        n_args = len(args)

        if n_args == 0:
           self._data = np.zeros(4, dtype='float')

        elif n_args == 1:
            if isinstance(args[0], Quaternion):
                self._data = args[0]._data
            elif len(args[0]) == 4:
                self._data = np.array(args[0])
            else:
                raise ValueError("Input to constructor must be either another Quaternion instance or a 4-element sequence")

        elif n_args == 4:
            self._data = np.array(args, dtype='float')

        else:
            raise ValueError("Only 0, 1 or 4 positional arguments accepted")

        if kwargs:

            try:
                if "real" in kwargs:
                    self._data[0] = kwargs["real"]
                if "imag" in kwargs:
                    self._data[1:4] = kwargs["imag"]

            except ValueError:
                raise ValueError("Real part must be specified as a scalar and imaginary part as a 3-element sequence")

    def copy(self):
        return Quaternion(self._data)

    def __repr__(self):
        return ("<" + type(self).__name__ + "(" + np.array2string(self._data) + ")>")

    def __getitem__(self, index):
        return self._data[index]

    def __setitem__(self, index, value):
        self._data[index] = value

    def __bool__(self):
        return bool(self._data.any())

    def __neg__(self):
        return Quaternion(-self._data)

    def conj(self):
        return Quaternion(real = self.real, imag = -self.imag)

    def inv(self, eps = 1e-20):

        norm_sq = self.norm_sq
        if norm_sq < eps:
            raise ValueError("Inversion failed: norm below threshold")
        return Quaternion(self.conj()._data / norm_sq)

    def normalize(self, eps = 1e-14):

        norm = self.norm
        if norm < eps:
            raise ValueError("Normalization failed: norm below threshold")
        self._data /= norm
        return self

    def __eq__(self, other, tol = 1e-14):
        return (True if np.allclose(self._data, other._data, tol, tol) else False)

    def __iadd__(self, other): #in-place addition (+=)
        self._data += other._data
        return self

    def __isub__(self, other): #in-place subtraction (-=)
        self._data -= other._data
        return self

    def __add__(self, other): #addition (+)

        new_obj = self.copy()
        new_obj += other
        return new_obj

    def __sub__(self, other): #subtraction (-)

        new_obj = self.copy()
        new_obj -= other
        return new_obj

    def __imul__(self, other):

        if isinstance(other, Quaternion):
            q1_0, q1_v = self.real, self.imag
            q2_0, q2_v = other.real, other.imag
            self.real = q1_0 * q2_0 - np.dot(q1_v, q2_v)
            self.imag = q1_0 * q2_v + q2_0 * q1_v + cross(q1_v, q2_v)

        elif isinstance(other, numbers.Real):
            self._data *= other

        else:
            raise ValueError("Right operand must be either a real scalar or a Quaternion")

        return self

    def __mul__(self, other):

        new_obj = self.copy()
        new_obj *= other
        return new_obj

    def __rmul__(self, other): #enables expresions of the form 2*quat()

        if isinstance(other, numbers.Real):
            return self * other
        else:
            raise ValueError("Left operand must be a real scalar")

    def __itruediv__(self, other):

        if isinstance(other, Quaternion):
            self = self * other.inv()

        elif isinstance(other, numbers.Real):
            self._data /= other

        else:
            raise ValueError("Right operand must be either a real scalar or a Quaternion")

        return self

    def __truediv__(self, other):

        new_obj = self.copy()
        new_obj /= other
        return new_obj

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value):
        self._data[0:4] = value

    @property
    def real(self):
        return self._data[0]

    @real.setter
    def real(self, value):
        self._data[0] = value

    @property
    def imag(self):
        return self._data[1:4]

    @imag.setter
    def imag(self, value):
        self._data[1:4] = value

    @property
    def norm_sq(self):
        return (self._data**2).sum()

    @property
    def norm(self):
        return np.sqrt(self.norm_sq)