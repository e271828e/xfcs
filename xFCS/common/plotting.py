import os
from collections import namedtuple as NT
from matplotlib import pyplot as plt
# from matplotlib.figure import Figure
# from matplotlib import rcParams

PlotVar = NT(typename = 'PlotVar',
             field_names = ('title', 'label', 'y_data'))


def make_figure(title, t, plot_vars):

    fig = plt.figure(figsize=(24, 12), dpi=100, tight_layout = False)
    if title:
        fig.suptitle(title, fontsize = 16)

    n_cols = len(plot_vars)

    for i, v in enumerate(plot_vars):
        ax = fig.add_subplot(1, n_cols, i+1)
        ax.plot(t, v.y_data)
        ax.set_xlabel(r"$t \/ (s)$")
        ax.set_ylabel(v.label)
        ax.set_title(v.title)
        ax.grid()

    return fig

def export_figures(figs: dict, path: str):

    for i, (f_id, f) in enumerate(figs.items()):
        print(f"Exporting figure {i} of {len(figs)}...")
        f.savefig(os.path.join(path, f_id), dpi = 100)