import time
from abc import ABC
from collections import OrderedDict as OD

from xFCS.simulation.profiling import profile_me

#initialize() can be overriden. for ex, i can define a source block whose purpose
#is communicating with another process via a queue and receive new messages,
#which are then output in a block.
#this one will have no input pots, but may have to synchronize somehow with the
#process.
#similarly, can define a sink block sending messages via queue
#to aoid bottlenecks, the data consumed by the other processes must be sent
#before the heavy computations in this one start, so that they can run in
#parallel

#so, we should:
#1) execute blocks that retrieve new data from other processes
#2) execute blocks that send new data to other processes

#furthermore, it could be the same block!!! when update() is called, it
#retrieves new values y from the other process, then sends inputs u to it.
#u can be any data type!!!

class Signal:

    def __init__(self, name = 'signal', logging = False):

        self._name = name #should be set by the model
        self._initialized = False
        self._data = None
        self._source_port = None
        self._logging = logging
        self._log = {'t': [], 'y': []}

    def _set_source_port(self, port):
        if self._source_port:
            raise SignalAlreadyDriven(self)
        if not isinstance(port, OutputPort):
            raise TypeError("Signal source port must be an OutputPort")
        self._source_port = port

    def _reset_source_port(self):
        self._source_port = None

    def _write_data(self, t, data):

        self._data = data

        if not self._initialized:
            self._initialized = True

        if self._logging:
            self._log['t'].append(t)
            self._log['y'].append(self._data)

class Port:

    def __init__(self, name, parent):
        self._name = name #this should be set by the parent block
        self._parent = parent
        self._signal = None

    @property
    def is_connected(self):
        return self._signal is not None

    @property
    def path(self):
        return (f"{self._parent._name}/{self._name}")

class OutputPort(Port):

    def _connect_signal(self, signal):
        signal._set_source_port(self)
        self._signal = signal

    def _disconnect(self):
        self._signal._reset_source_port()
        self._signal = None

class InputPort(Port):

    def _connect_signal(self, signal):
        if self.is_connected:
            raise PortAlreadyConnected(self)
        self._signal = signal

    def _disconnect(self):
        self._signal = None


class SignalAlreadyDriven(Exception):

    def __init__(self, signal):
        port = signal._source_port
        message = (f"Signal {signal._name} already driven by output " +
                   f"port {port.path}. Please disconnect port and try again.")
        super().__init__(message)

class PortAlreadyConnected(Exception):

    def __init__(self, port):
        signal = port._signal
        message = (f"Port {port.path} already connected to signal " +
                   f"{signal._name}. Please disconnect port and try again.")
        super().__init__(message)

class InputsNotReady(Exception):
    pass

class Block(ABC):

    def __init__(self, name):

        self._name = name
        self._input_ports = OD()
        self._output_ports = OD()
        self._x = None

        self.define_input_ports()
        self.define_output_ports()
        self.define_x() #cannot depend on u, for a DiscretizedSystem this simply assigns self.system.x

        self._initialized = False

    def _add_input_port(self, name):
        port = InputPort(name, parent = self)
        if name in self._input_ports:
            raise ValueError(f"Input port {name} already exists \
                in block {self._name}")
        self._input_ports[name] = port

    def _add_output_port(self, name):
        port = OutputPort(name, parent = self)
        if name in self._output_ports:
            raise ValueError(f"Output port {name} already exists \
                in block {self._name}")
        self._output_ports[name] = port

    def get_input(self, name = None):
        input_ports = self._input_ports
        if name is None:
            if len(input_ports) <= 1:
                name = list(input_ports.keys())[0]
            else:
                raise ValueError("More than one input port, must specify name")
        return input_ports[name]

    def get_output(self, name = None):
        output_ports = self._output_ports
        if name is None:
            if len(output_ports) <= 1:
                name = list(output_ports.keys())[0]
            else:
                raise ValueError("More than one output port, must specify name")
        return output_ports[name]

    #overload as required
    def define_input_ports(self):
        pass

    #overload as required
    def define_output_ports(self):
        pass

    #overload as required
    def define_x(self):
        pass

    def initialize(self, t):

        #first make sure all input ports are connected
        for port in self._input_ports.values():
            if not port.is_connected:
                raise ValueError(f"Unconnected port {port._name} in block {self._name}")

        for port in self._output_ports.values():
            if not port.is_connected:
                raise ValueError(f"Unconnected port {port._name} in block {self._name}")

        #if any input signal is not ready, it will hold None as a value.
        #whenever block initialization is prevented by this, the block must
        #signal it so model initialization can skip it and return to it later
        try:
            self.write_output_ports(t)
        except InputsNotReady:
            pass
        else:
            self._initialized = True

    #overload as required
    def update(self, t, dt): #update internal state, and possibly also
        """This method must update the block's internal state, and also possibly
store the outputs if they are computed simultaneously, so they can later be
written by write_output_ports"""
        pass

    #overload as required
    def write_output_ports(self, t):
        """This method must write to the block's output ports. The values used
for this can be computed in this very method call, or may have been precomputed
and stored by the update method"""
        pass

    #overload as required
    def shutdown(self):
        pass


class Model:

    #logging should be done signal wise! everytime Signal.write_data() is called,
    #it writes to its own log. taking account decimation!
    def __init__(self, dt = 1e-2, t_start = 0, t_end = 10):

        self.dt = dt
        self.t_start = t_start
        self.t_end = t_end
        self._blocks = OD()
        self._signals = OD()

    @property
    def blocks(self):
        return self._blocks

    @property
    def signals(self):
        return self._signals

    def add_block(self, name, block):
        if name in self._blocks:
            raise ValueError(f"Block {name} already exists")
        block._name = name
        self._blocks[name] = block

    def add_signal(self, name, signal):
        if name in self._signals:
            raise ValueError(f"Signal {name} already exists")
        signal._name = name
        self._signals[name] = signal

    def connect(self, source, target):

        if isinstance(source, OutputPort) and isinstance(target, InputPort):
            if not source.is_connected: #has no output signal, need to allocate a new one
                s = Signal()
                self.add_signal(source.path, s)
                source._connect_signal(s)
            target._connect_signal(source._signal)

        elif isinstance(source, OutputPort) and isinstance(target, Signal):
            source._connect_signal(target)

        elif isinstance(source, Signal) and isinstance(target, InputPort):
            target._connect_signal(source)

        else:
            raise TypeError("Wrong source-target combination")

    def initialize(self, t):

        #initialize blocks iteratively until all signals are propagated
        initialized = {block_id: False for block_id in self._blocks.keys()}
        init_count = 0
        ordered_blocks = OD()
        while True:
            if init_count == len(self._blocks):
                print("All blocks initialized")
                break
            init_count_last = init_count
            for block_id, block in self._blocks.items():
                if not initialized[block_id]:
                    block.initialize(t) #try
                    if block._initialized: #success?
                        ordered_blocks[block_id] = block #fill an OD with the init order
                        initialized[block_id] = True
                        init_count +=1

            #if no blocks have been initialized in this iteration, they won't be
            #either in the next one. initialization will never finish.
            print(initialized)
            print(init_count)
            if init_count == init_count_last:
                raise RuntimeError("Model initialization failed, check for algebraic loops")

        print(f"Blocks reordered as: {ordered_blocks.keys()}")
        self._blocks = ordered_blocks

    def shutdown(self):
        for block in self._blocks.values():
            block.shutdown()

    def run(self, profiled = False, real_time = False):

        if profiled:
            scheduler = profile_me(self._scheduler)
        else:
            scheduler = self._scheduler

        #the call to initialize already writes output signals for the first time
        self.initialize(self.t_start)
        scheduler(real_time)
        self.shutdown()

    def _scheduler(self, real_time):

        # i = 0 #might be useful again when implementing multi-rate
        t = self.t_start

        treal_last = time.perf_counter()

        while t < self.t_end:

            # print(f"t = {t}: {[(s_id, s._data) for s_id, s in self._signals.items()]}")

            for block in self._blocks.values():
                block.update(t, self.dt)

            for block in self._blocks.values():
                block.write_output_ports(t)

            t += self.dt

            if real_time:
                treal_next = treal_last + self.dt
                treal_now = time.perf_counter()
                if treal_next > treal_now:
                    # print(f"Sleeping for {treal_next - treal_now}")
                    time.sleep(treal_next - treal_now)
                    # treal_wakeup = time.perf_counter()
                    # if treal_wakeup > treal_next:
                    #     # print(f"Overslept by {treal_wakeup - treal_next}\n")
                    #     pass
                treal_last = treal_next

            # in theory, we should now be exactly at treal_next, but this won't be
            # the case due to the inaccuracy of time.sleep()
            # i += 1
        return