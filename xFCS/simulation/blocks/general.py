from abc import abstractmethod
import numbers
import copy

import numpy as np
from xFCS.common.math import Heun, Integrator
from xFCS.common.structuredvector import NodeType
from xFCS.common.system import System
from xFCS.simulation.engine import Block, InputsNotReady


class Accumulator(Block):

    def __init__(self, name = 'accumulator', length = 1, initial_value = 0):
        self._initial_value = initial_value
        self._length = length
        self._y = None
        super().__init__(name)

    def define_input_ports(self):
        self._add_input_port('input')

    def define_output_ports(self):
        self._add_output_port('output')

    def define_x(self):
        self._x = self._initial_value

    def initialize(self, t):
        super().initialize(t)

    #we don't really gain anything from writing self._y in the call to update,
    #this just a reference for DiscretizedSystem (which does)
    def update(self, t, dt):
        u = self._input_ports['input']._signal._data
        self._x += u

    def write_output_ports(self, t):
        self._output_ports['output']._signal._write_data(t, copy.copy(self._x))


class Adder(Block):

    def __init__(self, name = 'Adder'):
        super().__init__(name)

    def define_input_ports(self):
        self._add_input_port('input1')
        self._add_input_port('input2')

    def define_output_ports(self):
        self._add_output_port('output')

    def write_output_ports(self, t):
        u1 = self._input_ports['input1']._signal._data
        u2 = self._input_ports['input2']._signal._data
        if (u1 is None) or (u2 is None):
            raise InputsNotReady
        y = u1 + u2
        self._output_ports['output']._signal._write_data(t, y)


class Gain(Block):

    def __init__(self, name = 'Gain', value = 1):
        self._value = value
        super().__init__(name)

    def define_input_ports(self):
        self._add_input_port('input')

    def define_output_ports(self):
        self._add_output_port('output')

    def write_output_ports(self, t):
        u = self._input_ports['input']._signal._data
        if u is None:
            raise InputsNotReady
        y = self._value * u
        self._output_ports['output']._signal._write_data(t, y)


class Constant(Block):

    def __init__(self, name = 'Constant', value = 1):
        self._value = value
        super().__init__(name)

    def define_output_ports(self):
        self._add_output_port('output')

    def write_output_ports(self, t):
        self._output_ports['output']._signal._write_data(t, self._value)


class DiscretizedSystem(Block):

    """Wraps a hybrid (continuous/discrete) system, providing methods to update
    it over an arbitrary time increment and to retrieve the resulting outputs"""

    def __init__(self, *, name = 'DiscretizedSystem',
                 system: System,
                 integrator: Integrator = Heun()):

        self._system = system #we need this first to initialize the state
        self._integrator = integrator
        self._y = None
        self._k1 = None #temporary storage for the next x_dot

        super().__init__(name)

    def define_x(self):
        self._x = self._system.x

    #override as required
    def define_input_ports(self):
        pass

    def define_output_ports(self):
        pass

    @property
    def x(self):
        return self._x

    @property
    def u(self):
        #return a reference to this object's input vector, so it can be assigned
        #externally
        return self._system.u

    @property
    def y(self):
        return self._y

    def f_cont_x(self, t, x: np.ndarray) -> np.ndarray:
        #computes x_dot and y for a given system state x, preserving the current
        #internal state of the system
        x_backup = self._x[:].copy() #save the current state of the system
        self._x[:] = x #assign the requested state to the system
        out = self._system.f_cont(t = t) #get x_dot and y
        self._x[:] = x_backup #restore internal system state
        return out

    def f_int(self, t, x: np.ndarray) -> np.ndarray: #helper function for calling the integrator
        return self.f_cont_x(t, x)[0][:] #simply discards y and extracts np array from xdot

    @abstractmethod
    def assign_system_inputs(self):
        pass

    #override this to read from inputs and assign them to underlying System's u
    def update(self, t, dt):
        #WARNING: assign slices, not the variable itself. otherwise, the
        #reference is broken
        self.assign_system_inputs()

        x = self._x[:]
        if self._k1 is not None:
            k1 = self._k1[:]
        else:
            k1 = None

        x = self._integrator.step(self.f_int, t, x, dt, k1)
        self._x[:] = x

        #perform discrete update (this might also change continuous states!)
        self._system.f_disc(t = t) #update the internal discrete states of each subsystem

        #get outputs from f_cont and save x_dot = k1 to use it as the initial
        #derivative at the next continuous update
        self._k1, self._y = self.f_cont_x(t, x)

    #overload this as required to write to output ports, then call it as super()
    def write_output_ports(self, t):
        if self._y is None: #if we've been called before the first update...
            #we need to do this to ensure the initial output is correct in case
            #there is feedthrough
            self.assign_system_inputs()
            _, self._y = self.f_cont_x(t, self._x[:])
