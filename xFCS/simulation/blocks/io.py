import numpy as np
from xFCS.io.controller import ControllerInterface, XBoxOneInterface
from xFCS.io.xpinterfaces import DispInterface
from xFCS.simulation.blocks.aircraft import GndCtl, HoGCtl
from xFCS.simulation.engine import Block


class XPlaneDisplay(Block):

    def __init__(self, *, name = 'XPlaneDisplay'):

        self._interface = None
        super().__init__(name)

    def initialize(self, t):
        #wait until initialization is called, because the DispInterface
        #constructor already excepts xplane running
        self._interface = DispInterface()
        self._interface.disable_physics()
        super().initialize(t)

    def define_input_ports(self):
        self._add_input_port('input')

    def update(self, t, dt):
        y_aircraft = self._input_ports['input']._signal._data
        Ob = y_aircraft.ss['airframe'].root.pos.Ob
        n_b = y_aircraft.ss['airframe'].root.att.n_b
        self._interface.set_pose(Ob, n_b)


class Controller(Block):

    def __init__(self, name, interface: ControllerInterface):
        self._interface = interface
        super().__init__(name)

    def define_output_ports(self):
        self._add_output_port('output')

    def initialize(self, t):

        self._interface.start()
        super().initialize(t)

    def write_output_ports(self, t):
        #can't use nowait here, because we cannot have it returning None
        #we could fix it by storing the last controller state as self._x, and
        #when get_state_nowait returns None, output that instead. however, this
        #is not possible during initialization, so we would need to check
        #self._initialized for that
        c_state = self._interface.get_state()
        self._output_ports['output']._signal._write_data(t, c_state)

    def shutdown(self):
        self._interface.stop()


class XBoxOne(Controller):

    def __init__(self, name = 'XBoxOne'):
        super().__init__(name, interface = XBoxOneInterface())


class ControllerAdapter(Block):

    def __init__(self, source: type, target: type, name = 'ControllerAdapter'):
        self._map = self.get_map(source, target)
        super().__init__(name)

    def define_input_ports(self):
        self._add_input_port('input')

    def define_output_ports(self):
        self._add_output_port('output')

    def write_output_ports(self, t):
        controller_state = self._input_ports['input']._signal._data
        output = self._map(controller_state)
        self._output_ports['output']._signal._write_data(t, output)

    @classmethod
    def get_map(cls, source, target):
        if source == XBoxOneInterface.State and target == GndCtl:
            return cls.map_XBoxOne2GndCtl
        if source == XBoxOneInterface.State and target == HoGCtl:
            return cls.map_XBoxOne2HoGCtl
        else:
            raise TypeError( f"No map from {source} to {target} available.")

    @classmethod
    def map_XBoxOne2GndCtl(cls, state: XBoxOneInterface.State):

        return GndCtl(throttle = max(0, -state.rstick_y),
                      lbrake = float(state.lbumper),
                      rbrake = float(state.rbumper),
                      nws = cls.exp_curve(-state.trigger, strength=0.5))

    @classmethod
    def map_XBoxOne2HoGCtl(cls, state: XBoxOneInterface.State):

        return HoGCtl(
            lbrake = float(state.lbumper),
            rbrake = float(state.rbumper),
            f_Oc_xc = cls.exp_curve(-state.lstick_y, strength=1, deadzone=0.3),
            f_Oc_yc = cls.exp_curve(state.lstick_x, strength=1, deadzone=0.3),
            t_Oc_xc = cls.exp_curve(state.rstick_x, strength=1, deadzone=0.3),
            t_Oc_yc = cls.exp_curve(state.rstick_y, strength=1, deadzone=0.3),
            t_Oc_zc = cls.exp_curve(-state.trigger, strength=1, deadzone=0.1))

    @staticmethod
    def exp_curve(x, *, strength = 0, deadzone = 0):

        if abs(x) > 1:
            raise ValueError("Input to exponential curve must be within [-1, 1]")

        a = strength
        x0 = deadzone
        if x > 0:
            y = max(0, (x - x0)/(1 - x0)) * np.exp( a * (abs(x) -1) )
        else:
            y = min(0, (x + x0)/(1 - x0)) * np.exp( a * (abs(x) -1) )
        return y


class XBoxOne2GndCtl(ControllerAdapter):

    def __init__(self):
        super().__init__(XBoxOneInterface.State, GndCtl)

class XBoxOne2HoGCtl(ControllerAdapter):

    def __init__(self):
        super().__init__(XBoxOneInterface.State, HoGCtl)
