from collections import namedtuple as NT

from xFCS.aircraft.aircraft import AircraftBase
from xFCS.common.math import Heun, Integrator
from xFCS.simulation.blocks.general import DiscretizedSystem

GndCtl = NT(typename = 'GndCtl',
            field_names = ('throttle', 'lbrake', 'rbrake', 'nws'),
            defaults = (0, 0, 0, 0 ))

HoGCtl = NT(typename = 'HoGCtl',
            field_names = ('f_Oc_xc', 'f_Oc_yc', 'f_Oc_zc',
                           't_Oc_xc', 't_Oc_yc', 't_Oc_zc',
                           'lbrake', 'rbrake'),
            defaults = (0, 0, 0, 0, 0, 0, 0, 0 ))

class AircraftBlock(DiscretizedSystem):
    """Base class for Aircraft system blocks. Defines an output but no inputs;
these may be added by subclasses as required"""

    def __init__(self, *, name = 'Aircraft',
                 aircraft: AircraftBase,
                 integrator: Integrator = Heun()):
        super().__init__(name = name, system = aircraft, integrator = integrator)

    def define_input_ports(self):
        self._add_input_port('input')

    def define_output_ports(self):
        self._add_output_port('output')

    #override abstractmethod in assign_system_inputs
    def assign_system_inputs(self):
        pass

    def write_output_ports(self, t):
        super().write_output_ports(t)
        self._output_ports['output']._signal._write_data(t, self._y)


class AircraftGndCtlBlock(AircraftBlock):

    """Wraps all Aircraft classes designed for ground model testing, which share
a common configuration: tricycle gear, steered nose wheel and brakes in the main
landing gear"""

    ControlInputs = GndCtl

    def assign_system_inputs(self):
        control_inputs = self._input_ports['input']._signal._data
        if not isinstance(control_inputs, self.ControlInputs):
            raise TypeError(
                f"Expected {self.ControlInputs}, "+
                f"got {type(control_inputs)}")

        self.u.pwp.front.throttle[:] = control_inputs.throttle
        self.u.ldg.lmain.brk[:] = control_inputs.lbrake
        self.u.ldg.rmain.brk[:] = control_inputs.rbrake
        self.u.ldg.nose.psi[:] = control_inputs.nws
        # print(control_inputs)


class AircraftHoGCtlBlock(AircraftBlock):

    ControlInputs = HoGCtl

    def assign_system_inputs(self):
        control_inputs = self._input_ports['input']._signal._data
        if not isinstance(control_inputs, self.ControlInputs):
            raise TypeError(
                f"Expected {self.ControlInputs}, "+
                f"got {type(control_inputs)}")

        self.u.ldg.lmain.brk[:] = control_inputs.lbrake
        self.u.ldg.rmain.brk[:] = control_inputs.rbrake
        self.u.pwp.handofgod.f_Oc_c[0] = control_inputs.f_Oc_xc
        self.u.pwp.handofgod.f_Oc_c[1] = control_inputs.f_Oc_yc
        self.u.pwp.handofgod.f_Oc_c[2] = control_inputs.f_Oc_zc
        self.u.pwp.handofgod.t_Oc_c[0] = control_inputs.t_Oc_xc
        self.u.pwp.handofgod.t_Oc_c[1] = control_inputs.t_Oc_yc
        self.u.pwp.handofgod.t_Oc_c[2] = control_inputs.t_Oc_zc
        # print(control_inputs)