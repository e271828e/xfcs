import threading
import time
import pyglet.input
from collections import namedtuple as NT
from abc import ABC, abstractmethod, abstractproperty


class ControllerInterface(ABC):

    def __init__(self, axis_sampling_period = 0.02):

        self._axis_sampling_period = axis_sampling_period
        self._initialized = threading.Event()
        self._shutdown = threading.Event()
        self._state_lock = threading.Lock()
        self._state = None

        try:
            connected_controllers = {c.device.name: c for c in
                                     pyglet.input.get_joysticks()}
            self._controller = connected_controllers[self.name]
        except KeyError as e:
            raise KeyError(f"{self.name} not found") from e

    @abstractproperty
    def name(self):
        pass

    def get_state(self):
        with self._state_lock:
            output = self._state
        return output

    def get_state_nowait(self):
        success = self._state_lock.acquire(blocking = False)
        if success: #managed to acquire the lock
            output = self._state
            self._state_lock.release()
        else: #state is being updated by the pyglet thread
            output = None
        return output

    def start(self):
        pyglet_thread = threading.Thread(target = self.pyglet_target)
        pyglet_thread.start()
        #give pyglet some time to initialize and retrieve the first state
        print("Initializing pyglet input thread...")
        self._initialized.wait()

    def stop(self):
        #Event.set() is atomic, so we can safely do this while calling is_set()
        #in the pyglet event handler
        self._shutdown.set()

    @abstractmethod
    def _read_state(self):
        """Reads device and returns a named tuple holding the values of its axes
        and buttons. This is always called from the pyglet thread"""
        pass

    def _update_state(self):
        """Retrieves controller state and safely updates the copy state stored
        in the main thread"""

        state = self._read_state()

        with self._state_lock: #safely update state
            self._state = state

    def _scheduled_update(self, _): #receives dt as first arg
        self._update_state()

    def _button_handler(self, joystick, _): #receives button as second arg
        if joystick is self._controller:
            self._update_state()

    def pyglet_target(self):

        #ugly, but pyglet requires that pyglet.app be imported in the same
        #thread it is run from
        import pyglet.app as app
        import pyglet.clock as clock

        def _shutdown_check(_): #receives dt as first arg
            if self._shutdown.is_set():
                print("Shutdown event was set, exiting...")
                app.exit()

        #map the joystick event handler to button press and release events,
        #and also schedule its execution to read axes periodically
        self._controller.on_joybutton_press = self._button_handler
        self._controller.on_joybutton_release = self._button_handler
        clock.schedule_interval(func = self._scheduled_update,
                                interval = self._axis_sampling_period)
        clock.schedule_interval(func = _shutdown_check,
                                interval = 0.1)
        try:
            self._controller.open(exclusive = True)
            self._update_state() #initializes self._state with default values
            self._initialized.set() #self._state is no longer None
            print("... success")
            app.run()
        except OSError:
            print("... error: could not open the device")
        except pyglet.input.DeviceExclusiveException:
            print("... error: could not open the device in exclusive mode")
        finally:
            self._initialized.set() #otherwise start() will remain stuck


class XBoxOneInterface(ControllerInterface):

    State = NT(
        typename = 'XBoxOneState',
        field_names = (
            'lstick_x', 'lstick_y', 'rstick_x', 'rstick_y', 'trigger',
            'dpad_up', 'dpad_down', 'dpad_left', 'dpad_right',
            'A', 'B', 'X', 'Y', 'View', 'Menu',
            'lstick_press', 'rstick_press', 'lbumper', 'rbumper'))

    @property
    def name(self):
        return 'Controller (Xbox One For Windows)'

    def _read_state(self):

        c = self._controller
        buttons = c.buttons
        return self.State(
            lstick_x = c.x,
            lstick_y = c.y,
            rstick_x = c.rx,
            rstick_y = c.ry,
            trigger = c.z,
            dpad_up = c.hat_y == 1,
            dpad_down = c.hat_y == -1,
            dpad_left = c.hat_x == -1,
            dpad_right = c.hat_x == 1,
            A = buttons[0],
            B = buttons[1],
            X = buttons[2],
            Y = buttons[3],
            View = buttons[6],
            Menu = buttons[7],
            lstick_press = buttons[8],
            rstick_press = buttons[9],
            lbumper = buttons[4],
            rbumper = buttons[5]
        )


if __name__ == "__main__":
    i = XBoxOneInterface(axis_sampling_period=0.1)
    i.start()
    for _ in range(100):
        time.sleep(0.1)
        print(i.get_state_nowait())
    i.stop()