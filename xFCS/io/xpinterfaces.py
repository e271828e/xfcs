from typing import Union, Iterable
from collections import namedtuple
import numpy as np
from enum import IntEnum
from xFCS.io.xpconnect import XPlaneConnect
from xFCS.environment.wgs84 import Point
from xFCS.common.attitude import Attitude3D as Att

M_PER_FOOT = 0.3048
RAD_PER_DEG = np.pi / 180
MS_PER_KT = 0.514444
PA_PER_INHG = 3386.389
TWO_π = 2 * np.pi

class Interface:

    #provides the FMS with an interface to either control the simulation,
    #command the autopilot or obtain the state vector

    def __init__(self, client: XPlaneConnect=None):

        if client is None:
            client = XPlaneConnect()
        self._client = client


class DispInterface(Interface):

    rad2deg = lambda x: x / RAD_PER_DEG

    def disable_physics(self, value=True):
        self._client.sendDREF('sim/operation/override/override_planepath', int(value))

    def set_pose(self, Ob: Point, n_b: Att):

        lat, lon, h = Ob.geo
        ψ_nb, θ_nb, φ_nb = n_b.euler
        pose = [lat/RAD_PER_DEG, lon/RAD_PER_DEG, h, θ_nb/RAD_PER_DEG,
                   φ_nb/RAD_PER_DEG, ψ_nb/RAD_PER_DEG]

        self._client.sendPOSI(pose)


class SimInterface(Interface):

    #frame: Conversion factor for NED frame components
    #unit: Conversion factor for SI units
    State = namedtuple('State', ['dref', 'frame', 'units'])

    _states = {
        'psi': State('sim/flightmodel/position/true_psi', 1, RAD_PER_DEG),
        'theta': State('sim/flightmodel/position/true_theta', 1, RAD_PER_DEG),
        'phi': State('sim/flightmodel/position/true_phi', 1, RAD_PER_DEG),
        'v_N': State('sim/flightmodel/position/local_vz', -1, 1),
        'v_E': State('sim/flightmodel/position/local_vx', 1, 1),
        'v_D': State('sim/flightmodel/position/local_vy', -1, 1),
        'lat': State('sim/flightmodel/position/latitude', 1, RAD_PER_DEG),
        'lon': State('sim/flightmodel/position/longitude', 1, RAD_PER_DEG),
        'alt': State('sim/flightmodel/position/elevation', 1, 1),
        'omega_p': State('sim/flightmodel/position/Prad', 1, 1),
        'omega_q': State('sim/flightmodel/position/Qrad', 1, 1),
        'omega_r': State('sim/flightmodel/position/Rrad', 1, 1),
        'a_N': State('sim/flightmodel/position/local_az', -1, 1),
        'a_E': State('sim/flightmodel/position/local_ax', 1, 1),
        'a_D': State('sim/flightmodel/position/local_ay', -1, 1),
        'g': State('sim/weather/gravity_mss', 1, 1),
        'psi_m': State('sim/flightmodel/position/mag_psi', 1, RAD_PER_DEG),
        'dpsi_m': State('sim/flightmodel/position/magnetic_variation', -1, RAD_PER_DEG),#CCW+!!!
        'p': State('sim/weather/barometer_current_inhg', 1, PA_PER_INHG),
        'rho': State('sim/weather/rho', 1, 1),
        'sigma': State('sim/weather/sigma', 1, 1),
        'a': State('sim/weather/speed_sound_ms', 1, 1),
        'vw_N': State('sim/weather/wind_now_z_msc', -1, 1),
        'vw_E': State('sim/weather/wind_now_x_msc', 1, 1),
        'vw_D': State('sim/weather/wind_now_y_msc', -1, 1),
        'TAS': State('sim/flightmodel/position/true_airspeed', 1, 1),
        'IAS': State('sim/flightmodel/position/indicated_airspeed', 1, MS_PER_KT),
        'alpha': State('sim/flightmodel/position/alpha', 1, RAD_PER_DEG),
        'beta': State('sim/flightmodel/position/beta', 1, RAD_PER_DEG)
    }


    def get_state(self, ids:Iterable=None):

        if ids is None:
            ids = self._states.keys()

        drefs = [self._states[s].dref for s in ids]
        factors = [self._states[s].frame * self._states[s].units for s in ids]
        values = self._client.getDREFs(drefs)
        return {s: k * v[0] for s, k, v in zip(ids, factors, values)}


class APInterface(Interface):

    max_attempts = 2
    AP_STATE_WORD = "sim/cockpit/autopilot/autopilot_state"

    class Master(IntEnum):
        OFF = 0x0
        FLIGHT_DIRECTOR = 0x1
        ON = 0x2

    class VertMode(IntEnum):
        CLIMB_RATE = 0x10 #barometric climb rate
        # FL_CHANGE = 0x40 #does not work well with autothrottle engaged
        PITCH_HOLD = 0x80
        ALT_HOLD = 0x4000 #barometric altitude
        # GAMMA_HOLD = 0x800000 #NOT AVAILABLE IN G1000

    class LatMode(IntEnum):
        HDG_MAG_HOLD = 0x2
        BANK_HOLD = 0x4

    def write_int(self, dref, value):

        for _ in range(self.max_attempts):
            readback = self._client.getDREF(dref)[0]
            if value == int(readback):
                return readback
            self._client.sendDREF(dref, value)

        raise ConnectionError(f"Failed to get a correct readback from X-Plane\
            after {self.max_attempts} attempts")


    def write_float(self, dref, value, readback_tol=1e-3):

        for _ in range(self.max_attempts):
            readback = self._client.getDREF(dref)[0]
            if abs(value - readback) < readback_tol:
                return readback
            self._client.sendDREF(dref, value)

        raise ConnectionError(f"Failed to get a correct readback from X-Plane\
            after {self.max_attempts} attempts")


    def write_bit(self, dref, mask: int, value: bool):

        for _ in range(self.max_attempts):
            confirm = value == bool(mask & int(self._client.getDREF(dref)[0]))
            if confirm:
                return
            self._client.sendDREF(dref, mask)

        raise ConnectionError("Failed to get AP mode confirmation from X-Plane")

    def AP_enable(self):
        self.write_int("sim/cockpit2/autopilot/flight_director_mode", 2)

    def AP_disable(self):
        self.write_int("sim/cockpit2/autopilot/flight_director_mode", 0)

    def enable_override(self, state: bool = True):
        self.write_int("sim/operation/override/override_autopilot", state)

    def enable_autothrottle(self, state: bool = True):
        self.write_int("sim/cockpit2/autopilot/autothrottle_enabled", state)

    def select_mode(self, mode: Union[VertMode, LatMode]):
        self.write_bit(self.AP_STATE_WORD, mode, True)

    def set_ias(self, ias: float, units='ms'):
        ias_kts = ias if units=='kts' else ias / MS_PER_KT
        self.write_float("sim/cockpit2/autopilot/airspeed_dial_kts_mach", ias_kts)

    def set_baroalt(self, h_baro: float, units='m'):
        h_baro_feet = h_baro if units=='feet' else h_baro / M_PER_FOOT
        self.write_float("sim/cockpit2/autopilot/altitude_dial_ft", h_baro_feet)

    def set_climb_rate(self, hdot: float, units='ms'):
        hdot_fpm = hdot if units=='fpm' else hdot / M_PER_FOOT * 60
        self.write_float("sim/cockpit2/autopilot/vvi_dial_fpm", hdot_fpm)

    def set_pitch_angle(self, theta: float, units='rad'):
        theta_deg = theta if units=='deg' else theta / RAD_PER_DEG
        self.write_float("sim/cockpit2/autopilot/sync_hold_pitch_deg", theta_deg)

    def set_bank_angle(self, phi: float, units='rad'):
        phi_deg = phi if units=='deg' else phi / RAD_PER_DEG
        self.write_float("sim/cockpit2/autopilot/sync_hold_roll_deg", phi_deg)

    def set_hdg_mag(self, psi_mag: float, units='rad'):
        psi_mag_deg = psi_mag if units=='deg' else psi_mag / RAD_PER_DEG
        self.write_float("sim/cockpit2/autopilot/heading_dial_deg_mag_pilot", psi_mag_deg)
class OldInterface(Interface):

    def set_location(self, loc, units='rad'):

        lat, lon = loc
        if units=='rad': #convert rad to deg
            lat = loc[0] / RAD_PER_DEG
            lon = loc[1] / RAD_PER_DEG
        elif units=='deg':
            lat = loc[0]
            lon = loc[1]
        else:
            raise ValueError("Invalid units")

        self._client.sendPOSI([lat, lon])


    def set_altitude(self, h, units='m'):

        if units=='feet': #convert feet to m
            h_m = h * M_PER_FOOT
        elif units=='m':
            h_m = h
        else:
            raise ValueError("Invalid units")

        self._client.sendPOSI([-998, -998, h_m])


    def set_wind_vector(self, vw_NE, SI_input=True):

        assert len(vw_NE)==2, "Can only specify wind components in the NE plane"

        vw_NE_from = -np.array(vw_NE)
        norm_w = np.linalg.norm(vw_NE_from)
        norm_w_kts = norm_w if not SI_input else norm_w / MS_PER_KT
        xi_w_from = np.arctan2(vw_NE_from[1], vw_NE_from[0])

        self._client.sendDREFs(['sim/weather/wind_speed_kt[0]',
                                'sim/weather/wind_direction_degt[0]'],
                                [norm_w_kts,
                                 xi_w_from / RAD_PER_DEG])
