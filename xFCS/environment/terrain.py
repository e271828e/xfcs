from abc import ABC, abstractmethod
from collections import namedtuple as NT
from enum import IntEnum

import numpy as np
import xFCS.environment.wgs84 as wgs84
from xFCS.common.system import System

TrnData = NT('TrnData', ('h_gnd', 'k_t_n', 'condition'))

#k_t_n: NED coordinates of the terrain surface normal k_t (positive pointing
#inwards). perfectly horizontal terrain has k_t_n = (0, 0, 1)

class SurfaceCondition(IntEnum):
    Dry = 0
    Wet = 1
    Icy = 2


class Terrain(System):

    def __init__(self):
        super().__init__()

    # @abstractmethod
    def terrain_altitude(self, P: wgs84.Point):
        pass

    # @abstractmethod
    def surface_normal(self, P: wgs84.Point):
        pass

    # @abstractmethod
    def surface_condition(self, P: wgs84.Point):
        pass

    def __call__(self, P: wgs84.Point):
        return TrnData(h_gnd = self.terrain_altitude(P),
                       k_t_n = self.surface_normal(P),
                       condition = self.surface_condition(P))


class Flat(Terrain):

    def __init__(self,
                 h: float = 0.0,
                 k_t_n = np.array((0, 0, 1)),
                 condition: SurfaceCondition = SurfaceCondition.Dry):

        self._h = h
        self._k_t_n = k_t_n
        self._condition = condition
        super().__init__()

    def terrain_altitude(self, P: wgs84.Point):
        return self._h

    def surface_normal(self, P: wgs84.Point):
        return self._k_t_n

    def surface_condition(self, P: wgs84.Point):
        return self._condition
