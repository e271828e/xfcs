from warnings import warn

import numpy as np
from xFCS.common.attitude import Attitude3D as Att3D

#pylint: disable=invalid-name
#pylint: disable=non-ascii-name

π = np.pi

#WGS84 fundamental constants, SI units
GM = 3.986005e+14 #Gravitational constant
a = 6378137 #Equatorial radius
f = 1/298.257223563 #Ellipsoid flattening
ω_ie = 7.292115e-05 #Earth's angular velocity with respect to the ECI frame

#derived parameters
b = a * (1 - f) #Polar semi-minor axis
e_sq = 2*f - f**2 #First eccentricity squared
eʹ_sq = e_sq / (1 - e_sq) #Second eccentricity squared
eʹ = np.sqrt(eʹ_sq) #Second eccentricity

#convenience parameters
a_sq = a**2
b_sq = b**2
q_0 = 0.5 * ((1 + 3/eʹ_sq) * np.arctan(eʹ) - 3/eʹ) #[Hof06]2-113
qʹ_0 = 3 * (1 + 1/eʹ_sq) * (1 - 1/eʹ * np.arctan(eʹ)) - 1 #[Hof06]2-133 with u = b and [Hof06]2-138
m = ω_ie**2 * a**2 * b / GM #[Hof06] 2-70

#normal gravity magnitudes
γ_a = GM / (a * b) * (1 - m - m/6 * eʹ * qʹ_0 / q_0) #Normal gravity at the equator, [Hof06] 2-141
γ_b = GM / (a**2) * (1 + m/3 * eʹ * qʹ_0 / q_0) #Normal gravity at the poles, [Hof06] 2-142

class Point:
    """
    Defines a location relative to the WGS84 ellipsoid.

    Non-singular internal representation is provided by:
    a) An n-vector defining the horizontal location on the ellipsoid's surface
    b) Altitude above the ellipsoid surface along the local vertical

    Units are SI unless otherwise specified.
    """

    #numerical tolerances for altitude and n-vector components
    _eps_h = 1e-7
    _eps_n = 1e-10

    def __init__(self, n_e = np.array(( 1, 0, 0 )), h = 0, bypass_val = False):
        """
        Default constructor. New instances should be created instead using the
        provided class methods.

        Args:

        n_e (numpy array of shape (3,)): n-vector representing the location on
        the WGS84 ellipsoid.

        h (numeric): Altitude over the WGS84 ellipsoid along the local vertical.
        """

        if not bypass_val:
            if not isinstance(n_e, np.ndarray) or np.shape(n_e) != (3,):
                raise TypeError("n-vector must be provided as a 3-element \
                    numpy array of floats. For other input types, \
                    use the provided classmethods.")

            norm_n_e = np.sqrt((n_e**2).sum())
            if abs(norm_n_e - 1) > self._eps_n:
                warn("n-vector norm outside tolerance, normalizing...", RuntimeWarning)
                n_e /= norm_n_e

            try:
                h = float(h)
            except ValueError as exc:
                raise ValueError("Failed converting altitude to float") from exc

        self._n_e = n_e
        self._h = h


    @classmethod
    def from_nh(cls, *nh, bypass_val = False):
        """
        Creates a Point from an n-vector and an altitude value.

        Args:

        nh (2-element sequence or two separate arguments): n-vector (3-element
        sequence) and WGS84 altitude (scalar float).
        """

        n_args = len(nh)

        if n_args == 1:
            n_e, h = nh[0] #as a 2-element sequence
        elif n_args == 2:
            n_e, h = nh #as two separate arguments
        else:
            raise ValueError("Received more than two positional arguments.\
                n-vector and altitude must be provided as a 2-element \
                sequence or as two separate arguments")

        n_e = np.array(n_e)

        return cls( n_e, h, bypass_val)


    @classmethod
    def from_geo(cls, *geo, bypass_val = False):
        """
        Creates a Point from a set of geodetic coordinates.

        Args:

        geo (3-element sequence of three scalar floats): Latitude (φ), longitude
        (λ), WGS84 altitude (h)
        """

        n_args = len(geo)

        if n_args == 1:
            φ, λ, h = geo[0]
        elif n_args == 3:
            φ, λ, h = geo
        else:
            raise ValueError("Geodetic coordinates must be provided either as \
                a 3-element sequence or as 3 separate arguments")

        if not bypass_val:
            if abs(φ) > π/2:
                raise ValueError("Latitude must be within [-π/2, π/2], input: " + str(φ))
            if abs(λ) > π:
                raise ValueError("Longitude must be within [-π, π], input: " + str(λ))
            if h < -20000:
                raise ValueError("Altitude must be above -20000 m, input: " + str(h))

        sin_φ = np.sin(φ); sin_λ = np.sin(λ)
        cos_φ = np.cos(φ); cos_λ = np.cos(λ)

        n_e = np.array((cos_φ * cos_λ, cos_φ * sin_λ, sin_φ))

        return cls(n_e, h, bypass_val = True)


    @classmethod
    def from_r_ECEF(cls, r_e_eP = None, max_iter = 10):
        """
        Creates a Point from an ECEF position vector. Solution is found
        iteratively using h=0 as an initial approximation. See Hoffmann &
        Moritz.

        Args:

        r_e_eP (sequence of 3 floats): Position vector from the ellipsoid center
        (Oe) to the point represented by the class instance (P), resolved in the
        ECEF frame (e)

        max_iter (int): Maximum number of iterations for the iterative solution.
        """

        r_x, r_y, r_z = r_e_eP
        p = np.sqrt(r_x**2 + r_y**2)

        if p > abs(r_z): #abs(φ) closer to 0, use tan_φ as iteration variable

            x_0 = r_z / (p * (1 - e_sq)) #tan_φ(N, h = 0)

            def step(tan_φ):
                #positive within [-π/2, π/2]
                cos_φ = 1 / np.sqrt(1 + tan_φ**2)
                N = a / np.sqrt(1 - e_sq * (1 - cos_φ**2))
                h = p / cos_φ - N
                tan_φ = r_z / (p * (1 - e_sq * N / (N + h)))
                return (tan_φ, (N, h))

        else:  #abs(φ) closer to π/2, use cot_φ as iteration variable

            x_0 = p * (1 - e_sq) / r_z #cot_φ(N, h = 0)

            def step(cot_φ):
                #within [-π/2, π/2] sign(sin_φ) = sign(cot_φ)
                sin_φ = np.sign(cot_φ) / np.sqrt(1 + cot_φ**2)
                N = a / np.sqrt(1 - e_sq * sin_φ**2)
                h = r_z / sin_φ - (1 - e_sq) * N
                cot_φ = p * (1 - e_sq * N / (N + h)) / r_z
                return (cot_φ, (N, h))

        h_0 = 0.0
        for _ in range(max_iter):
            x, (N, h) = step(x_0)
            if abs(x - x_0) < cls._eps_n and abs(h - h_0) < cls._eps_h:
                break
            x_0 = x
            h_0 = h
        else:
            raise GeneratorExit("Iteration terminated without convergence")

        n_e = np.array((
            r_x / (N + h),
            r_y / (N + h),
            r_z / ((1 - e_sq) * N + h)
        ))

        return cls(n_e, h, bypass_val = True)


    @classmethod
    def from_LTFh(cls, *LTFh):
        """
        Creates a Point from its local tangent frame (LTF) and a WGS84 altitude.

        The LTF (l) is a generalization of the NED frame (n). It is obtained by
        rotating the NED frame an angle ψ_nl around its z axis. For ψ_nl = 0,
        both frames coincide.

        The LTF components of the n-vector (n_w) are simply [0, 0, -1]. Thus,
        its ECEF components (n_e) can be extracted directly from the last column
        of the ECEF-to-LTF rotation matrix. The azimuth ψ_nl can be extracted
        from its last row.

        Args:

        LTFh (2-element sequence or two separate arguments): ECEF-to-LTF
        attitude (Attitude3D instance) and WGS84 altitude (scalar float).

        Returns:

        P (Point): Point instance defined by LTFh.

        ψ_nl (float): Azimuth angle of the supplied LTF.
        """

        n_args = len(LTFh)

        if n_args == 1:
            LTF, h = LTFh[0] #as a 2-element sequence
        elif n_args == 2:
            LTF, h = LTFh #as two separate arguments
        else:
            raise ValueError("Received more than two positional arguments.\
                Local tangent frame and altitude must be provided as a \
                2-element sequence or as two separate arguments")

        try:
            R_el = LTF.rmat
        except AttributeError as exc:
            raise TypeError("First argument must be an Attitude3D instance.") \
            from exc

        n_e = - R_el[:,2]
        ψ_nl = np.arctan2( - R_el[2,1], R_el[2,0] )

        return (cls.from_nh(n_e, h), ψ_nl)


    def __repr__(self):

        return (type(self).__name__ + "(n-vector = " + str(self._n_e) +
                ", altitude = " + str(self._h) + ")")


    def __add__(self, r_e_PQ):
        """
        Addition operator overload.

        If self represents a Point P, and r_e_PQ is the ECEF vector from P to
        Point Q, P + r_e_PQ returns Q.

        Args:

        r_e_PQ (numpy.ndarray): ECEF vector from P to Q

        Returns:

        Q (Point): Point whose position vector is r_e_eQ = r_e_eP + r_e_PQ.
        """

        P = self
        if not isinstance(r_e_PQ, np.ndarray) or r_e_PQ.shape != (3,):
            raise TypeError("Input must be a 3-element numpy array")

        r_e_eP = P.r_ECEF
        r_e_eQ = r_e_eP + r_e_PQ
        return Point.from_r_ECEF(r_e_eQ)


    def __sub__(self, P):
        """
        Subtraction operator overload.

        If self represents a Point Q and P is another Point, Q - P returns the
        ECEF vector r_e_PQ from P to Q

        Args:

        P (Point)

        Returns:

        r_e_PQ (numpy.ndarray): ECEF vector from P to Q
        """

        Q = self
        if not isinstance(P, Point):
            raise TypeError("Right operand must be another WGS84 Point instance")

        r_e_eQ = Q.r_ECEF
        r_e_eP = P.r_ECEF
        r_e_PQ = r_e_eQ - r_e_eP
        return r_e_PQ


    def __eq__(self, other):
        """
        Equality operator overload. Tests for equality applying the
        class-defined tolerances for n-vector components and altitude, with an
        order of magnitude of margin.
        """

        return (abs(self._n_e - other._n_e) < 10 * self._eps_n).all() and\
                abs(self._h - other._h) < 10 * self._eps_h


    def LTF(self, ψ_nl = 0):
        """
        Computes the local tangent frame corresponding to Point self.

        This method returns an Attitude3D instance representing the ECEF-to-LTF
        attitude for Point self. It is constructed through rotation composition:
        the first two rotations yield the NED frame, and the third one yields
        the LTF frame.

        Args:

        ψ_nl (float): Azimuth angle from the NED frame to the LTF frame.

        Returns:

        q_el (Attitude3D): ECEF-to-LTF attitude.
        """

        return (Att3D.from_axis_angle( [0, 0, 1], self.lon, bypass_val = True ) *
                Att3D.from_axis_angle( [0, -1, 0], self.lat + π/2, bypass_val = True ) *
                Att3D.from_axis_angle( [0, 0, 1], float(ψ_nl), bypass_val = True) )


    def LTFh(self, ψ_nl = 0):
        """
        Returns a tuple containing LTF and altitude

        Point self can be recovered from this method's output using the
        from_LTFh method.
        """
        return self.LTF(ψ_nl), self._h


    @property
    def n_e(self):
        """
        Returns n-vector ECEF components
        """
        return self._n_e


    @property
    def lat(self):
        """
        Returns WGS84 latitude
        """
        return np.arctan2(self._n_e[2], np.sqrt(self._n_e[0]**2 + self._n_e[1]**2))


    @property
    def lon(self):
        """
        Returns WGS84 longitude
        """
        return np.arctan2(self._n_e[1], self._n_e[0])


    @property
    def h(self):
        """
        Returns WGS84 altitude
        """
        return self._h


    @property
    def nh(self):
        """
        Returns a tuple of n-vector and altitude
        """
        return self._n_e, self._h


    @property
    def geo(self):
        """
        Returns a tuple of geodetic coordinates
        """
        return self.lat, self.lon, self._h


    @property
    def r_ECEF(self):
        """
        Returns:

        r_e_eP (numpy.ndarray): Position vector from the ellipsoid center (Oe)
        to the point represented by the Point instance self (P), resolved in the
        ECEF frame (e)
        """

        _, N = self.radii
        h = self._h

        r_e_eP = np.array([
            (N + h) * self._n_e[0],
            (N + h) * self._n_e[1],
            (N * (1 - e_sq) + h) * self._n_e[2]
        ])

        return r_e_eP


    @property
    def radii(self):
        """
        Computes WGS84 ellipsoid's meridian and transverse (prime-vertical)
        curvature radii.

        Returns:

        M (float): WGS84 ellipsoid's meridian radius of curvature

        N (float): WGS84 ellipsoid's transverse radius of curvature
        """

        f_den = np.sqrt(1 - e_sq * self._n_e[2]**2)
        M = a * (1 - e_sq) / f_den**3 #R_N
        N = a / f_den #R_E

        return (M, N)


    @property
    def gravity(self):
        """
        Computes normal gravity above the WGS84 ellipsoid for small altitudes
        (h<<a). Uses Somigliana's formula for gravity at the ellipsoid surface with
        a second order altitude correction. See Hoffmann & Moritz.

        Returns:

        g_l (numpy.ndarray): Normal gravity vector resolved in the LTF
        """

        sin_φ_sq = (self._n_e[2])**2
        cos_φ_sq = self._n_e[0]**2 + self._n_e[1]**2
        h = self._h

        #gravity at the ellipsoid surface (Somigliana)
        γ_0 = ( (a * γ_a * cos_φ_sq + b * γ_b * sin_φ_sq) /
            np.sqrt(a_sq * cos_φ_sq + b_sq * sin_φ_sq) )#[Hof06] 2-146

        #altitude correction
        γ = γ_0 * (1 - 2/a * (1 + f + m - 2 * f * sin_φ_sq) * h + 3/a_sq * h**2)

        g_l = np.array((0,0,γ))

        return g_l


    def gravitation(self):
        #TODO
        pass
