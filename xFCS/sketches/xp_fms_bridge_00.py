import time
import functools
import socket
import json
import numpy as np
from xFCS.io.xpinterfaces import NavInterface, APInterface, DispInterface

p0 = 101325
ρ0 = 1.225
γ = 1.400

def timing(f):

    @functools.wraps(f)
    def wrap(*args, **kwargs):
        t_s = time.perf_counter()
        output = f(*args, **kwargs)
        t_e = time.perf_counter()
        print(f"Call to {f.__name__}({args}, {kwargs}) took {1000*(t_e - t_s)} ms")
        return output
    return wrap


class FMSInterface():

    def __init__(self, timeout:float, address=('127.0.0.1', 1060)):

        #don't let the socket block, because if the server is initially not
        #ready and does not receive our first request, we would wait
        #indefinitely for an answer that will not come and never try again.

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.settimeout(timeout)
        self.server_address = address
        self.max_bytes = 4096

    def send(self, state_vector: dict):

        states = ('lat', 'lon', 'alt', 'v_N', 'v_E', 'v_D',
                  'vw_N', 'vw_E', 'vw_D', 'TAS')
        message_dict = {s: state_vector[s] for s in states}

        message_str = json.dumps(message_dict)
        message_bytes = message_str.encode('ascii')

        try:
            self.socket.sendto(message_bytes, self.server_address)
        except socket.timeout:
            print("Timed out while trying to send server request")

    def receive(self):

        message_dict = None
        try:
            message_bytes = self.socket.recv(self.max_bytes)
            message_str = message_bytes.decode('ascii')
            message_dict = json.loads(message_str)

        except socket.timeout:
            print("Timed out while waiting for server response")

        return message_dict


def init_ap(ap: APInterface):

    ap.AP_enable()
    ap.enable_override()
    ap.enable_autothrottle()
    ap.select_mode(ap.LatMode.BANK_HOLD)
    ap.select_mode(ap.VertMode.CLIMB_RATE)
    ap.set_climb_rate(0, units='fpm')
    ap.set_bank_angle(5, units='deg')
    ap.set_ias(90, units='kts')


def set_ap_demands(ap: APInterface, fms_commands: dict, state_vector: dict):

    ap.set_climb_rate(fms_commands['hdot'])
    ap.set_bank_angle(fms_commands['phi'])
    ap.set_ias(TAS_to_CAS(fms_commands['TAS'],
                          p=state_vector['p'],
                          ρ=state_vector['rho']))


def TAS_to_CAS(TAS: float, p: float, ρ: float):

    pt = p * (1 + (γ-1)/(2*γ) * ρ/p * TAS**2) ** (γ/(γ-1))
    Δp = pt - p
    CAS = np.sqrt( (2*γ)/(γ-1) * p0/ρ0 * ( (1 + Δp/p0) ** ((γ-1)/γ) - 1) )
    return CAS


def main_loop():

    T_TIMEOUT = 3
    T_RETRY = 5

    sim = DispInterface()
    nav = NavInterface()
    ap = APInterface()
    fms = FMSInterface(T_TIMEOUT)

    init_ap(ap)
    sim.set_location((43.3722, -5.4146),  units='deg')
    sim.set_altitude(1500, units='m')

    while True:

        state_vector = nav.get_state_update()
        try:
            fms.send(state_vector)
            fms_output = fms.receive()
            if fms_output:
                set_ap_demands(ap, fms_output, state_vector)
        except ConnectionResetError:
            print("Can't reach the server, will try again " +
                  f"in {T_RETRY} seconds...")
            time.sleep(T_RETRY)

if __name__ == "__main__":
    main_loop()