import multiprocessing as mp
import threading as thr
import queue
import time
import functools
import psutil
import socket
from xFCS.io.xpinterfaces import NavInterface, APInterface

#architecture based on sketch_03a

#TODO:
#add logging
#create generic thread subclasses read_from_IO, send_to_IO
#add a barrier to wait for all threads to be ready


# Aquitectura alternativa:

# Meter IONav, IOAct, IOGuid, Main en un mismo proceso RealTime
# Crear otro proceso Background con un dummy CPU-intensive como prueba de
# concepto

# Main thread: Computes FMS inputs and Autopilot inputs
# IONav thread: Requests, receives and processes state vector updates from XPlane, and relaying them to Main through a Queue.
# IOAct thread: Receives autopilot command updates from Main through a Queue and sending them to XPlane
# IOFMS thread: Receives FMS inputs from Main, sends them to the FMS application
# and receives Guidance inputs and sends them to Main

#All threads in the main process


class CustomProcess(mp.Process):

    def __init__(self, *, affinity:list=None, priority=None, **kwargs):

        super().__init__(**kwargs)
        self._affinity = affinity
        self._priority = priority

    def run(self):

        proc = psutil.Process()
        if self._affinity is not None:
            proc.cpu_affinity(self._affinity)
        if self._priority is not None:
            proc.nice(self._priority)
        print(f"Starting process {self.name} with PID {proc.pid}, " +
              f"CPU affinity: {proc.cpu_affinity()}, priority: {proc.nice()}")

        if self._target:
            self._target(*self._args, **self._kwargs)


def timing(f):

    @functools.wraps(f)
    def wrap(*args, **kwargs):
        t_s = time.perf_counter()
        output = f(*args, **kwargs)
        t_e = time.perf_counter()
        print(f"Call to {f.__name__}({args}, {kwargs}) took {1000*(t_e - t_s)} ms")
        return output
    return wrap


def thr_IONav(q_Nav2Ctl: mp.Queue, shutdown: mp.Event):

    @timing
    def get_state_vector():
        sv = nav_int.get_state_update(('lat',))
        return sv
        # for x in range(2000000):
        #     dummy = x**2

    thread_name = thr.current_thread().name

    nav_int = NavInterface()

    dt = 1.0
    i = 0
    t_last = t_previous = time.perf_counter()

    while not shutdown.is_set():

        t_current = time.perf_counter()
        print(f"\n{thread_name}: Iteration {i}, " +
                f"actual delta time: {t_current - t_previous}")
        t_previous = t_current

        sv = get_state_vector()

        try:
            q_Nav2Ctl.put(sv, timeout=3)
            print(f"{thread_name}: Pushed a state vector update to queue {q_Nav2Ctl}, " +
                f"current size: {q_Nav2Ctl.qsize()}")
        except queue.Full:
            print(f"{thread_name}: Timed out while waiting for a slot " +
                    "in the output queue")
            break

        t_next = t_last + dt
        try:
            time.sleep(t_next - time.perf_counter())
        except ValueError: #negative sleep time
            print(f"{thread_name}: PANIC!!! Missed state vector update epoch!" +
                    "We're lagging behind!")
        t_last = t_next
        i += 1

    print(f"{thread_name}: Exiting...")


def thr_IOAct(q_Ctl2Act: mp.Queue, shutdown: mp.Event):

    def configure_AP():
        AP_int.AP_enable()
        AP_int.enable_override()
        AP_int.enable_autothrottle()
        AP_int.select_mode(AP_int.LatMode.BANK_HOLD)
        AP_int.select_mode(AP_int.VertMode.CLIMB_RATE)

    thread_name = thr.current_thread().name

    AP_int = APInterface()
    configure_AP()

    while not shutdown.is_set():

        n_pending = q_Ctl2Act.qsize()
        if n_pending > 1:
            print(f"{thread_name}: PANIC! {n_pending} pending autopilot command updates, "
                + "expected at most 1. We are lagging behind!")
            while q_Ctl2Act.qsize() > 1: #discard obsolete messages
                _ = q_Ctl2Act.get_nowait()

        try:
            #block until a new autopilot command arrives
            act = q_Ctl2Act.get(timeout=3)
            print(f"{thread_name}: Popped an autopilot command update from queue {q_Ctl2Act}, " +
                f"current size: {q_Ctl2Act.qsize()}")
            if act is None: #this is a signal from the main process to stop waiting
                print(f"{thread_name}: Got a None value from the queue")
                break
        except queue.Empty:
            print(f"{thread_name}: Timed out while waiting for an autopilot command update")
            break

        print(f"{thread_name}: Got the following autopilot command update: {act}")
        AP_int.set_ias(act['IAS'], units='kts')
        AP_int.set_climb_rate(act['climb_rate'], units='fpm')
        AP_int.set_bank_angle(act['bank_angle'], units='deg')
        #eventually do something with act

    print(f"{thread_name}: Exiting...")


def thr_CtlMain(q_Nav2Ctl: mp.Queue, q_Ctl2Act: mp.Queue, shutdown: mp.Event):

    @timing
    def compute_actuation_commands(sv):
        #increase range arbitrarily to simulate longer data processing
        for x in range(4000000):
            dummy = x ** 0.5
        # geo_pos = (sv['lat'], sv['lon'], sv['alt'])
        # v_NED = (-sv['v_S'], sv['v_E'], -sv['v_U'])
        # vw_NED = (-sv['vw_S'], sv['vw_E'], -sv['vw_U'])

        # #output to FMS:
        # alt, lon, alt
        # xi_g = np.atan2(v_NED[1], v_NED[0])
        # TAS
        # vw_NED

        # #input demands from FMS
        # climb_rate/h, TAS, xi_g_dot

        ap_demands = {'IAS': 100, 'climb_rate': -100, 'bank_angle': 5}
        return ap_demands

    # def init_FMS_interface():
    #     sck = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    #     sck.bind(('127.0.0.1', port))
    #     pass

    thread_name = thr.current_thread().name

    while not shutdown.is_set():

        n_pending = q_Nav2Ctl.qsize()
        if n_pending > 1:
            print(f"{thread_name}: PANIC! {n_pending} pending state vector updates, "
                + "expected at most 1. We are lagging behind!")
            while q_Nav2Ctl.qsize() > 1: #drop obsolete state vector messages
                _ = q_Nav2Ctl.get_nowait()

        try:
            #block until a sv update arrives
            sv = q_Nav2Ctl.get(timeout=3)
            print(f"{thread_name}: Popped a state vector update from queue {q_Nav2Ctl}, " +
                f"current size: {q_Nav2Ctl.qsize()}")
            if sv is None: #this is a signal from the main process to stop
                print(f"{thread_name}: Got a None value from the queue")
                break
        except queue.Empty:
            print(f"{thread_name}: Timed out while waiting for a state vector update")
            break

        #if no update is available, this won't run
        print(f"{thread_name}: Got the following state vector update: {sv}")
        act = compute_actuation_commands(sv)

        try:
            q_Ctl2Act.put(act, timeout=3)
            print(f"{thread_name}: Pushed an autopilot command update to queue {q_Ctl2Act}, " +
                f"current size: {q_Ctl2Act.qsize()}")
        except queue.Full:
            print(f"{thread_name}: Timed out while waiting for a free autopilot command queue slot")
            break

    print(f"{thread_name}: Exiting...")


def prc_IO(q_Ctl2Act: mp.Queue, q_Nav2Ctl: mp.Queue, shutdown: mp.Event):

    threads = {'IONav': thr.Thread(name='IONav', target=thr_IONav,
                                    args=(q_Nav2Ctl, shutdown)),
                'IOAct': thr.Thread(name='IOAct', target=thr_IOAct,
                                args=(q_Ctl2Act, shutdown))}

    for thread in threads.values():
        thread.start()


def prc_Ctl(q_Nav2Ctl: mp.Queue, q_Ctl2Act: mp.Queue, shutdown: mp.Event):

    threads = {'CtlMain': thr.Thread(name='CtlMain', target=thr_CtlMain,
                                    args=(q_Nav2Ctl, q_Ctl2Act, shutdown))}

    for thread in threads.values():
        thread.start()


def main_function(time_to_run=None):

    def start_processes():
        for p in processes.values():
            p.start()

    def stop_processes():
        shutdown.set()
        #release any processes blocked on empty queues
        for q in queues.values():
            if q.qsize()==0:
                q.put(None, timeout=3)

    shutdown = mp.Event()
    queues = {'IO_Ctl': mp.Queue(), 'Ctl_IO': mp.Queue()}
    processes = {
        'IO': CustomProcess(affinity=[0], target=prc_IO,
                               args=(queues['Ctl_IO'], queues['IO_Ctl'], shutdown), name='I/O'),
        'Ctl': CustomProcess(affinity=[2], target=prc_Ctl,
                                args=(queues['IO_Ctl'], queues['Ctl_IO'], shutdown), name='Control')}

    start_processes()
    if time_to_run:
        time.sleep(time_to_run)
    print(f"\n{mp.current_process().name}: Shutting down...")
    stop_processes()


if __name__ == "__main__":
    main_function(10.0)