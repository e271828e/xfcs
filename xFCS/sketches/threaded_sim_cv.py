import threading
import time

import numpy as np
from matplotlib import pyplot as plt

import xFCS.aircraft.rigidbody as rbd
import xFCS.environment.wgs84 as wgs84
from xFCS.aircraft.aircraft import AircraftGeneric
from xFCS.common.attitude import Attitude3D as Att
from xFCS.environment.terrain import Flat
from xFCS.simulation.objects import SimAircraft
from xFCS.simulation.utils import SimObject
from xFCS.xplane.xpinterfaces import NavInterface, DispInterface
from xFCS.simulation.profiling import profile_me


class ThreadedSim:

    def __init__(self, simobj: SimObject, display: DispInterface,
           dt = 1e-2, t_start = 0, t_end = 10,
           log_decimation = 1, io_decimation = 1, disp_decimation = 1):

        self.dt = dt
        self.t_start = t_start
        self.t_end = t_end
        self.log_decimation = log_decimation
        self.io_decimation = io_decimation
        self.disp_decimation = disp_decimation
        self.display = display
        self.simobj = simobj
        self.log = None
        self.display_outputs = None

        self.event_shutdown = threading.Event()
        self.event_display = threading.Event()
        self.cond_display = threading.Condition()



    def run(self):

        t_main = threading.Thread(target = profile_me(self.thr_main))
        t_disp = threading.Thread(target = self.thr_disp)
        t_main.start()
        t_disp.start()
        print(f"Active threads: {threading.active_count()}")
        t_main.join()
        t_disp.join()


    def thr_disp(self):

        while not self.event_shutdown.is_set():
            with self.cond_display: #acquire the lock
                self.cond_display.wait() #release the lock and block
                # notified either to display or shutdown, awakened and lock reacquired.
                if self.event_display.is_set():
                    #send stuff to xplane
                    self.event_display.clear()
                    self.display.set_pos_att(*self.display_outputs)

        print("Received shutdown event, terminating...")


    def thr_main(self):

        t_log = []
        y_log = []
        i = 0
        t = self.t_start
        treal_last = treal_previous = time.perf_counter()

        self.display.disable_physics()

        while t < self.t_end:

            if i % self.io_decimation == 0:
                #signal output thread to get y and put it out to control process
                pass #here

            if i % self.log_decimation == 0:
                y = self.simobj.output(t) #get the latest output vector
                t_log.append(t)
                y_log.append(y)

            if i % self.disp_decimation == 0:
                #acquire lock to write display outputs, notify the disp thread
                with self.cond_display:
                    self.display_outputs = (y['kin'].pos.Ob, y['kin'].att.n_b)
                    self.event_display.set()
                    self.cond_display.notify()

            # treal_current = time.perf_counter()
            # print(f"\nSimulation: Iteration {i}, " +
            #         f"actual delta time: {treal_current - treal_previous}")
            # treal_previous = treal_current

            self.simobj.update(t, self.dt) #update aircraft state to t_next
            t += self.dt

            ###### UNCOMMENT FOR REAL TIME
            # treal_next = treal_last + self.dt
            # treal_now = time.perf_counter()
            # if treal_next > treal_now:
            #     # print(f"Sleeping for {treal_next - treal_now}")
            #     time.sleep(treal_next - treal_now)
            #     treal_wakeup = time.perf_counter()
            #     if treal_wakeup > treal_next:
            #         # print(f"Overslept by {treal_wakeup - treal_next}\n")
            #         pass
            # treal_last = treal_next

            # in theory, we should now be exactly at treal_next, but this won't be
            # the case due to the inaccuracy of time.sleep()
            i += 1

        self.log = (t_log, y_log)

        print("Simulation done, signaling shutdown...")
        self.event_shutdown.set()
        with self.cond_display:
            self.cond_display.notify()


        return


#start X-Plane scenario here
display = DispInterface()
navint = NavInterface()
xp_loc = navint.get_state_update(('lat', 'lon', 'alt', 'psi', 'theta', 'phi'))

#define terrain altitude as a negative offset with respect to the initial
#altitude matching the resting length of the strut (supporting weight)$
trn = Flat(xp_loc['alt'] - .9)
av_system = AircraftGeneric(trn)

#we can do either x = av_simobj.x or x = av_system.x, they're references to
#the same object
x = av_system.x
u = av_system.u

#get the at-rest x-plane altitude and use it for initialization
init = rbd.InitialCondition(
    Ob = wgs84.Point.from_geo(xp_loc['lat'], xp_loc['lon'], xp_loc['alt'] + 0.25),
    n_b = Att.from_euler(xp_loc['psi'], 0.1, 0.05),
    ω_wb_b = np.array((0., 0, 0)),
    v_eOb_b = np.array((30, 0, 0)))


x.airframe = av_system.airframe.x0(init)
u.ldg.nose.psi = 0.05
u.ldg.rmain.brk = 1
u.ldg.lmain.brk = 0
u.pwp.pwp.throttle = 4

av_simobj = SimAircraft(system = av_system)
sim = ThreadedSim(aircraft = av_simobj, display = display, disp_decimation=1, dt=1e-2)
sim.run()


t = sim.log[0]
samples = sim.log[1]
kin_samples = [s['kin'] for s in samples]
kin_figures = av_system.airframe.figures(t, kin_samples)

print(samples[-1]['kin'].pos.Ob)
