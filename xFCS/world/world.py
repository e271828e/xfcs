from collections import OrderedDict as OD

from xFCS.common.system import System
from xFCS.aircraft.aircraft import AircraftBase
from xFCS.environment.terrain import Terrain
from xFCS.environment.atmosphere import Atmosphere
from warnings import warn


class World(System):

    def __init__(self,
                 aircraft: AircraftBase,
                 terrain: Terrain,
                 atmosphere: Atmosphere):

        if aircraft.trn is not terrain:
            warn("Aircraft's terrain model reference does not match the\
                Terrain object passed to the World constructor\n")

        if aircraft.atm is not atmosphere:
            warn("Aircraft's atmosphere model reference does not match the\
                Atmosphere object passed to the World constructor\n")

        subsystems = OD([('aircraft', aircraft),
                         ('trn', terrain),
                         ('atm', atmosphere)])

        super().__init__(subsystems)