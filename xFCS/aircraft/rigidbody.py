import numpy as np
import os
import matplotlib.pyplot as plt
from collections import OrderedDict as OD
from collections import namedtuple as NT
import xFCS.environment.wgs84 as wgs84
from xFCS.common.structuredvector import NodeType
from xFCS.common.quaternion import Quaternion as Quat
from xFCS.common.attitude import Attitude3D as Att
from xFCS.common.math import cross, v2skew
from xFCS.common.system import System, SystemOutput
from xFCS.common.plotting import PlotVar, make_figure


#defines the transformation from a reference frame f1(O1, Ɛ1) to a local frame
#f2(O2, Ɛ2) by:
# a) the position vector of the local frame origin O2 relative to the reference
#frame origin O1, projected in the reference frame axes
# b) the attitude of the local frame axes relative to the reference frame axes
FrameTransform = NT(typename = 'FrameTransform',
                    field_names = ('r_O1O2_1', 'att_12'),
                    defaults = (np.zeros(3), Att.null()))

Twist = NT(typename = 'Twist',
           field_names = ('ω', 'v'),
           defaults = (np.zeros(3), np.zeros(3)))


class Wrench:

    def __init__(self, *, F: np.ndarray = None, T: np.ndarray = None):

        if F is None:
            F = np.zeros(3)
        else:
            self.F = F.copy()

        if T is None:
            T = np.zeros(3)
        else:
            self.T = T.copy()

    def __iadd__(self, other):

        self.F += other.F
        self.T += other.T
        return self

    def __add__(self, other):

        #deadly bug: creates a Wrench with a REFERENCE to the old one's F and T
        # new_wrench = Wrench(F = self.F, T = self.T)
        new_wrench = self.__copy__()
        new_wrench += other
        return new_wrench

    def __copy__(self):

        return Wrench(F = self.F.copy(), T = self.T.copy())

    def translate(self, frame_transform: FrameTransform):

        #translates a wrench specified on a local frame f2(O2, ε2) to a
        #reference frame f1(O1, ε1) given the frame transform from 1 to 2

        wrench_f2 = self #just for clarity

        F_O2_2 = wrench_f2.F
        T_O2_2 = wrench_f2.T

        #project on the reference axes
        F_O2_1 = frame_transform.att_12.transform(F_O2_2)
        T_O2_1 = frame_transform.att_12.transform(T_O2_2)

        #translate them to airframe origin
        F_O1_1 = F_O2_1
        T_O1_1 = T_O2_1 + cross(frame_transform.r_O1O2_1, F_O2_1)

        wrench_f1 = Wrench(F = F_O1_1, T = T_O1_1)

        return wrench_f1

    @classmethod
    def null(cls):
        return cls(F = np.zeros(3), T = np.zeros(3))

    def __repr__(self):
        return (type(self).__name__ + "(" +
                "F = " + repr(self.F) +
                ", T = " + repr(self.T) + ")")


InitialCondition = NT(typename = 'InitialCondition',
          field_names = ('n_b', 'Ob', 'ω_wb_b', 'v_eOb_b'),
          defaults = (Att.null(), wgs84.Point(), np.zeros(3), np.zeros(3)))

MassData = NT(typename = 'MassData',
              field_names = ('m', 'J_Ob_b', 'r_ObG_b'))

AttData = NT(typename = 'AttData',
          field_names = ('w_b', 'e_b', 'n_b', 'e_w', 'n_w'))

VelData = NT(typename = 'VelData',
          field_names = ('ω_eb_b', 'ω_wb_b', 'ω_ew_w', 'ω_ie_b', 'ω_ib_b',
                        'v_eOb_b', 'v_eOb_n'))

PosData = NT(typename = 'PosData',
          field_names = ('Ob'))

AccData = NT(typename = 'AccData',
          field_names = ('α_eb_b', 'α_ib_b', 'a_eOb_b', 'a_iOb_b'))

Kinematics = NT(typename = 'Kinematics',
                    field_names = ('att', 'vel', 'pos'))

class RigidBodyDynamics(System):

    XRbdWander = NodeType(name = 'XRbdWander', blocks = OD((
                                ('q_w_b',   NodeType(name = 'q_w_b', size = 4)),
                                ('q_e_w',   NodeType(name = 'q_e_w', size = 4)),
                                ('h',       NodeType(name = 'h', size = 1)),
                                ('ω_eb_b',  NodeType(name = 'ω_eb_b', size = 3)),
                                ('v_eOb_b', NodeType(name = 'v_eOb_b', size = 3)))))

    # URbdWander = NodeType(name = 'URbd', blocks = OD((
    #                         ('F_Ob_b', NodeType(name = 'F_Ob_b', size = 3)),
    #                         ('T_Ob_b', NodeType(name = 'T_Ob_b', size = 3)))))

    YRoot = NT(typename = 'YRoot', field_names = ('att', 'vel', 'pos', 'acc'))

    def _get_XType(self):
        return self.XRbdWander

    # def _get_UType(self):
    #     return self.URbdWander

    def initialize_state(self, **kwargs):

        init = kwargs.get('initial_condition')

        if init is None:
            init = InitialCondition()

        n_b, Ob, ω_wb_b, v_eOb_b = init

        ψ_nw = 0
        n_w = Att.from_axis_angle((0, 0, 1), ψ_nw)
        w_b = n_w.inv() * n_b

        h = Ob.h
        (R_N, R_E) = Ob.radii
        v_eOb_n = n_b.transform(v_eOb_b)
        ω_ew_n = np.array(( v_eOb_n[1] / (R_E + h),
                            -v_eOb_n[0] / (R_N + h),
                            0.0))

        ω_ew_b = n_b.inv().transform(ω_ew_n)
        ω_eb_b = ω_ew_b + ω_wb_b

        self.x.q_w_b = w_b.quat[:]
        self.x.q_e_w = Ob.LTF(ψ_nw).quat[:]
        self.x.h = h
        self.x.ω_eb_b = ω_eb_b
        self.x.v_eOb_b = v_eOb_b


    def f_cont(self, *, t):
        raise NotImplementedError("You should not be calling this")

    def f_disc(self, *, t):
        self.normalize()

    def normalize(self):

        x = self.x
        x.q_w_b = Quat(x.q_w_b[:]).normalize()[:]
        x.q_e_w = Quat(x.q_e_w[:]).normalize()[:]

    @property
    def kinematics(self) -> Kinematics:

        x = self.x

        #copying the data blocks from the state vector before assigning them to
        #the outputs is ESSENTIAL. otherwise, we end up with references to views
        #of the state vector, which will be obviously overwritten in the next
        #update

        w_b = Att.from_quat(x.q_w_b[:])
        e_w = Att.from_quat(x.q_e_w[:])
        h = x.h[:].copy()
        v_eOb_b = x.v_eOb_b[:].copy()
        ω_eb_b = x.ω_eb_b[:].copy()

        Ob, ψ_nw = wgs84.Point.from_LTFh((e_w, h))
        n_w = Att.from_axis_angle((0, 0, 1), ψ_nw)
        e_b = e_w * w_b
        n_b = n_w * w_b

        h = Ob.h
        (R_N, R_E) = Ob.radii
        v_eOb_n = n_b.transform(v_eOb_b)
        ω_ew_n = np.array(( v_eOb_n[1] / (R_E + h),
                           -v_eOb_n[0] / (R_N + h),
                           0.0))
        ω_ew_w = n_w.inv().transform(ω_ew_n)
        ω_ew_b = w_b.inv().transform(ω_ew_w)
        ω_wb_b = ω_eb_b - ω_ew_b

        ω_ie_e = np.array((0, 0, wgs84.ω_ie))
        ω_ie_b = e_b.inv().transform(ω_ie_e)
        ω_ib_b = ω_ie_b + ω_eb_b

        att = AttData(w_b = w_b, e_b = e_b, n_b = n_b, e_w = e_w, n_w = n_w)
        vel = VelData(ω_eb_b = ω_eb_b, ω_wb_b = ω_wb_b, ω_ew_w = ω_ew_w,
                      ω_ie_b = ω_ie_b, ω_ib_b = ω_ib_b, v_eOb_b = v_eOb_b,
                      v_eOb_n = v_eOb_n)
        pos = PosData(Ob = Ob)
        kin = Kinematics(att, vel, pos)

        return kin


    @staticmethod
    def gravity_wrench(mass: MassData, kin: Kinematics) -> Wrench:

        g_O_w = kin.pos.Ob.gravity
        #neglect the difference between gravity at Ob and G
        g_G_w = g_O_w

        #the resultant consists of the force of gravity acting on G along the
        #local vertical and a null torque
        F_G_w = mass.m * g_G_w
        T_G_w = np.zeros(3)
        wr_G_w = Wrench(F = F_G_w, T = T_G_w)

        #the local gravity frame is the LTF with origin at G
        b_w = kin.att.w_b.inv()
        gravity_frame = FrameTransform(r_O1O2_1 = mass.r_ObG_b, att_12 = b_w)

        wr_Ob_b = wr_G_w.translate(gravity_frame)

        return wr_Ob_b


    @staticmethod
    def inertia_wrench(mass: MassData, kin: Kinematics,
                       h_rot_b: np.ndarray) -> Wrench:

        #h_rot_b: additional angular momentum due to rotating airframe
        #components
        vel = kin.vel
        ω_ie_b = vel.ω_ie_b
        ω_eb_b = vel.ω_eb_b
        ω_ib_b = vel.ω_ib_b
        v_eOb_b = vel.v_eOb_b

        m = mass.m
        J_Ob_b = mass.J_Ob_b
        r_ObG_b = mass.r_ObG_b

        #angular momentum of the overall airframe as a rigid body
        h_rig_b = J_Ob_b @ ω_ib_b

        #total angular momentum
        h_all_b = h_rig_b + h_rot_b

        #simplified
        a_1_b = cross(ω_eb_b + 2 * ω_ie_b, v_eOb_b)
        F_in_Ob_b = -( m * (a_1_b + cross(ω_ib_b, cross(ω_ib_b, r_ObG_b))))
        T_in_Ob_b = - ( cross(ω_ib_b, h_all_b) + m * cross(r_ObG_b, a_1_b) )

        ## exact
        # T_in_O_b = - ( J_Ob_b @ Ω_ie_b @ ω_eb_b +
        #             Ω_ib_b @ h_all_b + m * r_ObG_b_skew @ a_1_b)
        # F_in_O_b = -( m * ( a_1_b + Ω_ib_b @ Ω_ib_b @ r_ObG_b +
        #                      r_ObG_b_skew @ Ω_eb_b @ ω_ie_b ) )

        wr_Ob_b = Wrench(F = F_in_Ob_b, T = T_in_Ob_b)

        return wr_Ob_b


    def x_dot(self, mass: MassData, kin: Kinematics,
          wr_ext_Ob_b: Wrench = Wrench.null(),
          h_rot_b: np.ndarray = np.zeros(3)) -> tuple:

        x_dot = self.XRbdWander()

        #mass matrix
        m = mass.m
        J_Ob_b = mass.J_Ob_b
        r_ObG_b = mass.r_ObG_b
        r_ObG_b_skew = v2skew(r_ObG_b)

        A = np.block([[J_Ob_b,                 m * r_ObG_b_skew],
                      [-(m * r_ObG_b_skew),    m * np.eye(3)   ]])

        #overall wrench (input, external, gravity and inertia)
        wr_g_Ob_b = self.gravity_wrench(mass, kin)
        wr_in_Ob_b = self.inertia_wrench(mass, kin, h_rot_b)
        wr_Ob_b = wr_ext_Ob_b + wr_g_Ob_b + wr_in_Ob_b

        b = np.block([ wr_Ob_b.T, wr_Ob_b.F ])

        x_dot_vel = np.linalg.solve(A, b)

        x_dot['ω_eb_b'] = x_dot_vel[0:3]
        x_dot['v_eOb_b'] = x_dot_vel[3:6]

        x_dot.q_e_w = kin.att.e_w.quat_dot(kin.vel.ω_ew_w)[:]
        x_dot.q_w_b = kin.att.w_b.quat_dot(kin.vel.ω_wb_b)[:]
        x_dot.h = np.array(-kin.vel.v_eOb_n[2])

        return x_dot


    def y(self, x_dot: XRbdWander, kin: Kinematics) -> SystemOutput:

        acc = self.accelerations(x_dot, kin)
        y_root = self.YRoot(kin.att, kin.vel, kin.pos, acc)
        y = SystemOutput(root = y_root)
        return y


    @staticmethod
    def accelerations(x_dot, kin):

        ω_eb_b_dot = x_dot['ω_eb_b'][:].copy()
        v_eOb_b_dot = x_dot['v_eOb_b'][:].copy()

        ω_eb_b = kin.vel.ω_eb_b
        ω_ie_b = kin.vel.ω_ie_b
        v_eOb_b = kin.vel.v_eOb_b

        r_eO_e = kin.pos.Ob.r_ECEF
        r_eO_b = kin.att.e_b.inv().transform(r_eO_e)

        α_eb_b = ω_eb_b_dot
        α_ib_b = ω_eb_b_dot - cross(ω_eb_b, ω_ie_b)

        a_eOb_b = v_eOb_b_dot + cross(ω_eb_b, v_eOb_b)
        a_iOb_b = v_eOb_b_dot + cross(ω_eb_b + 2 * ω_ie_b, v_eOb_b) +\
            cross(ω_ie_b, cross(ω_ie_b, r_eO_b))

        return AccData(
            α_eb_b = α_eb_b, α_ib_b = α_ib_b,
            a_eOb_b = a_eOb_b, a_iOb_b = a_iOb_b)


    def get_root_figures(self, log):

        t = log['t']
        y = log['y']

        euler_NED = [s.att.n_b.euler for s in y]

        pvars = {
            'psi':  PlotVar('Heading', r"$\psi \/ (rad)$",
                           np.array([euler[0] for euler in euler_NED])),
            'theta': PlotVar('Inclination', r"$\theta \/ (rad)$",
                             np.array([euler[1] for euler in euler_NED])),
            'phi': PlotVar('Bank', r"$\phi \/ (rad)$",
                           np.array([euler[2] for euler in euler_NED])),

            'v_x': PlotVar('X-body Velocity', r"$v_{eOb}^{b[x]} \/ (m/s)$",
                           np.array([s.vel.v_eOb_b[0] for s in y])),
            'v_y': PlotVar('Y-body Velocity', r"$v_{eOb}^{b[y]} \/ (m/s)$",
                           np.array([s.vel.v_eOb_b[1] for s in y])),
            'v_z': PlotVar('Z-body Velocity', r"$v_{eOb}^{b}[z] \/ (m/s)$",
                           np.array([s.vel.v_eOb_b[2] for s in y])),

            'v_N': PlotVar('North Velocity', r"$v_{eOb}^{n[x]} \/ (m/s)$",
                        np.array([s.vel.v_eOb_n[0] for s in y])),
            'v_E': PlotVar('East Velocity', r"$v_{eOb}^{n[y]} \/ (m/s)$",
                        np.array([s.vel.v_eOb_n[1] for s in y])),
            'v_D': PlotVar('Down Velocity', r"$v_{eOb}^{n}[z] \/ (m/s)$",
                        np.array([s.vel.v_eOb_n[2] for s in y])),

            'p': PlotVar('Roll Rate', r"$\omega_{eb}^{b[x]} \/ (rad/s)$",
                        np.array([s.vel.ω_eb_b[0] for s in y])),
            'q': PlotVar('Pitch Rate', r"$\omega_{eb}^{b[y]} \/ (rad/s)$",
                        np.array([s.vel.ω_eb_b[1] for s in y])),
            'r': PlotVar('Yaw Rate', r"$\omega_{eb}^{b[z]} \/ (rad/s)$",
                        np.array([s.vel.ω_eb_b[2] for s in y])),

            'lat': PlotVar('Latitude', r"$\varphi_{Ob} \/ (rad)$",
                        np.array([s.pos.Ob.lat for s in y])),
            'lon': PlotVar('Longitude', r"$\lambda_{Ob} \/ (rad)$",
                        np.array([s.pos.Ob.lon for s in y])),
            'h': PlotVar('Altitude', r"$h_{Ob} \/ (m)$",
                        np.array([s.pos.Ob.h for s in y])),

            'p_dot': PlotVar('Roll Acceleration', r"$\alpha_{eb}^{b[x]} \/ (rad/s^2)$",
                            np.array([s.acc.α_eb_b[0] for s in y])),
            'q_dot': PlotVar('Pitch Acceleration', r"$\alpha_{eb}^{b[y]} \/ (rad/s^2)$",
                            np.array([s.acc.α_eb_b[1] for s in y])),
            'r_dot': PlotVar('Yaw Acceleration', r"$\alpha_{eb}^{b[z]} \/ (rad/s^2)$",
                            np.array([s.acc.α_eb_b[2] for s in y])),

            'a_x': PlotVar('X-body Acceleration', r"$a_{eOb}^{b[x]} \/ (m/s^2)$",
                        np.array([s.acc.a_eOb_b[0] for s in y])),
            'a_y': PlotVar('Y-body Acceleration', r"$a_{eOb}^{b[y]} \/ (m/s^2)$",
                        np.array([s.acc.a_eOb_b[1] for s in y])),
            'a_z': PlotVar('Z-body Acceleration', r"$a_{eOb}^{b[z]} \/ (m/s^2)$",
                        np.array([s.acc.a_eOb_b[2] for s in y]))
        }


        root_figs = {
            'att': make_figure(
                title = "Attitude (Body/NED)", t = t,
                plot_vars = (pvars['psi'], pvars['theta'], pvars['phi'])),
            'v_b': make_figure(
                title = "Velocity (Body/ECEF, Body Axes)", t = t,
                plot_vars = (pvars['v_x'], pvars['v_y'], pvars['v_z'])),
            'v_n': make_figure(
                title = "Velocity (Body/ECEF, NED Axes)", t = t,
                plot_vars = (pvars['v_N'], pvars['v_E'], pvars['v_D'])),
            'pos': make_figure(
                title = "Geodetic Position", t = t,
                plot_vars = (pvars['lat'], pvars['lon'], pvars['h'])),
            'omega': make_figure(
                title = "Angular Velocity (Body/ECEF, Body Axes)", t = t,
                plot_vars = (pvars['p'], pvars['q'], pvars['r'])),
            'alpha': make_figure(
                title = "Angular Acceleration (Body/ECEF, Body Axes)", t = t,
                plot_vars = (pvars['p_dot'], pvars['q_dot'], pvars['r_dot'])),
            'acc': make_figure(
                title = "Acceleration (Body/ECEF, Body Axes)", t = t,
                plot_vars = (pvars['a_x'], pvars['a_y'], pvars['a_z'])),
        }

        return root_figs
