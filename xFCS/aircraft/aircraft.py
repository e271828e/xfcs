from collections import OrderedDict as OD
from collections import namedtuple as NT

import numpy as np
import xFCS.aircraft.landinggear as landinggear
import xFCS.aircraft.powerplant as powerplant
from xFCS.aircraft.aerodynamics import Aerodynamics, AeroNone
from xFCS.aircraft.mass import Mass, MassConstant
from xFCS.aircraft.rigidbody import FrameTransform, RigidBodyDynamics, Wrench
from xFCS.common.attitude import Attitude3D as Att
from xFCS.common.system import System, SystemOutput
from xFCS.environment.atmosphere import Atmosphere as Atmosphere
from xFCS.environment.terrain import Flat, Terrain

#pylint: disable=invalid-name
#pylint: disable=non-ascii-name

π = np.pi
KG_PER_SLUG = 14.59390
KG_PER_LB = 0.45359237
M_PER_FT = 0.3048
M_PER_IN = 0.0254
np.set_printoptions(precision=3)


class PwpGroup(System): #just for labelling purposes
    pass

class LdgGroup(System):
    pass

class SrfGroup(System):
    pass

class AircraftBase(System):

    def __init__(self,
                 airframe: RigidBodyDynamics = RigidBodyDynamics(),
                 pwp_group: PwpGroup = PwpGroup(),
                 srf_group: SrfGroup = SrfGroup(),
                 ldg_group: LdgGroup = LdgGroup(),
                 mass: Mass = MassConstant(),
                 aero: Aerodynamics = AeroNone(),
                 trn: Terrain = Flat(),
                 atm: Atmosphere = Atmosphere()):

        #define stateful components so the state vector can be assembled by
        #System.__init__
        subsystems = OD([
            ('airframe', airframe),
            ('pwp', pwp_group),
            ('srf', srf_group),
            ('ldg', ldg_group)])
        super().__init__(subsystems)

        self.mass = mass
        self.aero = aero
        self.trn = trn #typically, just a reference to an external Terrain
        self.atm = atm


    def f_cont(self, *, t):

        y_kin = self.airframe.kinematics #get kinematic data
        y_air = None #get air data from self.atm

        #this x_dot only has its subsystem blocks updated, the airframe must be
        #handled afterwards
        # x_dot, y = super().f_cont(t = t, y_kin = y_kin, y_air = y_air,
        #                           trn = self.trn)

        x_pwp_dot, y_pwp = self.pwp.f_cont(t = t, y_kin = y_kin, y_air = y_air)
        x_ldg_dot, y_ldg = self.ldg.f_cont(t = t, y_kin = y_kin, trn = self.trn)
        x_srf_dot, y_srf = self.srf.f_cont(t = t)

        y_mass = self.mass.y(y_fuel = None) #stores are kept track of in MassProperties
        y_aero = self.aero.y(y_air, y_srf, y_ldg, self.trn)

        #external wrench from airframe aerodynamics
        wr_aero_Ob_b = y_aero.wr_Ob_b

        #external wrench from the powerplant components
        wr_pwp_Ob_b = sum([y.root.wr_Ob_b for y in y_pwp.ss.values()],
                          Wrench.null())

        #external wrench from the landing gear components
        wr_ldg_Ob_b = sum([y.root.wr_Ob_b for y in y_ldg.ss.values()],
                          Wrench.null())

        #overall external wrench on the vehicle
        wr_ext_Ob_b = wr_aero_Ob_b + wr_pwp_Ob_b + wr_ldg_Ob_b

        #angular momentum due to powerplant rotating elements
        h_rot_b = sum([y.root.h_Gc_b for y in y_pwp.ss.values()])

        x_airframe_dot = self.airframe.x_dot(y_mass, y_kin, wr_ext_Ob_b, h_rot_b)
        y_airframe = self.airframe.y(x_airframe_dot, y_kin)

        x_dot = self._XType()
        x_dot.airframe = x_airframe_dot
        if x_pwp_dot:
            x_dot.pwp = x_pwp_dot
        if x_ldg_dot:
            x_dot.ldg = x_ldg_dot
        if x_srf_dot:
            x_dot.srf = x_srf_dot

        y_ss = OD((('airframe', y_airframe), ('pwp', y_pwp), ('ldg', y_ldg),
                  ('srf', y_srf)))

        y = SystemOutput(ss = y_ss)

        return x_dot, y


    def f_disc(self, t):

        # self.mass.f_disc(t)
        # self.aerodynamics.f_disc(t)
        #etc
        super().f_disc(t = t) #this takes care of the subsystems



class AircraftGeneric(AircraftBase):

    def __init__(self, trn: Terrain = Flat(), atm: Atmosphere = Atmosphere()):

        m = 1454.0 * KG_PER_LB
        r_ObG_b = np.zeros(3)
        J_Ob_b = np.diag([948, 1346, 1967]) * (KG_PER_SLUG * M_PER_FT**2)

        mass = MassConstant(m = m, r_ObG_b = r_ObG_b, J_Ob_b = J_Ob_b)

        pwp = OD((
            ('front', powerplant.PistonPowerplant(
                frame = FrameTransform(
                    r_O1O2_1 = np.array((0, 0, 0)),
                    att_12 = Att().from_euler((0, 0, 0))),
                prop = powerplant.FixedPitch(
                    sense = powerplant.Propeller.TurnSense.CCW))
            ),
            ))

        ldg = OD((
            ('lmain', landinggear.LandingGear(
                s_frame = FrameTransform(
                    r_O1O2_1 = np.array((-1, -1.25, 1)),
                    att_12 = Att.null()))
             ),
            ('rmain', landinggear.LandingGear(
                s_frame = FrameTransform(
                    r_O1O2_1 = np.array((-1, 1.25, 1)),
                    att_12 = Att.null()))
             ),
            ('nose', landinggear.LandingGear(
                s_frame = FrameTransform(
                    r_O1O2_1 = np.array((2, 0, 1)),
                    att_12 = Att.null()))
             ),
        ))

        super().__init__(mass = mass, trn = trn, atm = atm,
                         pwp_group = PwpGroup(pwp),
                         ldg_group = LdgGroup(ldg))

class ReactionTestRig(AircraftBase):

    def __init__(self, trn = Flat(), atm = Atmosphere()):

        m = 659.52
        r_ObG_b = np.array((0, 0, 0))
        J_Ob_b = np.diag([1285.315, 1824.931, 2666.893])

        mass = MassConstant(m = m, r_ObG_b = r_ObG_b, J_Ob_b = J_Ob_b)
        pwp = OD((
            ('front', powerplant.PistonPowerplant(
                frame = FrameTransform(
                    r_O1O2_1 = np.array((0, 0, 0)), #vertical eq point
                    att_12 = Att().from_euler((π/2, 0, 0))),
                prop = powerplant.FixedPitch(
                    sense = powerplant.Propeller.TurnSense.CCW))
            ),
            ))
        ldg= OD((
            ('lmain', landinggear.LandingGear(
                s_frame = FrameTransform(
                    r_O1O2_1 = np.array((-2/3, -1, 0.0868562930)),
                    att_12 = Att.from_euler(0, 0, 0)))
             ),
            ('rmain', landinggear.LandingGear(
                s_frame = FrameTransform(
                    r_O1O2_1 = np.array((-2/3, 1, 0.0868562930)),
                    att_12 = Att.from_euler(0, 0, 0)))
             ),
            ('nose', landinggear.LandingGear(
                s_frame = FrameTransform(
                    r_O1O2_1 = np.array((4/3, 0, 0.0868562930)),
                    att_12 = Att.from_euler(0, 0, 0)))
             ),
        ))

        super().__init__(trn = trn, atm = atm, mass = mass,
                         pwp_group = PwpGroup(pwp),
                         ldg_group = LdgGroup(ldg))


class HandOfGodRig(AircraftBase):

    def __init__(self, trn = Flat(), atm = Atmosphere()):

        m = 659.52
        r_ObG_b = np.array((0, 0, 0))
        J_Ob_b = np.diag([1285.315, 1824.931, 2666.893])
        z_ldg = 0.086

        mass = MassConstant(m = m, r_ObG_b = r_ObG_b, J_Ob_b = J_Ob_b)
        pwp = OD((
            ('handofgod', powerplant.HandOfGod()),
            ))
        ldg= OD((
            ('lmain', landinggear.LandingGear(
                s_frame = FrameTransform(
                    r_O1O2_1 = np.array((-2/3, -1, z_ldg)),
                    att_12 = Att.from_euler(0, 0, 0)))
             ),
            ('rmain', landinggear.LandingGear(
                s_frame = FrameTransform(
                    r_O1O2_1 = np.array((-2/3, 1, z_ldg)),
                    att_12 = Att.from_euler(0, 0, 0)))
             ),
            ('nose', landinggear.LandingGear(
                s_frame = FrameTransform(
                    r_O1O2_1 = np.array((4/3, 0, z_ldg)),
                    att_12 = Att.from_euler(0, 0, 0)))
             ),
        ))

        super().__init__(trn = trn, atm = atm, mass = mass,
                         pwp_group = PwpGroup(pwp),
                         ldg_group = LdgGroup(ldg))


class DropTestRig(AircraftBase):

    def __init__(self, trn = Flat(), atm = Atmosphere(), splay_angle = 0):

        ldg= OD((
            ('front_left', landinggear.LandingGear(
                s_frame = FrameTransform(
                    r_O1O2_1 = np.array((1, -1, 1)),
                    att_12 = Att.from_euler(0, 0, splay_angle)),
                shock_abs = landinggear.ShockAbsorber(
                    k_s = 1, k_d_cmp = 0, k_d_ext = 0))
             ),
            ('front_right', landinggear.LandingGear(
                s_frame = FrameTransform(
                    r_O1O2_1 = np.array((1, 1, 1)),
                    att_12 = Att.from_euler(0, 0, -splay_angle)),
                shock_abs = landinggear.ShockAbsorber(
                    k_s = 1, k_d_cmp = 0, k_d_ext = 0))
             ),
            ('back_left', landinggear.LandingGear(
                s_frame = FrameTransform(
                    r_O1O2_1 = np.array((-1, -1, 1)),
                    att_12 = Att.from_euler(0, 0, splay_angle)),
                shock_abs = landinggear.ShockAbsorber(
                    k_s = 1, k_d_cmp = 0, k_d_ext = 0))
             ),
            ('back_right', landinggear.LandingGear(
                s_frame = FrameTransform(
                    r_O1O2_1 = np.array((-1, 1, 1)),
                    att_12 = Att.from_euler(0, 0, -splay_angle)),
                shock_abs = landinggear.ShockAbsorber(
                    k_s = 1, k_d_cmp = 0, k_d_ext = 0))
             ),
        ))

        super().__init__(trn = trn, atm = atm, ldg_group = LdgGroup(ldg))
#mass properties taken from JSBSim's Cessna 172.
# see:
# http://jsbsim.sourceforge.net/JSBSimReferenceManual.pdf
# https://github.com/JSBSim-Team/jsbsim/blob/master/aircraft/c172x/c172x.xml

#conceptually, the body frame I'm using is analogous to JSBSim's
#structural frame, but using the conventional x-forward, z-down axes
#convention (hence the sign changes in r_ObG_b). also, I'm solving the
#dynamic equations directly in this frame rather than defining another
#body frame with origin at the CoM.

# class Cessna172(AircraftBase):

#     def __init__(self, trn: Terrain, atm: Atmosphere):

#         m = 1454.0 * KG_PER_LB
#         r_ObG_b = np.zeros(3)
#         J_Ob_b = np.diag([948, 1346, 1967]) * (KG_PER_SLUG * M_PER_FT**2)

#         mass = MassConstant(m = m, r_ObG_b = r_ObG_b, J_Ob_b = J_Ob_b)

#         super().__init__(mass = mass, trn = trn, atm = atm)
