from collections import namedtuple as NT
from abc import ABC, abstractmethod
import numpy as np
import xFCS.environment.terrain as terrain
from xFCS.aircraft.rigidbody import Wrench, FrameTransform


#this component allows implementing an aerodynamic model for the overall
#airframe level, since this is how aerodynamic models are usually defined. these
#models are unique for a given air vehicle, typically stateless and they require
#air data, control surface deflections, landing gear configuration, etc.
#therefore, it does not make much sense to implement it as a Subsystem subtype

#however, this does not preclude defining component subtypes with non-zero
#aerodynamic wrench contributions (in fact, powerplant components are a specific
#example of such a subtype). however, these should be computed within their own
#f methods and returned as part of their output tuples

YAero = NT(typename = 'YAero', field_names = ('wr_Oc_c', 'wr_Ob_b'),
           defaults = (Wrench.null(), Wrench.null()))

class Aerodynamics():

    def __init__(self, frame: FrameTransform = None):
        if frame is None:
            frame = FrameTransform()
        self._frame = frame

    @abstractmethod
    def y(self, y_air, y_srf, y_ldg, trn) -> YAero:
        pass

class AeroNone(Aerodynamics):

    def y(self, y_air, y_srf, y_ldg, trn) -> YAero:

        wr_Oc_c = Wrench.null()
        wr_Ob_b = wr_Oc_c.translate(self._frame)

        return YAero(wr_Oc_c = wr_Oc_c, wr_Ob_b = wr_Ob_b)



# class Aerodynamics(Subsystem):

#     @property
#     def XType(self): #no continuous internal states
#         return None

#     @property
#     def UType(self): #no explicit inputs
#         return None

#     @property
#     def x0(self): #no continuous internal states to initialize
#         return None

# class NoAero(Aerodynamics):

#     def f(self, y_air, y_srf, y_trn):

#         wrench_Oc_c = Wrench(T = np.zeros(3), F = np.zeros(3))
#         wrench_Ob_b = wrench_Oc_c.translate(self._frame)

#         y = YAero(wrench_Oc_c = wrench_Oc_c, wrench_Ob_b = wrench_Ob_b)

#         return (None, y) #no x_dot