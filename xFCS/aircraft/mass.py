from abc import ABC, abstractmethod
from collections import namedtuple as NT

import numpy as np
from xFCS.aircraft.rigidbody import MassData


class Mass(ABC):
    pass

class MassConstant(Mass):

    def __init__(self, m = 1, J_Ob_b = np.eye(3), r_ObG_b = np.zeros(3)):

        self.m = m
        self.J_Ob_b = J_Ob_b
        self.r_ObG_b = r_ObG_b


    def y(self, y_fuel):

        y = MassData(m = self.m, J_Ob_b = self.J_Ob_b, r_ObG_b = self.r_ObG_b)

        return y
