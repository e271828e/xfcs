from collections import OrderedDict as OD
from collections import namedtuple as NT

import numpy as np
from xFCS.aircraft.rigidbody import FrameTransform, Wrench
from xFCS.common.attitude import Attitude3D as Att
from xFCS.common.math import cross, norm
from xFCS.common.structuredvector import NodeType
from xFCS.common.system import System, SystemOutput
from xFCS.common.plotting import PlotVar, make_figure
from xFCS.environment.terrain import SurfaceCondition

#pylint: disable=arguments-differ
#pylint: disable=invalid-name
#pylint: disable=non-ascii-name

π = np.pi

class ShockAbsorber:

    def __init__(self, l_0 = 0, k_s = 25000, k_d_ext = 1000, k_d_cmp = 1000):

        self.l_0 = l_0 #natural length
        self.k_s = k_s #spring constant
        self.k_d_ext = k_d_ext #extension damping coefficient
        self.k_d_cmp = k_d_cmp #compression damping coefficient
        self.ξ_min = -4 #compression below which the shock absorber is disabled

    def force(self, ξ, ξ_dot):

        #Force exerted by the shock absorber along zs (due to elastic
        #deformation or oleo elongation). This can be negative, which
        #corresponds to the shock absorber pulling the piston rod assembly
        #upwards along the negative z_s axis. This can happen when we have xi <
        #0 (otherwise we wouldn't even be here) with xi_dot > 0 and sufficiently
        #large.
        k_d = self.k_d_ext if ξ_dot > 0 else self.k_d_cmp
        F = -self.k_s * ξ - k_d * ξ_dot
        F = F * (ξ > self.ξ_min)

        return F


class vRegulator(System):

    YRoot = NT(typename = 'vReg_YRoot',
            field_names = ('v', 's', 'α_p', 'α_i',
                           'α_raw', 'sat', 'α'),
            defaults = (np.zeros(2), np.zeros(2), np.zeros(2), np.zeros(2),
                        np.zeros(2), np.full(2, False), np.zeros(2)))

    def __init__(self, k_p = 5, k_i = 400):

        self.k_p = k_p
        self.k_i = k_i

        super().__init__()

    def f_cont(self, *, t, v_eOc_c = None):

        x_dot = self._XType()

        #if there is no contact, v_eOc_c is None. disable integrator and return
        #default outputs
        if v_eOc_c is None:
            x_dot[:] = 0
            y_root = self.YRoot()

        else:
            v = v_eOc_c[0:2] #contact point velocity
            s = self._x[:] #velocity integrator state
            α_p = -self.k_p * v
            α_i = -self.k_i * s
            α_raw = α_p + α_i #raw mu scaling
            α = np.clip(α_raw, -1, 1) #clipped mu scaling
            sat = np.abs(α_raw) > np.abs(α) #saturated?
            #if not saturated, integrator accumulates
            x_dot[:] = v * np.logical_not(sat)
            y_root = self.YRoot(v = v, s = s, α_p = α_p, α_i = α_i,
                                α_raw = α_raw, sat = sat, α = α)

        y = SystemOutput(root = y_root)
        return x_dot, y


    def _get_XType(self):

        X = NodeType('vReg_X', blocks = OD((
            ('s_x', NodeType(size = 1)),
            ('s_y', NodeType(size = 1))
            )))
        return X

    def initialize_state(self):
        #initialize integrators to zero
        self.x.s_x = 0
        self.x.s_y = 0

    def get_root_figures(self, log):

        t = log['t']
        y = log['y']

        pvars = {
            'v_x': PlotVar('Contact Point Velocity (X)', r"$v_{eOc}^{c[x]} \/ (m/s)$",
                           np.array([s.v[0] for s in y])),
            'v_y': PlotVar('Contact Point Velocity (Y)', r"$v_{eOc}^{c[y]} \/ (m/s)$",
                           np.array([s.v[1] for s in y])),
            's_x': PlotVar('Integrator State (X)', r"$\int{v_{eOc}^{c[x]}dt} \/ (m)$",
                           np.array([s.s[0] for s in y])),
            's_y': PlotVar('Integrator State (Y)', r"$\int{v_{eOc}^{c[y]}dt} \/ (m)$",
                           np.array([s.s[1] for s in y])),
            'α_p_x': PlotVar('Proportional Term (X)', r"$\alpha_{p}^{x}$",
                           np.array([s.α_p[0] for s in y])),
            'α_p_y': PlotVar('Proportional Term (Y)', r"$\alpha_{p}^{y}$",
                           np.array([s.α_p[1] for s in y])),
            'α_i_x': PlotVar('Integral Term (X)', r"$\alpha_{i}^{x}$",
                           np.array([s.α_i[0] for s in y])),
            'α_i_y': PlotVar('Integral Term (Y)', r"$\alpha_{i}^{y}$",
                           np.array([s.α_i[1] for s in y])),
            'α_r_x': PlotVar('Raw Regulator Output (X)', r"$\alpha_{raw}^{x}$",
                           np.array([s.α_raw[0] for s in y])),
            'α_r_y': PlotVar('Raw Regulator Output (Y)', r"$\alpha_{raw}^{y}$",
                           np.array([s.α_raw[1] for s in y])),
            'sat_x': PlotVar('Regulator Saturation (X)', r"$sat_{x}$",
                           np.array([s.sat[0] for s in y])),
            'sat_y': PlotVar('Regulator Saturation (Y)', r"$sat_{y}$",
                           np.array([s.sat[1] for s in y])),
            'α_x': PlotVar('Saturated Regulator Output (X)', r"$\alpha^{x}$",
                           np.array([s.α[0] for s in y])),
            'α_y': PlotVar('Saturated Regulator Output (Y)', r"$\alpha^{y}$",
                           np.array([s.α[1] for s in y])),
        }

        root_figs = {
            '01_x1': make_figure(
                title = "X Axis Regulator (I)", t = t,
                plot_vars = (pvars['v_x'], pvars['α_p_x'], pvars['α_i_x'])),
            '02_x2': make_figure(
                title = "X Axis Regulator (II)", t = t,
                plot_vars = (pvars['α_r_x'], pvars['sat_x'], pvars['α_x'])),
            '03_y1': make_figure(
                title = "Y Axis Regulator (I)", t = t,
                plot_vars = (pvars['v_y'], pvars['α_p_y'], pvars['α_i_y'])),
            '04_y2': make_figure(
                title = "Y Axis Regulator (II)", t = t,
                plot_vars = (pvars['α_r_y'], pvars['sat_y'], pvars['α_y'])),
        }

        return root_figs


class LandingGear(System):

    YRoot = NT(typename = 'Ldg_YRoot',
            field_names = ('ψ_sw', 'brk', 'WoW', 'l', 'ξ', 'ξ_dot',
                           'v_eOc_c', 'k_bo', 'ψ_cv',
                           'μ_max', 'f_c', 'wr_Oc_c', 'wr_Ob_b'),
            defaults = (0, 0, False, 0, 0, 0,
                        np.zeros(3), 0, 0,
                        np.zeros(2), np.zeros(3), Wrench.null(), Wrench.null()))


    def __init__(self, s_frame = FrameTransform(np.array((0,0,1)), Att.null()),
                 shock_abs = ShockAbsorber(), castering = False):

        self.s_frame = s_frame #airframe to strut frame transform
        self.shock_abs = shock_abs
        self.ψ_max = np.deg2rad(45)
        self.castering = castering

        self.μ_roll = {'static': 0.03, 'dynamic': 0.02}
        self.μ_skid = {SurfaceCondition.Dry: {'static': 0.75, 'dynamic': 0.5},
                       SurfaceCondition.Wet: {'static': 0.25, 'dynamic': 0.15},
                       SurfaceCondition.Icy: {'static': 0.075, 'dynamic': 0.05}}
        self.v_bo = (0.005, 0.01) #breakout velocity interval
        self.ψ_skid = np.deg2rad(10)

        subsystems = OD((('vreg', vRegulator()),))

        super().__init__(subsystems)


    def _get_UType(self):

        ULdg = NodeType('Ldg_U', blocks = OD((
            ('brk', NodeType(size = 1)), #brake setting
            ('psi', NodeType(size = 1)), #steering angle
            )))

        return ULdg


    def initialize_input(self):
        self.u.brk = 0
        self.u.psi = 0


    def f_cont(self, *, t, **kwargs):

        x_dot = self._XType()

        b_s = self.s_frame.att_12
        r_ObOs_b = self.s_frame.r_O1O2_1
        brk = self._u.brk[0]

        y_kin = kwargs['y_kin']
        r_ObOs_e = y_kin.att.e_b.transform(r_ObOs_b)
        Os = y_kin.pos.Ob + r_ObOs_e
        y_trn = kwargs['trn'](Os)
        Δh = Os.h - y_trn.h_gnd

        n_b = y_kin.att.n_b
        n_s = n_b * b_s

        ###### determine WoW ######
        k_s_n = n_s.transform((0, 0, 1))
        if k_s_n[2] < 1e-3: #the strut is upside down, set it to its natural length
            l = self.shock_abs.l_0
        else:
            l = Δh / k_s_n[2]
        ξ = l - self.shock_abs.l_0
        WoW = ξ < 0

        if not WoW:
            ##resetting integrator states SHOULD ONLY BE DONE ON A F_DISC CALL
            # self._x.mu[:] = 0
            x_dot_vreg, y_vreg = self.vreg.f_cont(t = t, v_eOc_c = None)
            x_dot = x_dot_vreg
            y_root = self.YRoot(WoW = WoW, l = l, ξ = ξ)
            y_ss = OD((('vreg', y_vreg),))
            y = SystemOutput(root = y_root, ss = y_ss)
            return x_dot, y

        r_OsOc_s = np.array((0, 0, l))
        r_ObOc_b = r_ObOs_b + b_s.transform(r_OsOc_s)
        #contact point velocity due to airframe motion
        v_eOc_airframe_b = y_kin.vel.v_eOb_b + cross(y_kin.vel.ω_eb_b, r_ObOc_b)

        if self.castering:
            #steering angle is given by the projection of the contact point
            #velocity due to the airframe onto the xy plane of the strut axes
            v_eOc_airframe_s = b_s.inv().transform(v_eOc_airframe_b)
            ψ_sw = np.clip(np.arctan2(v_eOc_airframe_s[1], v_eOc_airframe_s[0]),
                           -self.ψ_max, self.ψ_max)
        else: #otherwise, it is determined by the steering angle input
            u_psi = self._u.psi[0]
            assert(abs(u_psi) <= 1)
            ψ_sw = u_psi * self.ψ_max

        # print(ψ_sw)

        #rotate strut axes to get wheel axes
        s_w = Att.from_axis_angle((0, 0, 1), ψ_sw, bypass_val=True)
        n_w = n_s * s_w
        i_w_n = n_w.transform((1, 0, 0)) #NED components of wheel x-axis

        ####### contact frame axes ######
        k_t_n = y_trn.k_t_n #terrain surface normal NED components
        i_w_srf_n = i_w_n - np.dot(i_w_n, k_t_n) * k_t_n
        norm_i_w_srf_n = norm(i_w_srf_n)
        assert(norm_i_w_srf_n > 1e-5)
        i_c_n = i_w_srf_n / norm_i_w_srf_n

        #construct n_c rotation matrix by stacking the n components of i_c, j_c
        #and k_c as column vectors
        k_c_n = k_t_n
        j_c_n = cross(k_c_n, i_c_n)
        R_n_c = np.vstack((i_c_n, j_c_n, k_c_n)).transpose()
        n_c = Att.from_rmat(R_n_c)
        b_c = n_b.inv() * n_c

        b_c_frame = FrameTransform(r_O1O2_1 = r_ObOc_b, att_12 = b_c)

        ######### determine xidot and overall contact point velocity #######
        c_b = b_c.inv()
        v_eOc_airframe_c = c_b.transform(v_eOc_airframe_b)

        #compute the shock absorber elongation rate so that it cancels the
        #airframe velocity of the contact point along the contact frame z-axis
        c_s = c_b * b_s
        k_s_c = c_s.transform((0, 0, 1)) #k_s_s
        ξ_dot = -v_eOc_airframe_c[2] / k_s_c[2]
        v_eOc_shock_c = k_s_c * ξ_dot
        v_eOc_c = v_eOc_airframe_c + v_eOc_shock_c

        #get friction coefficients and F_gnd_c / N vector
        x_dot_vreg, y_vreg = self.vreg.f_cont(t = t, v_eOc_c = v_eOc_c)
        ψ_cv = np.arctan2(v_eOc_c[1], v_eOc_c[0])
        μ_max, k_bo = self.get_μ_max(ψ_cv, brk, y_trn.condition, v_eOc_c)
        μ_scaled = y_vreg.root.α * μ_max #the components of this can be positive or negative

        #Force exerted by the ground on the piston rod assembly projected in the
        #contact frame axes is given by F_gnd_c = f_c * N. To maintain the
        #non-penetration constraint, the force exerted by the shock absorber on
        #the tire (F_shock_zs) must be canceled by that exerted by the ground
        #along the strut z axis, that is: F_shock_zs + #F_gnd_zs = F_shock_zs +
        #f_s[2] * N = 0
        f_c = np.array((μ_scaled[0], μ_scaled[1], -1))
        f_s = c_s.inv().transform(np.array(f_c))
        F_shock_zs = self.shock_abs.force(ξ, ξ_dot)
        assert(abs(f_s[2]) > 0)
        N = -F_shock_zs / f_s[2]
        N = max(0, N) #cannot be negative (could happen with large xi_dot >0)

        #Then:
        F_gnd_Oc_c = f_c * N
        wr_Oc_c = Wrench(F = F_gnd_Oc_c, T = np.zeros(3))
        wr_Ob_b = wr_Oc_c.translate(b_c_frame)

        x_dot = x_dot_vreg
        y_root = self.YRoot(
            ψ_sw = ψ_sw, brk = brk, WoW = WoW, l = l, ξ = ξ, ξ_dot = ξ_dot,
            v_eOc_c = v_eOc_c, k_bo = k_bo, ψ_cv = ψ_cv, μ_max = μ_max,
            f_c = f_c, wr_Oc_c = wr_Oc_c, wr_Ob_b = wr_Ob_b)
        y_ss = OD((('vreg', y_vreg),))
        y = SystemOutput(root = y_root, ss = y_ss)

        return x_dot, y


    def get_μ_max(self, ψ, brk, srf_cond: SurfaceCondition, v_eOc_c):

        """Computes maximum friction coefficients along the x and y axes of the
        contact frame"""

        v_bo = self.v_bo
        v_abs = norm(v_eOc_c[0:2])
        #breakout coefficient (linear transition from 0: static to 1: dynamic)
        k_bo = np.clip( (v_abs - v_bo[0]) / (v_bo[1] - v_bo[0]), 0, 1)

        μ_roll = self.μ_roll['dynamic'] * k_bo + \
                 self.μ_roll['static'] * (1 - k_bo)
        μ_skid = self.μ_skid[srf_cond]['dynamic'] * k_bo + \
                 self.μ_skid[srf_cond]['static'] * (1 - k_bo)
        ψ_skid = self.ψ_skid

        ψ_abs = abs(ψ)
        assert(ψ_abs <= π)
        #if the contact velocity is very small, ψ becomes ill-defined, and the
        #maximum mu_y chatters. to avoid this, for k_bo = 0 (static friction),
        #we set mu_y to its skidding value
        if k_bo < 1:
            μ_y = μ_skid
        else:
            if ψ_abs < ψ_skid:
                μ_y = μ_skid * ψ_abs / ψ_skid
            elif ψ_abs > π - ψ_skid:
                μ_y = μ_skid * (1 - (ψ_skid + ψ_abs - π)/ ψ_skid)
            else:
                μ_y = μ_skid

        μ_x = μ_roll + (μ_skid - μ_roll) * brk

        μ = np.array((μ_x, μ_y))

        #if the combined magnitude of mu_x and mu_y exceeds mu_skid, scale them
        μ_mag = norm(μ)
        μ_sc = min(1, μ_skid / μ_mag)
        μ *= μ_sc

        return μ, k_bo


    def get_root_figures(self, log):

        t = log['t']
        y = log['y']

        pvars = {
            'psi_sw': PlotVar('Wheel Steering Angle', r"$\psi_{sw} \/ (rad)$",
                           np.array([s.ψ_sw for s in y])),
            'brk': PlotVar('Braking Intensity', r"$u_{brk}$",
                           np.array([s.brk for s in y])),
            'WoW': PlotVar('Weight On Wheel', r"$WoW$",
                           np.array([s.WoW for s in y])),
            'xi': PlotVar('Shock Absorber Elongation', r"$\xi \/ (m)$",
                           np.array([s.ξ for s in y])),
            'xi_dot': PlotVar('Shock Absorber Elongation Rate', r"$\dot{\xi} \/ (m/s)$",
                           np.array([s.ξ_dot for s in y])),
            'v_xc': PlotVar('Contact Frame Velocity (X)', r"$v_{eOc}^{c[x]} \/ (m/s)$",
                           np.array([s.v_eOc_c[0] for s in y])),
            'v_yc': PlotVar('Contact Frame Velocity (Y)', r"$v_{eOc}^{c[y]} \/ (m/s)$",
                           np.array([s.v_eOc_c[1] for s in y])),
            'psi_cv': PlotVar('Contact Frame Velocity Azimuth', r"$\psi_{cv} \/ (rad)$",
                           np.array([s.ψ_cv for s in y])),
            'k_bo': PlotVar('Breakout Coefficient', r"$k_{bo}$",
                           np.array([s.k_bo for s in y])),
            'mu_max_x': PlotVar('Maximum Friction Coefficient (X)', r"$\mu_{max}^{x}$",
                           np.array([s.μ_max[0] for s in y])),
            'mu_max_y': PlotVar('Maximum Friction Coefficient (Y)', r"$\mu_{max}^{y}$",
                           np.array([s.μ_max[1] for s in y])),
            'mu_x': PlotVar('Effective Friction Coefficient (X)', r"$f_{c}^{[x]}$",
                           np.array([s.f_c[0] for s in y])),
            'mu_y': PlotVar('Effective Friction Coefficient (Y)', r"$f_{c}^{[y]}$",
                           np.array([s.f_c[1] for s in y])),
            'Fc_x': PlotVar('Contact Frame Force (X)', r"$F_{Oc}^{c[x]} \/ (N)$",
                           np.array([s.wr_Oc_c.F[0] for s in y])),
            'Fc_y': PlotVar('Contact Frame Force (Y)', r"$F_{Oc}^{c[y]} \/ (N)$",
                           np.array([s.wr_Oc_c.F[1] for s in y])),
            'Fc_z': PlotVar('Contact Frame Force (Z)', r"$F_{Oc}^{c[z]} \/ (N)$",
                           np.array([s.wr_Oc_c.F[2] for s in y])),
            'Tc_x': PlotVar('Contact Frame Torque (X)', r"$T_{Oc}^{c[x]} \/ (N \/ m)$",
                           np.array([s.wr_Oc_c.T[0] for s in y])),
            'Tc_y': PlotVar('Contact Frame Torque (Y)', r"$T_{Oc}^{c[y]} \/ (N \/ m)$",
                           np.array([s.wr_Oc_c.T[1] for s in y])),
            'Tc_z': PlotVar('Contact Frame Torque (Z)', r"$T_{Oc}^{c[z]} \/ (N \/ m)$",
                           np.array([s.wr_Oc_c.T[2] for s in y])),
            'Fb_x': PlotVar('Airframe Force (X)', r"$F_{Ob}^{b[x]} \/ (N)$",
                           np.array([s.wr_Ob_b.F[0] for s in y])),
            'Fb_y': PlotVar('Airframe Force (Y)', r"$F_{Ob}^{b[y]} \/ (N)$",
                           np.array([s.wr_Ob_b.F[1] for s in y])),
            'Fb_z': PlotVar('Airframe Force (Z)', r"$F_{Ob}^{b[z]} \/ (N)$",
                           np.array([s.wr_Ob_b.F[2] for s in y])),
            'Tb_x': PlotVar('Airframe Torque (X)', r"$T_{Ob}^{b[x]} \/ (N \/ m)$",
                           np.array([s.wr_Ob_b.T[0] for s in y])),
            'Tb_y': PlotVar('Airframe Torque (Y)', r"$T_{Ob}^{b[y]} \/ (N \/ m)$",
                           np.array([s.wr_Ob_b.T[1] for s in y])),
            'Tb_z': PlotVar('Airframe Torque (Z)', r"$T_{Ob}^{b[z]} \/ (N \/ m)$",
                           np.array([s.wr_Ob_b.T[2] for s in y])),
        }

        root_figs = {
            '01_u': make_figure(
                title = "Control Inputs", t = t,
                plot_vars = (pvars['psi_sw'], pvars['brk'])),
            '02_xi': make_figure(
                title = "Shock Absorber Kinematics", t = t,
                plot_vars = (pvars['WoW'], pvars['xi'], pvars['xi_dot'])),
            '03_v1': make_figure(
                title = "Contact Frame Velocity (I)", t = t,
                plot_vars = (pvars['v_xc'], pvars['v_yc'])),
            '04_v2': make_figure(
                title = "Contact Frame Velocity (II)", t = t,
                plot_vars = (pvars['psi_cv'], pvars['k_bo'])),
            '05_mu_max': make_figure(
                title = "Maximum Friction Coefficients", t = t,
                plot_vars = (pvars['mu_max_x'], pvars['mu_max_y'])),
            '06_mu': make_figure(
                title = "Effective Friction Coefficients", t = t,
                plot_vars = (pvars['mu_x'], pvars['mu_y'])),
            '07_Fc': make_figure(
                title = "Contact Frame Force", t = t,
                plot_vars = (pvars['Fc_x'], pvars['Fc_y'], pvars['Fc_z'])),
            '08_Tc': make_figure(
                title = "Contact Frame Torque", t = t,
                plot_vars = (pvars['Tc_x'], pvars['Tc_y'], pvars['Tc_z'])),
            '09_Fb': make_figure(
                title = "Airframe Force", t = t,
                plot_vars = (pvars['Fb_x'], pvars['Fb_y'], pvars['Fb_z'])),
            '10_Tb': make_figure(
                title = "Airframe Torque", t = t,
                plot_vars = (pvars['Tb_x'], pvars['Tb_y'], pvars['Tb_z'])),
        }

        return root_figs
