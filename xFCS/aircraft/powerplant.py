from abc import abstractmethod
from collections import OrderedDict as OD
from collections import namedtuple as NT
from enum import IntEnum

import numpy as np
from xFCS.aircraft.rigidbody import FrameTransform, Wrench
from xFCS.common.plotting import PlotVar, make_figure
from xFCS.common.structuredvector import NodeType
from xFCS.common.system import System, SystemOutput

#pylint: disable=arguments-differ
#pylint: disable=invalid-name
#pylint: disable=non-ascii-name

YRoot = NT('YPwpRoot', ('wr_Oc_c', 'wr_Ob_b', 'h_Gc_b'))

class Powerplant(System):

    #in its y output vector, a Powerplant object must always provide its
    #airframe wrench, its airframe-projected angular momentum
    @abstractmethod
    def h_Gc_b(self, **kwargs):
        #this computes the component's additional angular momentum about its own
        #center of mass due to its rotation relative to the airframe. it must be
        #projected on the airframe axes.
        #
        #note that the component's angular momentum as part of the airframe,
        #which results from the airframe's rotation with respect to the inertial
        #reference frame, is already accounted for in the dynamic equations,
        #because this component's mass properties are included in the mass
        #properties of the overall airframe (r_OGsys_b and J_sys_Ob)
        pass


class Propeller:
    class TurnSense(IntEnum):
        CW = 1
        CCW = -1


class FixedPitch(Propeller):

    def __init__(self, sense: Propeller.TurnSense = Propeller.TurnSense.CW,
                 J_xx = 1.0, k_T = 0.01, k_F = 1.0):

        self.sense = sense
        self.J_xx = J_xx
        self.k_T = k_T
        self.k_F = k_F

    def h_Gc_xc(self, ω):
        return self.J_xx * ω

    def wrench(self, ω, v_aOs_s):
        F_ext_Os_s = self.k_F * ω**2 * np.array([1, 0, 0]) #simulate traction
        T_ext_Os_s = -self.sense * self.k_T * ω**2 * np.array([1, 0, 0]) #simulate drag torque
        return Wrench(F = F_ext_Os_s, T = T_ext_Os_s)


class Engine:
    pass

class PistonEngine(Engine):

    #this could be a subsystem of Powerplant, but for the moment it has no
    #states of its own, and it is the only target for inputs within Powerplant,
    #so a flat hierarchy works

    def __init__(self, T_max = 10):
        self.T_max = T_max #maximum torque

    def torque(self, u, ω):
        T = u * self.T_max
        return T

class PistonPowerplant(Powerplant):

    """
    having a Powerplant class solves the problem of the engine not belonging to
    the propeller and the propeller not belonging to the engine

    the only state required by this component is the angular velocity of the
    propeller. the engine angular velocity can be computed from this value and
    the gear_ratio.

    although this state describes the angular velocity of the propeller, it
    cannot be assigned to the propeller subcomponent, because its time
    derivative depends both on the external torque on the propeller AND the
    torque provided by the engine, which is at the same level at the hierarchy
    within PowerPlant and therefore is not accessible by the propeller. an
    alternative architecture would consider the engine driving the propeller an
    attribute of the propeller itself, but...

    for a powerplant with variable pitch, we would need to have an additional
    block in u for the propeller pitch angle. and, if we modelled pitch actuator
    dynamics, those states would go inside the propeller class and inserted by
    the Powerplant class as a subnode in X. so we'd have x_pwp.omega and
    x_pwp.prop.pitch_angle
    """

    XPistonPwp = NodeType('XPistonPwp', blocks = OD((
        ('ω', NodeType(size = 1)),))) #propeller angular velocity

    UPistonPwp = NodeType('UPistonPwp', blocks = OD((
        ('throttle', NodeType(size = 1)), #throttle setting
        ('start', NodeType(size = 1))))) #state reset command

    def __init__(self, *,
                 frame: FrameTransform = None,
                 engine: PistonEngine = None,
                 prop: FixedPitch = None,
                 gear_ratio: float = 1.0):

        #define components before initializing state vector

        if frame is None:
            frame = FrameTransform()

        if engine is None:
            engine = PistonEngine()

        if prop is None:
            prop = FixedPitch()

        self._frame = frame
        self._engine = engine
        self._prop = prop
        self._gear_ratio = gear_ratio #propRPM / engineRPM, typically <1

        super().__init__()


    def _get_XType(self):
        return self.XPistonPwp

    def _get_UType(self):
        return self.UPistonPwp

    def initialize_state(self):
        self.x.ω = 0* self._prop.sense

    def initialize_input(self):
        self.u.throttle = 0
        self.u.start = 0

    def f_cont(self, *, t, **kwargs):

        #no subsystems to deal with, no need to call super().f_cont
        #neglects the contributions to angular momentum of any powerplant
        #components other than the propeller
        y_air = kwargs['y_air']

        ω_prop = self._x.ω[0]
        v_aOs_s = np.zeros(3)
        wr_Os_s = self._prop.wrench(ω_prop, v_aOs_s)
        wr_Ob_b = wr_Os_s.translate(self._frame)
        h_Gc_b = self.h_Gc_b(ω_prop)

        #now compute x_dot
        #engine model is unsigned!
        ω_eng = abs(ω_prop / self._gear_ratio)

        u_eng = self._u.throttle[0]
        T_eng_pwp_Os_sx = (self._prop.sense *
                           self._engine.torque(u = u_eng, ω = ω_eng))

        #net shaft torque
        T_ext_pwp_Os_sx = wr_Os_s.T[0]
        T_net_pwp_Os_sx = T_ext_pwp_Os_sx + T_eng_pwp_Os_sx

        ω_prop_dot = T_net_pwp_Os_sx / self.J_xx
        x_dot = self._XType()
        x_dot.ω = ω_prop_dot

        y_root = YRoot(wr_Oc_c = wr_Os_s, wr_Ob_b = wr_Ob_b, h_Gc_b = h_Gc_b)
        y = SystemOutput(root = y_root)

        return (x_dot, y)


    def f_disc(self, t):
        pass


    @property
    def J_xx(self):
        return self._prop.J_xx #neglect other contributions

    def h_Gc_xc(self, ω_prop):
        #axial angular momentum along the powerplant shaft (component axes)
        #neglects the contributions to angular momentum of any powerplant
        #components other than the propeller
        return self._prop.h_Gc_xc(ω_prop)

    def h_Gc_b(self, ω_prop):
        #axial angular momentum along the powerplant shaft (airframe axes)
        h_Gc_xc = self.h_Gc_xc(ω_prop)
        b_c = self._frame.att_12
        h_Gc_c = np.array((h_Gc_xc, 0, 0))
        h_Gc_b = b_c.transform(h_Gc_c)
        return h_Gc_b

    def get_root_figures(self, log):

        t = log['t']
        y = log['y']

        pvars = {
            'Fc_x': PlotVar('Local Frame Force (X)', r"$F_{Oc}^{c[x]} \/ (N)$",
                           np.array([s.wr_Oc_c.F[0] for s in y])),
            'Fc_y': PlotVar('Local Frame Force (Y)', r"$F_{Oc}^{c[y]} \/ (N)$",
                           np.array([s.wr_Oc_c.F[1] for s in y])),
            'Fc_z': PlotVar('Local Frame Force (Z)', r"$F_{Oc}^{c[z]} \/ (N)$",
                           np.array([s.wr_Oc_c.F[2] for s in y])),
            'Tc_x': PlotVar('Local Frame Torque (X)', r"$T_{Oc}^{c[x]} \/ (N \/ m)$",
                           np.array([s.wr_Oc_c.T[0] for s in y])),
            'Tc_y': PlotVar('Local Frame Torque (Y)', r"$T_{Oc}^{c[y]} \/ (N \/ m)$",
                           np.array([s.wr_Oc_c.T[1] for s in y])),
            'Tc_z': PlotVar('Local Frame Torque (Z)', r"$T_{Oc}^{c[z]} \/ (N \/ m)$",
                           np.array([s.wr_Oc_c.T[2] for s in y])),
            'Fb_x': PlotVar('Airframe Force (X)', r"$F_{Ob}^{b[x]} \/ (N)$",
                           np.array([s.wr_Ob_b.F[0] for s in y])),
            'Fb_y': PlotVar('Airframe Force (Y)', r"$F_{Ob}^{b[y]} \/ (N)$",
                           np.array([s.wr_Ob_b.F[1] for s in y])),
            'Fb_z': PlotVar('Airframe Force (Z)', r"$F_{Ob}^{b[z]} \/ (N)$",
                           np.array([s.wr_Ob_b.F[2] for s in y])),
            'Tb_x': PlotVar('Airframe Torque (X)', r"$T_{Ob}^{b[x]} \/ (N \/ m)$",
                           np.array([s.wr_Ob_b.T[0] for s in y])),
            'Tb_y': PlotVar('Airframe Torque (Y)', r"$T_{Ob}^{b[y]} \/ (N \/ m)$",
                           np.array([s.wr_Ob_b.T[1] for s in y])),
            'Tb_z': PlotVar('Airframe Torque (Z)', r"$T_{Ob}^{b[z]} \/ (N \/ m)$",
                           np.array([s.wr_Ob_b.T[2] for s in y])),
        }

        root_figs = {
            '01_Fc': make_figure(
                title = "Local Frame Force", t = t,
                plot_vars = (pvars['Fc_x'], pvars['Fc_y'], pvars['Fc_z'])),
            '02_Tc': make_figure(
                title = "Local Frame Torque", t = t,
                plot_vars = (pvars['Tc_x'], pvars['Tc_y'], pvars['Tc_z'])),
            '03_Fb': make_figure(
                title = "Airframe Force", t = t,
                plot_vars = (pvars['Fb_x'], pvars['Fb_y'], pvars['Fb_z'])),
            '04_Tb': make_figure(
                title = "Airframe Torque", t = t,
                plot_vars = (pvars['Tb_x'], pvars['Tb_y'], pvars['Tb_z'])),
        }

        return root_figs

class HandOfGod(Powerplant):

    UHoG = NodeType('UHoG', blocks = OD((
        ('f_Oc_c', NodeType(size = 3)), #local frame force
        ('t_Oc_c', NodeType(size = 3))))) #local frame torque

    def __init__(self, *, frame: FrameTransform = FrameTransform(),
                 F_max = 1000, T_max = 1000):

        self._frame = frame
        self.F_max = F_max
        self.T_max = T_max
        super().__init__()

    def _get_XType(self):
        return None

    def _get_UType(self):
        return self.UHoG

    def f_cont(self, *, t, **kwargs):

        h_Gc_b = self.h_Gc_b()
        wr_Oc_c = Wrench(F = self.u.f_Oc_c[:] * self.F_max,
                         T = self.u.t_Oc_c[:] * self.T_max)
        wr_Ob_b = wr_Oc_c.translate(self._frame)

        x_dot = None
        y_root = YRoot(wr_Oc_c = wr_Oc_c, wr_Ob_b = wr_Ob_b, h_Gc_b = h_Gc_b)
        y = SystemOutput(root = y_root)

        return (x_dot, y)

    def f_disc(self, t):
        pass

    def h_Gc_b(self):
        return np.zeros(3)

    def get_root_figures(self, log):

        t = log['t']
        y = log['y']

        pvars = {
            'Fc_x': PlotVar('Local Frame Force (X)', r"$F_{Oc}^{c[x]} \/ (N)$",
                           np.array([s.wr_Oc_c.F[0] for s in y])),
            'Fc_y': PlotVar('Local Frame Force (Y)', r"$F_{Oc}^{c[y]} \/ (N)$",
                           np.array([s.wr_Oc_c.F[1] for s in y])),
            'Fc_z': PlotVar('Local Frame Force (Z)', r"$F_{Oc}^{c[z]} \/ (N)$",
                           np.array([s.wr_Oc_c.F[2] for s in y])),
            'Tc_x': PlotVar('Local Frame Torque (X)', r"$T_{Oc}^{c[x]} \/ (N \/ m)$",
                           np.array([s.wr_Oc_c.T[0] for s in y])),
            'Tc_y': PlotVar('Local Frame Torque (Y)', r"$T_{Oc}^{c[y]} \/ (N \/ m)$",
                           np.array([s.wr_Oc_c.T[1] for s in y])),
            'Tc_z': PlotVar('Local Frame Torque (Z)', r"$T_{Oc}^{c[z]} \/ (N \/ m)$",
                           np.array([s.wr_Oc_c.T[2] for s in y])),
            'Fb_x': PlotVar('Airframe Force (X)', r"$F_{Ob}^{b[x]} \/ (N)$",
                           np.array([s.wr_Ob_b.F[0] for s in y])),
            'Fb_y': PlotVar('Airframe Force (Y)', r"$F_{Ob}^{b[y]} \/ (N)$",
                           np.array([s.wr_Ob_b.F[1] for s in y])),
            'Fb_z': PlotVar('Airframe Force (Z)', r"$F_{Ob}^{b[z]} \/ (N)$",
                           np.array([s.wr_Ob_b.F[2] for s in y])),
            'Tb_x': PlotVar('Airframe Torque (X)', r"$T_{Ob}^{b[x]} \/ (N \/ m)$",
                           np.array([s.wr_Ob_b.T[0] for s in y])),
            'Tb_y': PlotVar('Airframe Torque (Y)', r"$T_{Ob}^{b[y]} \/ (N \/ m)$",
                           np.array([s.wr_Ob_b.T[1] for s in y])),
            'Tb_z': PlotVar('Airframe Torque (Z)', r"$T_{Ob}^{b[z]} \/ (N \/ m)$",
                           np.array([s.wr_Ob_b.T[2] for s in y])),
        }

        root_figs = {
            '01_Fc': make_figure(
                title = "Local Frame Force", t = t,
                plot_vars = (pvars['Fc_x'], pvars['Fc_y'], pvars['Fc_z'])),
            '02_Tc': make_figure(
                title = "Local Frame Torque", t = t,
                plot_vars = (pvars['Tc_x'], pvars['Tc_y'], pvars['Tc_z'])),
            '03_Fb': make_figure(
                title = "Airframe Force", t = t,
                plot_vars = (pvars['Fb_x'], pvars['Fb_y'], pvars['Fb_z'])),
            '04_Tb': make_figure(
                title = "Airframe Torque", t = t,
                plot_vars = (pvars['Tb_x'], pvars['Tb_y'], pvars['Tb_z'])),
        }

        return root_figs
