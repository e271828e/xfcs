import numpy as np

import xFCS.environment.wgs84 as wgs84
from xFCS.aircraft.aircraft import AircraftGeneric
from xFCS.aircraft.rigidbody import InitialCondition
from xFCS.common.attitude import Attitude3D as Att
from xFCS.environment.atmosphere import Atmosphere
from xFCS.environment.terrain import Flat
from xFCS.io.xpinterfaces import SimInterface
from xFCS.simulation.blocks.aircraft import AircraftGndCtlBlock
from xFCS.simulation.blocks.general import Constant
from xFCS.simulation.engine import Model, Signal
from xFCS.simulation.blocks.io import XPlaneDisplay

#start X-Plane scenario here
sim = SimInterface()
xp_loc = sim.get_state(('lat', 'lon', 'alt', 'psi', 'theta', 'phi'))

#define terrain altitude as a negative offset with respect to the initial
#altitude matching the resting length of the strut (supporting weight)$
trn = Flat(xp_loc['alt'] - .9)
atm = Atmosphere()
aircraft = AircraftGeneric(trn, atm)

#get the at-rest x-plane altitude and use it for initialization
init = InitialCondition(
    Ob = wgs84.Point.from_geo(xp_loc['lat'], xp_loc['lon'], xp_loc['alt'] + 0.25),
    n_b = Att.from_euler(xp_loc['psi'], 0.1, 0.05),
    ω_wb_b = np.array((0., 0, 0)),
    v_eOb_b = np.array((30, 0, 0)))
aircraft.airframe.initialize_state(initial_condition = init)

m = Model(t_end = 15)

aircraft_block = AircraftGndCtlBlock(aircraft= aircraft)
aircraft_inputs_block = Constant(
    value = aircraft_block.ControlInputs(
    throttle = 0.4, lbrake = 0, rbrake = 1, nws = 0.063662 ))

m.add_block('inputs', aircraft_inputs_block)
m.add_block('aircraft', aircraft_block)
m.add_signal('aircraft_out', Signal(logging = True))
m.add_block('display', XPlaneDisplay())
m.connect(m.blocks['inputs'].get_output(), m.blocks['aircraft'].get_input())
m.connect(m.blocks['aircraft'].get_output(), m.signals['aircraft_out'])
m.connect(m.signals['aircraft_out'], m.blocks['display'].get_input())

m.run(profiled = True, real_time = True)

log = m.signals['aircraft_out']._log
save_path = "tmp\\display_test"
aircraft.plot(log, save_path = None , keep_open=False)
